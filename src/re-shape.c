/*
 * re-shape.c: Implementation of the REShape (Rect/Ellipse) abstract class
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include "re-shape.h"

static void
gdraw_re_shape_destroy (GtkObject *object)
{
	GDrawREShape *re = GDRAW_RE_SHAPE (object);

	g_free (re->fill);
	g_free (re->line);
	g_free (re->shadow);
	g_free (re->rotation);
}

static void
gdraw_re_shape_class_init (GtkObjectClass *klass)
{
	klass->destroy = gdraw_re_shape_destroy;
}

static void
gdraw_re_shape_init (GtkObject *object)
{
	GDrawREShape *re;

	g_return_if_fail (object);

	re = GDRAW_RE_SHAPE (object);

	re->fill = g_new0 (GDrawFillProperties, 1);
	re->line = g_new0 (GDrawLineProperties, 1);
	re->shadow = g_new0 (GDrawShadowProperties, 1);
	re->rotation = g_new0 (GDrawRotationDescriptor, 1);
}

GType
gdraw_re_shape_get_type (void)
{
	static GType re_type = 0;

	if (!re_type) {
		static const GTypeInfo re_info = {
			sizeof(GDrawREShapeClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gdraw_re_shape_class_init,
			NULL, NULL,
			sizeof(GDrawREShape),
			0,
			(GInstanceInitFunc) gdraw_re_shape_init
		};

		re_type = g_type_register_static(
			gdraw_shape_get_type(), "GDrawREShape", &re_info, 0);
	}

	return re_type;
}

gboolean
gdraw_re_shape_get_prop (GDrawREShape *re, guint arg_id, BonoboArg *arg)
{
	GDrawShape *shape = GDRAW_SHAPE (re);

	if (!gdraw_shape_descriptor_get_prop (shape->desc, arg_id, arg) &&
	    !gdraw_fill_properties_get_prop (re->fill, arg_id, arg) &&
	    !gdraw_line_properties_get_prop (re->line, arg_id, arg) &&
	    !gdraw_shadow_properties_get_prop (re->shadow, arg_id, arg) &&
	    !gdraw_rotation_descriptor_get_prop (re->rotation, arg_id, arg))
		return FALSE;

	return TRUE;
}

gboolean
gdraw_re_shape_set_prop (GDrawREShape *re, guint arg_id, BonoboArg *arg)
{
	GDrawShape *shape = GDRAW_SHAPE (re);

	if (gdraw_shape_descriptor_set_prop (shape->desc, arg_id, arg) ||
	   gdraw_fill_properties_set_prop (re->fill, arg_id, arg) ||
	   gdraw_line_properties_set_prop (re->line, arg_id, arg) ||
	   gdraw_shadow_properties_set_prop (re->shadow, arg_id, arg) ||
	   gdraw_rotation_descriptor_set_prop (re->rotation, arg_id, arg))
	{
		g_signal_emit_by_name(G_OBJECT(re), "changed");
		return TRUE;
	}

	return FALSE;
}

void
gdraw_re_shape_add_props (BonoboPropertyBag *bag)
{
	gdraw_shape_descriptor_add_props (bag);
	gdraw_fill_properties_add_props (bag);
	gdraw_line_properties_add_props (bag);
	gdraw_shadow_properties_add_props (bag);
	gdraw_rotation_descriptor_add_props (bag);
}
