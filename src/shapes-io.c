/*
 * shapes-io.c: Implements persistence for the Shapes aggregation class
 *
 * Copyright (c) 2001 Mike Kestner
 *
 * Authors: Mike Kestner <mkestner@ameritech.net>
 */

#include <config.h>
#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-stream-client.h>
#include <gdraw/oafiids.h>
#include "shapes-io.h"

#define PARENT_TYPE BONOBO_PERSIST_TYPE

static inline GDrawShapesStorage *
gdraw_shapes_storage_from_servant (PortableServer_Servant servant)
{
	return GDRAW_SHAPES_STORAGE (bonobo_object_from_servant (servant));
}	   

static CORBA_boolean
GSS_is_dirty (PortableServer_Servant servant, CORBA_Environment *ev)
{
	GDrawShapesStorage *gss = gdraw_shapes_storage_from_servant (servant);

	return gss->is_dirty;
}

static void
load_shape (GDrawShapes *shapes, const Bonobo_Storage storage, 
	    gint i, CORBA_Environment *ev)
{
	gchar *dir_name = g_strdup_printf ("shape%08d", i);
	gchar *oafiid;
	Bonobo_Storage shape_dir;
	Bonobo_Stream stream;
	Bonobo_PersistStream pstream;
	Bonobo_Unknown shape;
	glong len;

	shape_dir = Bonobo_Storage_openStorage (storage, dir_name,
						Bonobo_Storage_READ, ev);
	g_free (dir_name);
	if (BONOBO_EX (ev))
		return;

	stream = Bonobo_Storage_openStream (shape_dir, "oafiid",
					    Bonobo_Storage_READ, ev);
	if (BONOBO_EX (ev)) {
		bonobo_object_release_unref (shape_dir, NULL);
		return;
	}

	len = bonobo_stream_client_read_string (stream, &oafiid, ev);
	if (!oafiid) {
		bonobo_object_release_unref (shape_dir, NULL);
		bonobo_object_release_unref (stream, NULL);
		return;
	}

	bonobo_object_release_unref (stream, ev);

	stream = Bonobo_Storage_openStream (shape_dir, "shapedata",
					    Bonobo_Storage_READ, ev);
	if (BONOBO_EX (ev)) {
		bonobo_object_release_unref (shape_dir, NULL);
		return;
	}

	shape = bonobo_activation_activate_from_id(oafiid, 0, NULL, ev);
	g_free (oafiid);
	if (shape == CORBA_OBJECT_NIL || BONOBO_EX(ev)) {
		bonobo_object_release_unref (shape_dir, NULL);
		bonobo_object_release_unref (stream, NULL);
		return;
	}

	pstream = Bonobo_Unknown_queryInterface(
		shape, "IDL:Bonobo/PersistStream:1.0", ev);
	if (pstream == CORBA_OBJECT_NIL) {
		bonobo_object_release_unref (shape_dir, NULL);
		bonobo_object_release_unref (stream, NULL);
		Bonobo_Unknown_unref (shape, ev);
		return;
	}
	Bonobo_PersistStream_load (pstream, stream, "", ev);

	bonobo_object_release_unref (shape_dir, NULL);
	bonobo_object_release_unref (stream, NULL);
	Bonobo_Unknown_unref (pstream, ev);
	if (BONOBO_EX (ev)) {
		Bonobo_Unknown_unref (shape, ev);
		return;
	}

	gdraw_shapes_add_shape (shapes, shape);

	Bonobo_Unknown_unref (shape, ev);
}

static void
GSS_load (PortableServer_Servant servant, const Bonobo_Storage storage,
       	  CORBA_Environment *ev)
{
	GDrawShapesStorage *gss = gdraw_shapes_storage_from_servant (servant);
	Bonobo_Storage_DirectoryList *list;
	gint i;

	if (gss->shapes->shape_names) {
		g_warning ("Trying to load into a non-empty Shapes object");
		return;
	}

	list = Bonobo_Storage_listContents (storage, "/", 0, ev);
	if (!list)
		return;

	for (i = 0; i < list->_length; i++) {
		load_shape (gss->shapes, storage, i, ev);
		if (BONOBO_EX (ev)) {
			g_warning ("Failed to load a shape - %s",
				   bonobo_exception_get_text (ev));
			return;
		}
	}

	CORBA_free (list);
}

static void
save_shape (GNOME_Draw_Shape shape, const Bonobo_Storage storage, gint i, 
	    CORBA_Environment *ev)
{
	gchar *dir_name = g_strdup_printf ("shape%08d", i);
	gchar *oafiid;
	Bonobo_Storage shape_dir;
	Bonobo_Stream stream;
	Bonobo_PersistStream pstream;

	shape_dir = Bonobo_Storage_openStorage (storage, dir_name,
			Bonobo_Storage_CREATE, ev);
	g_free (dir_name);
	if (BONOBO_EX (ev))
		return;

	stream = Bonobo_Storage_openStream (shape_dir, "oafiid",
			Bonobo_Storage_CREATE | Bonobo_Storage_WRITE, ev);
	if (BONOBO_EX (ev)) {
		bonobo_object_release_unref (shape_dir, NULL);
		return;
	}

	oafiid = GNOME_Draw_Shape_getShapeType (shape, ev);
	if (BONOBO_EX (ev)) {
		bonobo_object_release_unref (shape_dir, NULL);
		return;
	}

	bonobo_stream_client_write_string (stream, oafiid, TRUE, ev);
	bonobo_object_release_unref (stream, ev);
	g_free (oafiid);
	if (BONOBO_EX (ev)) {
		bonobo_object_release_unref (shape_dir, NULL);
		return;
	}

	stream = Bonobo_Storage_openStream (shape_dir, "shapedata",
			Bonobo_Storage_CREATE | Bonobo_Storage_WRITE, ev);
	if (BONOBO_EX (ev)) {
		bonobo_object_release_unref (shape_dir, NULL);
		return;
	}

	pstream = Bonobo_Unknown_queryInterface (
			shape, "IDL:Bonobo/PersistStream:1.0", ev);
	if (pstream == CORBA_OBJECT_NIL) {
		bonobo_object_release_unref (shape_dir, NULL);
		bonobo_object_release_unref (stream, NULL);
		return;
	}

	Bonobo_PersistStream_save (pstream, stream, "", ev);

	bonobo_object_release_unref (shape_dir, NULL);
	bonobo_object_release_unref (stream, NULL);
	Bonobo_Unknown_unref (pstream, ev);
}

static void
GSS_save (PortableServer_Servant servant, const Bonobo_Storage storage,
       	  const CORBA_boolean same_as_loaded, CORBA_Environment *ev)
{
	GDrawShapesStorage *gss = gdraw_shapes_storage_from_servant (servant);
	GList *names;
	gint i = 0;

	names = gss->shapes->shape_names;

	while (names) {
		GNOME_Draw_Shape shape;

		shape = (GNOME_Draw_Shape) g_hash_table_lookup(
				gss->shapes->shape_by_name, names->data);
		if (shape == CORBA_OBJECT_NIL) {
			g_warning ("Inconsistency detected in Shapes hash");
			return;
		}
		save_shape (shape, storage, i++, ev);
		names = names->next;
	}
}

static Bonobo_Persist_ContentTypeList *
get_content_types (BonoboPersist *persist, CORBA_Environment *ev)
{
	return bonobo_persist_generate_content_types (1, "text/xml");
}

static void
gdraw_shapes_storage_class_init (GObjectClass *klass)
{
	GDrawShapesStorageClass *gssc = GDRAW_SHAPES_STORAGE_CLASS (klass);
	BonoboPersistClass *bpc = BONOBO_PERSIST_CLASS (klass);
	POA_Bonobo_PersistStorage__epv *epv = &gssc->epv;

	bpc->get_content_types = get_content_types;

#if 0
	epv->isDirty = GSS_is_dirty;
#endif
	epv->load = GSS_load;
	epv->save = GSS_save;
}

static void
gdraw_shapes_storage_init (GObject *object)
{
	GDrawShapesStorage *gss = GDRAW_SHAPES_STORAGE (object);

	gss->is_dirty = FALSE;
	gss->shapes = NULL;
}

BONOBO_TYPE_FUNC_FULL (GDrawShapesStorage, Bonobo_PersistStorage, 
		       PARENT_TYPE, gdraw_shapes_storage);

GDrawShapesStorage *
gdraw_shapes_storage_new (GDrawShapes *shapes)
{
	GDrawShapesStorage *gss;

	gss = g_object_new (GDRAW_SHAPES_STORAGE_TYPE, NULL);

	gss->shapes = shapes;

	printf ("PStorage servant = %p", BONOBO_OBJECT(gss)->servant);
	return gss;
}

