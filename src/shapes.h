/*
 * shapes.c: Interface for the Shapes class
 *
 * Copyright (c) 2000-2001 Mike Kestner 
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_SHAPES_H
#define __GDRAW_SHAPES_H

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-event-source.h>
#include <bonobo/bonobo-generic-factory.h>
#include <gdraw/GNOME_Draw.h>

G_BEGIN_DECLS

typedef struct _GDrawShapes      GDrawShapes;
typedef struct _GDrawShapesClass GDrawShapesClass;

#include "shapes-io.h"

#define GDRAW_SHAPES_TYPE      	 (gdraw_shapes_get_type())
#define GDRAW_SHAPES(o)          (GTK_CHECK_CAST((o), GDRAW_SHAPES_TYPE, GDrawShapes))
#define GDRAW_SHAPES_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDRAW_SHAPES_TYPE, GDrawShapesClass))
#define GDRAW_IS_SHAPES(o)       (GTK_CHECK_TYPE((o), GDRAW_SHAPES_TYPE))
#define GDRAW_IS_SHAPES_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), GDRAW_SHAPES_TYPE))

struct _GDrawShapes {
	BonoboObject	 	base;

	GHashTable	 	*shape_by_name;
	GList			*shape_names;

	BonoboEventSource	*ev_src;
	GDrawShapesStorage	*storage;
};
	
struct _GDrawShapesClass {
	BonoboObjectClass parent_class;

	POA_GNOME_Draw_Shapes__epv epv;
};

GType	         gdraw_shapes_get_type	(void);

GDrawShapes 	*gdraw_shapes_new	(void);

BonoboObject 	*gdraw_shapes_factory	(BonoboGenericFactory *);

void		gdraw_shapes_add_shape	(GDrawShapes *, GNOME_Draw_Shape);

G_END_DECLS

#endif /* __GDRAW_SHAPES_H */
