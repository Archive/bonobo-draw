/*
 * shape.c: Implementation of the Shape abstract class
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include "shape.h"

enum SIGNALS {
	CHANGED,
	LAST_SIGNAL
};
static guint signals[LAST_SIGNAL] = { 0 };

#define PARENT_TYPE BONOBO_OBJECT_TYPE

static inline GDrawShape *
gdraw_shape_from_servant (PortableServer_Servant servant)
{
	return GDRAW_SHAPE (bonobo_object_from_servant (servant));
}	   

static CORBA_char *
Shape_get_shape_type (PortableServer_Servant servant, CORBA_Environment *ev)
{
	GDrawShape *shape = gdraw_shape_from_servant (servant);

	return g_strdup (shape->type);
}
static GNOME_Draw_Point
Shape_get_position (PortableServer_Servant servant, CORBA_Environment *ev)
{
	GNOME_Draw_Point pt;
	GDrawShape *shape = gdraw_shape_from_servant (servant);

	pt.x = shape->x;
	pt.y = shape->y;

	return pt;
}

static void
Shape_set_position (PortableServer_Servant servant, const GNOME_Draw_Point *pt,
		    CORBA_Environment *ev)
{
	GDrawShape *shape = gdraw_shape_from_servant (servant);

	gdraw_shape_set_position (shape, (gdouble) pt->x, (gdouble) pt->y);
}

static GNOME_Draw_Size
Shape_get_size (PortableServer_Servant servant, CORBA_Environment *ev)
{
	GNOME_Draw_Size size;
	GDrawShape *shape = gdraw_shape_from_servant (servant);

	size.width = shape->width;
	size.height = shape->height;

	return size;
}
static void
Shape_set_size (PortableServer_Servant servant, const GNOME_Draw_Size *size,
		   CORBA_Environment *ev)
{
	GDrawShape *shape = gdraw_shape_from_servant (servant);

	gdraw_shape_set_size (shape, (gdouble) size->width, (gdouble) size->height);
}

static void
gdraw_shape_destroy (GtkObject *object)
{
	GDrawShape *shape = GDRAW_SHAPE (object);

	g_free (shape->type);
	g_free (shape->desc->name);
	g_free (shape->desc);
}

static void
gdraw_shape_class_init (GDrawShapeClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;
	POA_GNOME_Draw_Shape__epv *epv = &klass->epv;

	epv->getShapeType = Shape_get_shape_type;
	epv->getPosition = Shape_get_position;
	epv->setPosition = Shape_set_position;
	epv->getSize = Shape_get_size;
	epv->setSize = Shape_set_size;

	object_class->destroy = gdraw_shape_destroy;

	signals[CHANGED] = gtk_signal_new (
		"changed", GTK_RUN_LAST, GTK_CLASS_TYPE(object_class),
		GTK_SIGNAL_OFFSET (GDrawShapeClass, changed),
		gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);
}

static void
gdraw_shape_init (GtkObject *object)
{
	GDrawShape *shape;

	g_return_if_fail (object);

	shape = GDRAW_SHAPE (object);

	shape->x = 0.0;
	shape->y = 0.0;
	shape->width = 0.0;
	shape->height = 0.0;

	shape->type = NULL;
	shape->desc = g_new0 (GDrawShapeDescriptor, 1);
}

BONOBO_TYPE_FUNC_FULL(GDrawShape, GNOME_Draw_Shape, 
		      PARENT_TYPE, gdraw_shape);

void
gdraw_shape_set_position (GDrawShape *shape, gdouble x, gdouble y)
{
	g_return_if_fail (shape != NULL);
	g_return_if_fail (GDRAW_IS_SHAPE (shape));

	printf ("setPosition: %f, %f\n", x, y);

	shape->x = x;
	shape->y = y;

	g_signal_emit_by_name(G_OBJECT(shape), "changed");
}

void
gdraw_shape_set_size (GDrawShape *shape, gdouble width, gdouble height)
{
	g_return_if_fail (shape != NULL);
	g_return_if_fail (GDRAW_IS_SHAPE (shape));

	shape->width = width;
	shape->height = height;

	g_signal_emit_by_name (G_OBJECT (shape), "changed"); 
}

void
gdraw_shape_set_move_protect (GDrawShape *shape, gboolean protect)
{
	g_return_if_fail (shape != NULL);
	g_return_if_fail (GDRAW_IS_SHAPE (shape));

	shape->desc->move_protect = protect;
}

void
gdraw_shape_set_size_protect (GDrawShape *shape, gboolean protect)
{
	g_return_if_fail (shape != NULL);
	g_return_if_fail (GDRAW_IS_SHAPE (shape));

	shape->desc->size_protect = protect;
}

void
gdraw_shape_set_printable (GDrawShape *shape, gboolean printable)
{
	g_return_if_fail (shape != NULL);
	g_return_if_fail (GDRAW_IS_SHAPE (shape));

	shape->desc->printable = printable;
}

