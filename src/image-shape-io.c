/*
 * image-shape-io.c: ImageShape class PersistStream implementation
 *
 * Copyright (c) 2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 */

#include <config.h>
#include <libxml/parser.h>
#include <bonobo/bonobo-xobject.h>
#include <bonobo/Bonobo.h>
#include "shape-io.h"
#include "image-shape-io.h"

#define PARENT_TYPE BONOBO_PERSIST_TYPE

static gboolean
load_from_xml (GDrawImageShape *image, xmlNode *node)
{
	xmlNode *child;

	if (g_strcasecmp (node->name, "ImageShape"))
		return FALSE;

	for (child = node->children; child; child = child->next) {

		if (!g_strcasecmp (child->name, "ImageProperties")) {

			gchar *prop;

			prop = xmlGetProp (node, "GraphicURL");
			if (!prop)
				return FALSE;

			image->url = prop;

			image->pixbuf = gdk_pixbuf_new_from_file(
				image->url, NULL);
			if (!image->pixbuf) {
				g_free (image->url);
				image->url = NULL;
				return FALSE;
			}

		} else if (!g_strcasecmp (child->name, "ShadowProperties")) {

			if (!gdraw_shadow_properties_load_from_xml (
					image->shadow, child))
				return FALSE;

		} else if (!g_strcasecmp (child->name, "RotationDescriptor")) {

			if (!gdraw_rotation_descriptor_load_from_xml (
					image->rotation, child))
				return FALSE;

		} else if (!g_strcasecmp (child->name, "ShapeDescriptor")) {

			if (!gdraw_shape_descriptor_load_from_xml (
					GDRAW_SHAPE (image)->desc, child))
				return FALSE;

		} else {

			if (!gdraw_shape_load_from_xml (
					GDRAW_SHAPE (image), child))
				return FALSE;

		}

	}

	return TRUE;
}

static xmlNode *
save_to_xml (GDrawImageShape *image, xmlNode *parent)
{
	xmlNode *child;
	xmlNode *ip;

	child = xmlNewChild (parent, NULL, "ImageShape", NULL);

	if (!child)
		return NULL;

	ip = xmlNewChild (parent, NULL, "ImageProperties", NULL);
	if (!ip) {
		xmlFreeNode (child);
		return NULL;
	}

	xmlSetProp (ip, "GraphicURL", image->url);

	if (!gdraw_shadow_properties_save_to_xml (image->shadow, child))
		return NULL;

	if (!gdraw_rotation_descriptor_save_to_xml (image->rotation, child))
		return NULL;

	if (!gdraw_shape_save_to_xml (GDRAW_SHAPE (image), child))
		return NULL;

	return child;
}

static GDrawImageStream *
gdraw_image_stream_from_servant (PortableServer_Servant servant)
{
	return GDRAW_IMAGE_STREAM (bonobo_object_from_servant (servant));
}

static CORBA_boolean
ImageStream_is_dirty (PortableServer_Servant servant, CORBA_Environment *ev)
{
	GDrawImageStream *gis = gdraw_image_stream_from_servant (servant);

	return gis->is_dirty;
}

static void
ImageStream_load (PortableServer_Servant servant, const Bonobo_Stream stream,
		  Bonobo_Persist_ContentType type, CORBA_Environment *ev)
{
	GDrawImageStream *gis = gdraw_image_stream_from_servant (servant);
	GDrawImageShape *image = gis->image;

	if (!g_strcasecmp (type, "text/xml") || !g_strcasecmp (type, "")) {
		xmlDoc *doc;
		gchar *str = NULL;

		bonobo_stream_client_read_string (stream, &str, ev);
		doc = xmlParseMemory (str, strlen (str));
		if (!doc || !load_from_xml (image, xmlDocGetRootElement(doc)))
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
					     ex_Bonobo_Persist_WrongDataType, 
					     NULL);
		xmlFreeDoc (doc);
		g_free (str);
	} else
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
				     ex_Bonobo_NotSupported, NULL);
}

static void
ImageStream_save (PortableServer_Servant servant, const Bonobo_Stream stream,
		  Bonobo_Persist_ContentType type, CORBA_Environment *ev)
{
	GDrawImageStream *gis = gdraw_image_stream_from_servant (servant);
	GDrawImageShape *image = gis->image;

	if (!g_strcasecmp (type, "text/xml") || !g_strcasecmp (type, "")) {
		xmlDoc *doc;
		xmlChar *str;
		gint len;

		doc = xmlNewDoc ("1.0");
		doc->children = xmlNewDocNode (doc, NULL, 
					       "ImageShape", NULL);

		if( !save_to_xml (image, xmlDocGetRootElement(doc))) {
			xmlFreeDoc (doc);
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
					     ex_Bonobo_Stream_IOError, 
					     NULL);
			return;
		}
		
		xmlDocDumpMemory (doc, &str, &len);
		bonobo_stream_client_write_string (stream, str, TRUE, ev);
		xmlFreeDoc (doc);
		g_free (str);
	}
}

static Bonobo_Persist_ContentTypeList *
get_content_types (BonoboPersist *persist, CORBA_Environment *ev)
{
	return bonobo_persist_generate_content_types (1, "text/xml");
}

static void
gdraw_image_stream_class_init (GObjectClass *klass)
{
	GDrawImageStreamClass *gisc = GDRAW_IMAGE_STREAM_CLASS (klass);
	BonoboPersistClass *bpc = BONOBO_PERSIST_CLASS (klass);
	POA_Bonobo_PersistStream__epv *epv = &gisc->epv;

	bpc->get_content_types = get_content_types;

#if 0
	epv->isDirty 	= ImageStream_is_dirty;
#endif
	epv->load 	= ImageStream_load;
	epv->save 	= ImageStream_save;
}

static void
gdraw_image_stream_init (GObject *object)
{
	GDrawImageStream *image_stream = GDRAW_IMAGE_STREAM (object);

	image_stream->image = NULL;
	image_stream->is_dirty = FALSE;
}

BONOBO_TYPE_FUNC_FULL (GDrawImageStream, Bonobo_PersistStream, 
		       PARENT_TYPE, gdraw_image_stream);

GDrawImageStream *
gdraw_image_stream_new (GDrawImageShape *shape)
{
	GDrawImageStream *image_stream;

	image_stream = g_object_new(GDRAW_IMAGE_STREAM_TYPE, NULL);

	image_stream->image = shape;

	return image_stream;
}
