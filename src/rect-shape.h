/*
 * rect-shape.h: Implementation of the RectShape class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_RECT_SHAPE_H
#define __GDRAW_RECT_SHAPE_H

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-generic-factory.h>
#include <bonobo/bonobo-property-bag.h>
#include "re-shape.h"
#include "shape.h"

G_BEGIN_DECLS

typedef struct _GDrawRectShape         GDrawRectShape;
typedef struct _GDrawRectShapeClass    GDrawRectShapeClass;

#include "rect-shape-io.h"

#define GDRAW_RECT_SHAPE_TYPE          (gdraw_rect_shape_get_type ())
#define GDRAW_RECT_SHAPE(obj)          (GTK_CHECK_CAST((obj), 		\
					GDRAW_RECT_SHAPE_TYPE,		\
					GDrawRectShape))
#define GDRAW_RECT_SHAPE_CLASS(klass)  (GTK_CHECK_CLASS_CAST ((klass), 	\
                               	        GDRAW_RECT_SHAPE_TYPE, 		\
					GDrawRectShapeClass))
#define IS_GDRAW_RECT_SHAPE(obj)       (GTK_CHECK_TYPE ((obj),		\
					GDRAW_RECT_SHAPE_TYPE))
#define IS_GDRAW_RECT_SHAPE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
					  GDRAW_RECT_SHAPE_TYPE))

struct _GDrawRectShape {
	GDrawREShape	 	 parent_object;

	BonoboObject	 	*embeddable;
	BonoboPropertyBag 	*bag;
	GDrawRectStream		*stream;

	gint 			 corner_radius;
};
	
struct _GDrawRectShapeClass {
	GDrawREShapeClass parent_class;
};


GType    	   gdraw_rect_shape_get_type (void);

GDrawRectShape    *gdraw_rect_shape_new (void);

BonoboObject	  *gdraw_rect_shape_factory (BonoboGenericFactory *);

G_END_DECLS

#endif
