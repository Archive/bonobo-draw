/*
 * shape-client.c: Implementation of the Shape client helper functions
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-property-bag-client.h>
#include "shape-client.h"

gboolean
gdraw_shape_client_is_moveable (GNOME_Draw_Shape shape,
				CORBA_Environment *opt_ev)
{
	Bonobo_PropertyBag bag;
	CORBA_Environment ev, *ev_p;
	gboolean retval;

	g_return_val_if_fail (shape != CORBA_OBJECT_NIL, FALSE);

	if (opt_ev) 
		ev_p = opt_ev;
	else {
		ev_p = &ev;
		CORBA_exception_init (ev_p);
	}

	bag = Bonobo_Unknown_queryInterface(
				shape, "IDL:Bonobo/PropertyBag:1.0", ev_p);

	if (BONOBO_EX (ev_p) || (bag == CORBA_OBJECT_NIL) ||
	    bonobo_pbclient_get_boolean(bag, "MoveProtect", ev_p))
		retval = FALSE;
	else
		retval = TRUE;

	if (!opt_ev)
		CORBA_exception_free (&ev);

	return retval;
}

