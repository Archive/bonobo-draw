#include <gtk/gtkfilesel.h>
#include <glib-object.h>
#include "container-filesel.h"

static void
cancel_cb (GtkWidget *caller, GtkWidget *fs)
{
	gtk_widget_destroy (fs);
}

void
container_request_file (SampleApp    *app,
			gboolean      save,
			GCallback     cb,
			gpointer      user_data)
{
	GtkWidget *fs;

	app->fileselection = fs =
	    gtk_file_selection_new ("Select file");

	if (save)
		gtk_file_selection_show_fileop_buttons (GTK_FILE_SELECTION (fs));
	else
		gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (fs));

	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (fs)->ok_button),
			  "clicked", G_CALLBACK (cb), user_data);

	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (fs)->cancel_button),
			  "clicked", G_CALLBACK (cancel_cb), fs);

	gtk_window_set_modal (GTK_WINDOW (fs), TRUE);

	gtk_widget_show (fs);
}
