Description:

bonobo-draw is a component library based on the OpenOffice Drawing API. 
The library exposes a set of BonoboEmbeddables with corresponding 
GnomeCanvasComponents.  It is intended to be a general purpose drawing 
library for applications using the Gnome Canvas.

Using bonobo-draw:

For a very simple use example, check out the sample container app in 
the "container" directory.  This code is a modified version of the
bonobo sample container.  It produces the gdraw-canvas-container app.

The sample container app utilizes the ShapesLayer client object to
perform editing on  a Shapes collection.  It is also possible to directly
embed the Shape embeddables. The real power of bonobo-draw will be its
ability to allow you to forget about Shape editing and maintenance
though.  Put a Shapes collection in your model object, attach to it
with a ShapesLayer from your view, and you'll have a drawing layer on your
document for 10 lines of code or less.

Acknowledgments:

The project draws from code and design originally implemented in the
Achtung presentation application by my heros, Joe Shaw and Phil Schwan.
While it has been substantially altered to adapt to the OpenOffice API, 
much of the ShapesLayer is a simple namespace update of the canvas code
from Achtung's EmbeddableSlideView.

Many thanks also go to the folks at Sun for the boatload of IDL they've
made available through OpenOffice.org.  When I first started this project, 
it was my hope that it would be useful in the porting of OpenOffice to 
GNOME technologies.  Unfortunately, this now appears unlikely to occur.

Additional information:

Project information, including release tarballs, can be obtained at:

http://sourceforge.net/projects/bonobo-draw. 

If you want to live on the bleeding edge, checkout GNOME CVS module 
bonobo-draw.  If you're interested in hacking, please read the HACKING 
file first.

For more information, drop a note to:

bonobo-draw-list@lists.sourceforge.net.

