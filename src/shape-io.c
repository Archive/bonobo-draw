/*
 * shape-io.c: Shape class persistence implementation
 *
 * Copyright (c) 2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 */

#include "shape-io.h"

static gboolean
position_load_from_xml (GDrawShape *shape, xmlNode *node)
{
	gchar *prop;

	if (g_strcasecmp (node->name, "Position"))
		return FALSE;

	prop = xmlGetProp (node, "X");
	if (!prop)
		return FALSE;
	shape->x = strtod (prop, NULL);
	g_free (prop);

	prop = xmlGetProp (node, "Y");
	if (!prop)
		return FALSE;
	shape->y = strtod (prop, NULL);
	g_free (prop);

	return TRUE;
}

static gboolean
size_load_from_xml (GDrawShape *shape, xmlNode *node)
{
	gchar *prop;

	if (g_strcasecmp (node->name, "Size"))
		return FALSE;

	prop = xmlGetProp (node, "Width");
	if (!prop)
		return FALSE;
	shape->width = strtod (prop, NULL);
	g_free (prop);

	prop = xmlGetProp (node, "Height");
	if (!prop)
		return FALSE;
	shape->height = strtod (prop, NULL);
	g_free (prop);

	return TRUE;
}

gboolean
gdraw_shape_load_from_xml (GDrawShape *shape, xmlNode *node)
{
	xmlNode *child;

	if (g_strcasecmp (node->name, "Shape"))
		return FALSE;

	for (child = node->children; child; child = child->next) {
		if (!g_strcasecmp (child->name, "ShapeDesc")) {
			if (!gdraw_shape_descriptor_load_from_xml (
						shape->desc, child))
				return FALSE;
		} else if (!g_strcasecmp (child->name, "Position")) {
			if (!position_load_from_xml (shape, child))
				return FALSE;
		} else if (!g_strcasecmp (child->name, "Size")) {
			if (!size_load_from_xml (shape, child))
				return FALSE;
		} else {
			return FALSE;
		}
	}

	return TRUE;
}

static gboolean
position_save_to_xml (GDrawShape *shape, xmlNode *parent)
{
	xmlNode *child;
	gchar *prop;

	child = xmlNewChild (parent, NULL, "Position", NULL);
	if (!child)
		return FALSE;

	prop = g_strdup_printf ("%f", shape->x);
	if (!prop)
		return FALSE;
	xmlSetProp (child, "X", prop);
	g_free (prop);

	prop = g_strdup_printf ("%f", shape->y);
	if (!prop)
		return FALSE;
	xmlSetProp (child, "Y", prop);
	g_free (prop);

	return TRUE;
}

static gboolean
size_save_to_xml (GDrawShape *shape, xmlNode *parent)
{
	xmlNode *child;
	gchar *prop;

	child = xmlNewChild (parent, NULL, "Size", NULL);
	if (!child)
		return FALSE;

	prop = g_strdup_printf ("%f", shape->width);
	if (!prop)
		return FALSE;
	xmlSetProp (child, "Width", prop);
	g_free (prop);

	prop = g_strdup_printf ("%f", shape->height);
	if (!prop)
		return FALSE;
	xmlSetProp (child, "Height", prop);
	g_free (prop);

	return TRUE;
}

xmlNode *
gdraw_shape_save_to_xml (GDrawShape *shape, xmlNode *parent)
{
	xmlNode *child;

	child = xmlNewChild (parent, NULL, "Shape", NULL);

	if (!child)
		return NULL;

	if (!gdraw_shape_descriptor_save_to_xml (shape->desc, child))
		return NULL;

	if (!position_save_to_xml (shape, child))
		return NULL;

	if (!size_save_to_xml (shape, child))
		return NULL;

	return child;
}

