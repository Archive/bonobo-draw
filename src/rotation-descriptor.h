/*
 * rotation-descriptor.h: Implementation of the RotationDescriptor props
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_ROTATION_DESCRIPTOR_H
#define __GDRAW_ROTATION_DESCRIPTOR_H

#include <bonobo.h>
#include <libxml/tree.h>

G_BEGIN_DECLS

typedef struct {
  	gint32		 	 angle;
	gint32			 x;
	gint32			 y;
} GDrawRotationDescriptor;

/* RotationDescriptor manipulation API */
void	   gdraw_rotation_descriptor_add_props (BonoboPropertyBag *bag);

gboolean   gdraw_rotation_descriptor_get_prop (GDrawRotationDescriptor *rd,
					       guint arg_id, BonoboArg *arg);

gboolean   gdraw_rotation_descriptor_set_prop (GDrawRotationDescriptor *rd,
					       guint arg_id, BonoboArg *arg);

gboolean   gdraw_rotation_descriptor_load_from_xml (GDrawRotationDescriptor *,
						    xmlNode *);

xmlNode   *gdraw_rotation_descriptor_save_to_xml (GDrawRotationDescriptor *,
						  xmlNode *);

G_END_DECLS

#endif
