/*
 * main.c: GDraw factory executable
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <bonobo.h>
#include "factory.h"

#include <bonobo-activation/bonobo-activation.h>

poptContext ctx;

struct poptOption gdraw_popt_options[] = {
        { NULL, '\0', POPT_ARG_INCLUDE_TABLE, &bonobo_activation_popt_options, 
	  0, NULL, NULL },
        { NULL, '\0', 0, NULL, 0, NULL, NULL }
};

static void 
handle_corba_args (int argc, char *argv[])
{
	CORBA_Environment ev;
	CORBA_ORB orb;

	CORBA_exception_init (&ev);

	gnome_init_with_popt_table ("GDraw", VERSION, argc, argv,
				    gdraw_popt_options, 0, NULL);

	if (!bonobo_init(&argc, argv))
		g_error ("Cannot initialize bonobo\n");
}

int 
main (int argc, char *argv[])
{
	handle_corba_args (argc, argv);
	gdk_rgb_init ();
	bonobo_activate ();
	gdraw_factory_init ();
	bonobo_main ();
	return 0;
}
