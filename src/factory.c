/*
 * factory.c: Implements the GDraw factory object
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Authors: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <bonobo.h>
#include <gdraw/oafiids.h>
#include "factory.h"
#include "ellipse-shape.h"
#include "image-shape.h"
#include "rect-shape.h"
#include "shapes.h"

static BonoboGenericFactory *gdraw_factory_object = NULL;

static BonoboObject *
gdraw_factory (BonoboGenericFactory *this, const char *oafiid)
{
	BonoboObject *object = NULL;

	if (!g_strcasecmp (oafiid, GDRAW_RECT_OAFIID))
		object = gdraw_rect_shape_factory (this);
	else if (!g_strcasecmp (oafiid, GDRAW_ELLIPSE_OAFIID))
		object = gdraw_ellipse_shape_factory (this);
	else if (!g_strcasecmp (oafiid, GDRAW_IMAGE_OAFIID))
		object = gdraw_image_shape_factory (this);
	else if (!g_strcasecmp (oafiid, GDRAW_SHAPES_OAFIID))
		object = gdraw_shapes_factory (this);
	else
		g_warning ("Unexpected IID: %s", oafiid);

	return object;
}

void
gdraw_factory_init ()
{
	if (gdraw_factory_object != NULL) {
		g_warning ("Tried to reinitialize running factory");
		return;
	}

	gdraw_factory_object = bonobo_generic_factory_new (
		GDRAW_FACTORY_OAFIID, 
		(BonoboFactoryCallback) gdraw_factory,
		NULL);

	if (!gdraw_factory_object)
		g_error ("Unable to register GDraw object factory");

}
