/*
 * ellipse-shape-io.h: EllipseShape PersistStream implementation
 *
 * Copyright (c) 2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_ELLIPSE_SHAPE_IO_H
#define __GDRAW_ELLIPSE_SHAPE_IO_H

#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-persist.h>

G_BEGIN_DECLS

typedef struct _GDrawEllipseStream      GDrawEllipseStream;
typedef struct _GDrawEllipseStreamClass GDrawEllipseStreamClass;

#include "ellipse-shape.h"

#define GDRAW_ELLIPSE_STREAM_TYPE      	 (gdraw_ellipse_stream_get_type())
#define GDRAW_ELLIPSE_STREAM(o)          (GTK_CHECK_CAST((o), GDRAW_ELLIPSE_STREAM_TYPE, GDrawEllipseStream))
#define GDRAW_ELLIPSE_STREAM_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDRAW_ELLIPSE_STREAM_TYPE, GDrawEllipseStreamClass))
#define GDRAW_IS_ELLIPSE_STREAM(o)       (GTK_CHECK_TYPE((o), GDRAW_ELLIPSE_STREAM_TYPE))
#define GDRAW_IS_ELLIPSE_STREAM_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), GDRAW_ELLIPSE_STREAM_TYPE))


struct _GDrawEllipseStream {
	BonoboPersist	 	base;

	GDrawEllipseShape      *ellipse;
	gboolean		is_dirty;
};
	
struct _GDrawEllipseStreamClass {
	BonoboPersistClass parent_class;

	POA_Bonobo_PersistStream__epv epv;
};

GtkType gdraw_ellipse_stream_get_type (void);

GDrawEllipseStream *gdraw_ellipse_stream_new (GDrawEllipseShape *);

G_END_DECLS

#endif
