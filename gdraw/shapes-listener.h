/*
 * shapes-listener.h: Interface for the ShapesListener class
 *
 * Copyright (c) 2000-2001 Mike Kestner 
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_SHAPES_LISTENER_H
#define __GDRAW_SHAPES_LISTENER_H

#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-object.h>

G_BEGIN_DECLS

#define GDRAW_SHAPES_LISTENER_TYPE        (gdraw_shapes_listener_get_type())
#define GDRAW_SHAPES_LISTENER(o)          (GTK_CHECK_CAST((o), GDRAW_SHAPES_LISTENER_TYPE, GDrawShapesListener))
#define GDRAW_SHAPES_LISTENER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDRAW_SHAPES_LISTENER_TYPE, GDrawShapesListenerClass))
#define GDRAW_IS_SHAPES_LISTENER(o)       (GTK_CHECK_TYPE((o), GDRAW_SHAPES_LISTENER_TYPE))
#define GDRAW_IS_SHAPES_LISTENER_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), GDRAW_SHAPES_LISTENER_TYPE))

typedef struct _GDrawShapesListener      GDrawShapesListener;
typedef struct _GDrawShapesListenerClass GDrawShapesListenerClass;

struct _GDrawShapesListener {
	BonoboObject	 	base;
};
	
struct _GDrawShapesListenerClass {
	BonoboObjectClass parent_class;

	POA_Bonobo_Listener__epv epv;

	void (* shape_added) (const gchar *name, GNOME_Draw_Shape shape);
	void (* shape_removed) (const gchar *name);
};

GtkType gdraw_shapes_listener_get_type (void);

GDrawShapesListener *gdraw_shapes_listener_new (void);

G_END_DECLS

#endif /* __GDRAW_SHAPES_LISTENER_H */
