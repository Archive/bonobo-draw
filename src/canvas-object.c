/*
 * canvas-object.c: Implementation of the CanvasObject class
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <libgnomecanvas/gnome-canvas.h>
#include "canvas-object.h"

#define IS_TOP_BOX(x) ((x == 0) || (x == 1) || (x == 2))
#define IS_BOTTOM_BOX(x) ((x == 4) || (x == 5) || (x == 6))
#define IS_LEFT_BOX(x) ((x == 0) || (x == 6) || (x == 7))
#define IS_RIGHT_BOX(x) ((x == 2) || (x == 3) || (x == 4))

#define ICLASS(x) GNOME_CANVAS_ITEM_CLASS (GTK_OBJECT (x)->klass)

static void
update_handles (GDrawCanvasObject *cobj)
{
	GnomeCanvasItem *o;
	int i;

	for (i = 0; i < 8; i++) {
		double x, y;

		if (IS_LEFT_BOX (i))
			x = 0.0;
		else if (IS_RIGHT_BOX (i))
			x = cobj->shape->width;
		else
			x = cobj->shape->width / 2.0;

		if (IS_TOP_BOX (i))
			y = 0.0;
		else if (IS_BOTTOM_BOX (i))
			y = cobj->shape->height;
		else
			y = cobj->shape->height / 2.0;

		o = cobj->handles[i];
		if (o == NULL) {
			gnome_canvas_item_new (
				GNOME_CANVAS_GROUP (cobj->object_group),
				gnome_canvas_rect_get_type (),
				"outline_color", "black",
				"fill_color", "grey",
				NULL);
			gnome_canvas_item_show (o);
			cobj->handles[i] = o;
		}

		gnome_canvas_item_set (
			cobj->handles[i],
			"x1", x - (3.0 / o->canvas->pixels_per_unit),
			"y1", y - (3.0 / o->canvas->pixels_per_unit),
			"x2", x + (3.0 / o->canvas->pixels_per_unit),
			"y2", y + (3.0 / o->canvas->pixels_per_unit),
			NULL);
	}
}

static void
gdraw_canvas_object_destroy (GtkObject *object)
{
	GDrawCanvasObject *cobj = GDRAW_CANVAS_OBJECT (object);

	gtk_object_destroy (GTK_OBJECT (cobj->object_group));
}

static void
gdraw_canvas_object_class_init (GtkObjectClass *klass)
{
	klass->destroy = gdraw_canvas_object_destroy;
}

static void
gdraw_canvas_object_init (GtkObject *object)
{
	GDrawCanvasObject *cobj;
	gint i;

	g_return_if_fail (object);

	cobj = GDRAW_CANVAS_OBJECT (object);

	cobj->shape = NULL;
	cobj->object_group = NULL;
	cobj->component = NULL;

	for (i = 0; i < 8; i++)
		cobj->handles[i] = NULL;
}

GtkType
gdraw_canvas_object_get_type (void)
{
	static GtkType cobj_type = 0;

	if (!cobj_type) {
		static const GtkTypeInfo cobj_info = {
			"GDrawCanvasObject",
			sizeof (GDrawCanvasObject),
			sizeof (GDrawCanvasObjectClass),
			(GtkClassInitFunc) gdraw_canvas_object_class_init,
			(GtkObjectInitFunc) gdraw_canvas_object_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		cobj_type = gtk_type_unique (gtk_object_get_type (), 
					     &cobj_info);
	}

	return cobj_type;
}

/**
 * gdraw_canvas_object_construct:
 * @cobj: A #GDrawCanvasObject.
 * @canvas: The server-side #GnomeCanvas.
 * @shape: The #GDrawShape object that this canvas object displays.
 *
 * Creates the top-level CanvasGroup for the object and stores the shape.
 */
void
gdraw_canvas_object_construct (GDrawCanvasObject *cobj, GnomeCanvas *canvas,
			       GDrawShape *shape)
{
	g_return_if_fail (cobj != NULL);
	g_return_if_fail (GDRAW_IS_CANVAS_OBJECT (cobj));
	g_return_if_fail (canvas != NULL);
	g_return_if_fail (shape != NULL);
	g_return_if_fail (GDRAW_IS_SHAPE (shape));

	cobj->shape = shape;

	cobj->object_group = gnome_canvas_item_new (
					gnome_canvas_root (canvas),
					gnome_canvas_group_get_type (),
					"x", shape->x, "y", shape->y, NULL);
}

/**
 * gdraw_canvas_object_add_handles:
 * @cobj: A #GDrawCanvasObject.
 *
 * Adds the handles to a newly selected canvas object.
 */
void
gdraw_canvas_object_add_handles (GDrawCanvasObject *cobj)
{
	int i;

	for (i = 0; i < 8; i++) {
		if (cobj->handles[i])
			continue;

		cobj->handles[i] = gnome_canvas_item_new (
			GNOME_CANVAS_GROUP (cobj->object_group),
			gnome_canvas_rect_get_type (),
			"outline_color", "black",
			"fill_color", "grey",
			NULL);
		g_object_set_data (G_OBJECT (cobj->handles[i]), "index",
				   GINT_TO_POINTER (i));
	}
}

/**
 * gdraw_canvas_object_remove_handles:
 * @cobj: A #GDrawCanvasObject.
 *
 * Removes the handles from a selected canvas object.
 */
void
gdraw_canvas_object_remove_handles (GDrawCanvasObject *cobj)
{
	int i;

	for (i = 0; i < 8; i++)
		if (cobj->handles[i]) {
			gtk_object_destroy (GTK_OBJECT (cobj->handles[i]));
			cobj->handles[i] = NULL;
		}
}

/**
 * gdraw_canvas_object_update_handles:
 * @cobj: A #GDrawCanvasObject.
 *
 * Updates the handles on a selected canvas object.
 */
void
gdraw_canvas_object_update_handles (GDrawCanvasObject *cobj)
{
	if (cobj->handles[0])
		update_handles (cobj);
}

/**
 * gdraw_canvas_object_set_component:
 * @cobj: A #GDrawCanvasObject.
 * @bcc: The #BonoboCanvasComponent for this canvas object.
 *
 * Sets the component that will communicate with the client canvas.
 */
void
gdraw_canvas_object_set_component (GDrawCanvasObject *cobj, 
				   BonoboCanvasComponent *bcc)
{
	cobj->component = bcc;
}

