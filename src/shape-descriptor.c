/*
 * shape-descriptor.c: Implementation of the ShapeDescriptor properties
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include "properties.h"
#include "shape-descriptor.h"

void
gdraw_shape_descriptor_add_props (BonoboPropertyBag *bag)
{
	bonobo_property_bag_add (bag, "LayerID", PROP_LAYER_ID,
				 BONOBO_ARG_INT, NULL,
				 "ID of the owning Layer", 0);

	bonobo_property_bag_add (bag, "LayerName", PROP_LAYER_NAME,
				 BONOBO_ARG_STRING, NULL,
				 "Name of the owning layer", 0);

	bonobo_property_bag_add (bag, "Printable", PROP_PRINTABLE,
				 BONOBO_ARG_BOOLEAN, NULL,
				 "Whether or not the shape is printable", 0);

	bonobo_property_bag_add (bag, "MoveProtect", PROP_MOVE_PROTECT,
				 BONOBO_ARG_BOOLEAN, NULL,
				 "Whether or not the shape can be moved", 0);

	bonobo_property_bag_add (bag, "SizeProtect", PROP_SIZE_PROTECT,
				 BONOBO_ARG_BOOLEAN, NULL,
				 "Whether or not the shape can be resized", 0);

	bonobo_property_bag_add (bag, "Name", PROP_NAME,
				 BONOBO_ARG_STRING, NULL,
				 "Name of the shape", 0);

}

gboolean
gdraw_shape_descriptor_get_prop (GDrawShapeDescriptor *sd, 
				 guint arg_id, BonoboArg *arg)
{
	switch (arg_id) {

	case PROP_LAYER_ID:
		BONOBO_ARG_SET_INT (arg, sd->layer_id);
		break;
	case PROP_LAYER_NAME:
		BONOBO_ARG_SET_STRING (arg, sd->layer_name);
		break;
	case PROP_PRINTABLE:
		BONOBO_ARG_SET_BOOLEAN (arg, sd->printable);
		break;
	case PROP_MOVE_PROTECT:
		BONOBO_ARG_SET_BOOLEAN (arg, sd->move_protect);
		break;
	case PROP_SIZE_PROTECT:
		BONOBO_ARG_SET_BOOLEAN (arg, sd->size_protect);
		break;
	case PROP_NAME:
		BONOBO_ARG_SET_STRING (arg, sd->name);
		break;
	default:
		return FALSE;
	}

	return TRUE;
}
gboolean
gdraw_shape_descriptor_set_prop (GDrawShapeDescriptor *sd, 
				 guint arg_id, BonoboArg *arg)
{
	switch (arg_id) {

	case PROP_LAYER_ID:
		sd->layer_id = BONOBO_ARG_GET_INT (arg);
		break;
	case PROP_LAYER_NAME:
		sd->layer_name = BONOBO_ARG_GET_STRING (arg);
		break;
	case PROP_PRINTABLE:
		sd->printable = BONOBO_ARG_GET_BOOLEAN (arg);
		break;
	case PROP_MOVE_PROTECT:
		sd->move_protect = BONOBO_ARG_GET_BOOLEAN (arg);
		break;
	case PROP_SIZE_PROTECT:
		sd->size_protect = BONOBO_ARG_GET_BOOLEAN (arg);
		break;
	case PROP_NAME:
		sd->name = BONOBO_ARG_GET_STRING (arg);
		break;
	default:
		return FALSE;
	}

	return TRUE;
}

gboolean
gdraw_shape_descriptor_load_from_xml (GDrawShapeDescriptor *sd, xmlNode *node)
{
	gchar *prop;

	if (g_strcasecmp (node->name, "ShapeDesc"))
		return FALSE;

	prop = xmlGetProp (node, "Name");
	if (!prop)
		return FALSE;
	sd->name = prop;

	prop = xmlGetProp (node, "LayerID");
	if (!prop)
		return FALSE;
	sd->layer_id = strtol (prop, NULL, 10);
	g_free (prop);

	prop = xmlGetProp (node, "LayerName");
	if (!prop)
		return FALSE;
	sd->layer_name = prop;

	prop = xmlGetProp (node, "Printable");
	if (!prop)
		return FALSE;
	sd->printable = g_strcasecmp (prop, "0") ? TRUE : FALSE;
	g_free (prop);

	prop = xmlGetProp (node, "MoveProtect");
	if (!prop)
		return FALSE;
	sd->move_protect = g_strcasecmp (prop, "0") ? TRUE : FALSE;
	g_free (prop);

	prop = xmlGetProp (node, "SizeProtect");
	if (!prop)
		return FALSE;
	sd->size_protect = g_strcasecmp (prop, "0") ? TRUE : FALSE;
	g_free (prop);

	return TRUE;
}

xmlNode *
gdraw_shape_descriptor_save_to_xml (GDrawShapeDescriptor *sd, xmlNode *parent)
{
	xmlNode *child;
	gchar *prop;

	child = xmlNewChild (parent, NULL, "ShapeDesc", NULL);
	if (!child)
		return FALSE;

	if (sd->name)
		xmlSetProp (child, "Name", sd->name);

	if (sd->layer_name)
		xmlSetProp (child, "LayerName", sd->name);
	else
		xmlSetProp (child, "LayerName", "");

	prop = g_strdup_printf ("%d", sd->layer_id);
	if (!prop) {
		xmlFreeNode (child);
		return FALSE;
	}
	xmlSetProp (child, "LayerID", prop);
	g_free (prop);

	prop = g_strdup (sd->printable ? "1" : "0");
	if (!prop) {
		xmlFreeNode (child);
		return FALSE;
	}
	xmlSetProp (child, "Printable", prop);
	g_free (prop);

	prop = g_strdup (sd->move_protect ? "1" : "0");
	if (!prop) {
		xmlFreeNode (child);
		return FALSE;
	}
	xmlSetProp (child, "MoveProtect", prop);
	g_free (prop);

	prop = g_strdup (sd->size_protect ? "1" : "0");
	if (!prop) {
		xmlFreeNode (child);
		return FALSE;
	}
	xmlSetProp (child, "SizeProtect", prop);
	g_free (prop);

	return child;
}
