/*
 * image-shape-io.h: ImageShape PersistStream implementation
 *
 * Copyright (c) 2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_IMAGE_SHAPE_IO_H
#define __GDRAW_IMAGE_SHAPE_IO_H

#include <bonobo/bonobo-persist.h>
#include <bonobo/Bonobo.h>

G_BEGIN_DECLS

typedef struct _GDrawImageStream      GDrawImageStream;
typedef struct _GDrawImageStreamClass GDrawImageStreamClass;

#include "image-shape.h"

#define GDRAW_IMAGE_STREAM_TYPE      	 (gdraw_image_stream_get_type())
#define GDRAW_IMAGE_STREAM(o)          (GTK_CHECK_CAST((o), GDRAW_IMAGE_STREAM_TYPE, GDrawImageStream))
#define GDRAW_IMAGE_STREAM_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDRAW_IMAGE_STREAM_TYPE, GDrawImageStreamClass))
#define GDRAW_IS_IMAGE_STREAM(o)       (GTK_CHECK_TYPE((o), GDRAW_IMAGE_STREAM_TYPE))
#define GDRAW_IS_IMAGE_STREAM_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), GDRAW_IMAGE_STREAM_TYPE))


struct _GDrawImageStream {
	BonoboPersist	 	base;

	GDrawImageShape		*image;
	gboolean		is_dirty;
};
	
struct _GDrawImageStreamClass {
	BonoboPersistClass parent_class;

	POA_Bonobo_PersistStream__epv epv;
};

GType gdraw_image_stream_get_type (void);

GDrawImageStream *gdraw_image_stream_new (GDrawImageShape *);

G_END_DECLS

#endif
