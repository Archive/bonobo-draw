/*
 * line-properties.h: Implementation of the LineProperties
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 */

#ifndef __GDRAW_LINE_PROPERTIES_H
#define __GDRAW_LINE_PROPERTIES_H

#include <libxml/tree.h>

G_BEGIN_DECLS

typedef struct {
	GNOME_Draw_LineStyle	 style;
	guint32		 	 rgba;
	gint32			 width;
} GDrawLineProperties;

/* LineProperties manipulation API */
void	   gdraw_line_properties_add_props (BonoboPropertyBag *bag);

gboolean   gdraw_line_properties_get_prop (GDrawLineProperties *,
					   guint arg_id, BonoboArg *arg);

gboolean   gdraw_line_properties_set_prop (GDrawLineProperties *,
					   guint arg_id, BonoboArg *arg);

gboolean   gdraw_line_properties_load_from_xml (GDrawLineProperties *,
						xmlNode *);

xmlNode   *gdraw_line_properties_save_to_xml (GDrawLineProperties *,
					      xmlNode *);

G_END_DECLS

#endif
