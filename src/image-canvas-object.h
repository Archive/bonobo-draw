/*
 * image-canvas-object.h: Implementation of the ImageCanvasObject class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_IMAGE_CANVAS_OBJECT_H
#define __GDRAW_IMAGE_CANVAS_OBJECT_H

#include <gtk/gtktypeutils.h>
#include <gdraw/GNOME_Draw.h>

G_BEGIN_DECLS

typedef struct _GDrawImageCanvasObject         GDrawImageCanvasObject;
typedef struct _GDrawImageCanvasObjectClass    GDrawImageCanvasObjectClass;

#include "canvas-object.h"
#include "image-shape.h"

#define GDRAW_IMAGE_CANVAS_OBJECT_TYPE  (gdraw_image_canvas_object_get_type())
#define GDRAW_IMAGE_CANVAS_OBJECT(obj)  (GTK_CHECK_CAST((obj), GDRAW_IMAGE_CANVAS_OBJECT_TYPE, GDrawImageCanvasObject))
#define GDRAW_IMAGE_CANVAS_OBJECT_CLASS(klass)  (GTK_CHECK_CLASS_CAST((klass), GDRAW_IMAGE_CANVAS_OBJECT_TYPE, GDrawImageCanvasObjectClass))
#define IS_GDRAW_IMAGE_CANVAS_OBJECT(obj)       (GTK_CHECK_TYPE((obj), GDRAW_IMAGE_CANVAS_OBJECT_TYPE))
#define IS_GDRAW_IMAGE_CANVAS_OBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GDRAW_IMAGE_CANVAS_OBJECT_TYPE))

struct _GDrawImageCanvasObject {
	GDrawCanvasObject 	parent_object;

	GnomeCanvasItem 	*canvas_item;
	GnomeCanvasItem 	*shadow_item;
};
	
struct _GDrawImageCanvasObjectClass {
	GDrawCanvasObjectClass parent_class;
};

GtkType gdraw_image_canvas_object_get_type (void);

GDrawCanvasObject *gdraw_image_canvas_object_new (GDrawImageShape *,
						  GnomeCanvas *);

G_END_DECLS

#endif

