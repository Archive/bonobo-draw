/*
 * canvas-object.h: Implementation of the CanvasObject abstract class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_CANVAS_OBJECT_H
#define __GDRAW_CANVAS_OBJECT_H

#include <libgnomecanvas/gnome-canvas.h>
#include <gdraw/GNOME_Draw.h>

G_BEGIN_DECLS

typedef struct _GDrawCanvasObject         GDrawCanvasObject;
typedef struct _GDrawCanvasObjectClass    GDrawCanvasObjectClass;

#include "shape.h"

#define GDRAW_CANVAS_OBJECT_TYPE            (gdraw_canvas_object_get_type())
#define GDRAW_CANVAS_OBJECT(obj)            (GTK_CHECK_CAST((obj),	\
					     GDRAW_CANVAS_OBJECT_TYPE, 	\
					     GDrawCanvasObject))
#define GDRAW_CANVAS_OBJECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST((klass), \
                               	     	     GDRAW_CANVAS_OBJECT_TYPE, 	    \
					     GDrawCanvasObjectClass))
#define GDRAW_IS_CANVAS_OBJECT(obj)         (GTK_CHECK_TYPE((obj), 	\
					     GDRAW_CANVAS_OBJECT_TYPE))
#define GDRAW_IS_CANVAS_OBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), \
					     GDRAW_CANVAS_OBJECT_TYPE))

struct _GDrawCanvasObject {
	GtkObject 		parent_object;

	GDrawShape 		*shape;
	GnomeCanvasItem 	*object_group;
	BonoboCanvasComponent 	*component;
	GnomeCanvasItem 	*handles[8];

};
	
struct _GDrawCanvasObjectClass {
	GtkObjectClass parent_class;
};

/* Standard GtkObject function */
GtkType gdraw_canvas_object_get_type (void);

void    gdraw_canvas_object_construct (GDrawCanvasObject *, GnomeCanvas *, 
				       GDrawShape *);

void	gdraw_canvas_object_set_component (GDrawCanvasObject *, 
					   BonoboCanvasComponent *);

void 	gdraw_canvas_object_add_handles (GDrawCanvasObject *);
void 	gdraw_canvas_object_remove_handles (GDrawCanvasObject *);
void 	gdraw_canvas_object_update_handles (GDrawCanvasObject *);

G_END_DECLS

#endif
