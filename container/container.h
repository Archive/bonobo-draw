#ifndef SAMPLE_CONTAINER_H
#define SAMPLE_CONTAINER_H

#include <bonobo/bonobo-ui-container.h>
#include <gdraw/shapes-layer.h>

typedef struct _SampleApp        SampleApp;
typedef struct _SampleClientSite SampleClientSite;

struct _SampleApp {
	BonoboUIContainer   *ui_container;

	Bonobo_Unknown       shapes;
	GDrawShapesLayer    *layer;

	GtkWidget           *app;
	GtkWidget           *canvas;
	GtkWidget           *fileselection;
};

void              sample_app_exit             (SampleApp        *app);

#endif /* SAMPLE_CONTAINER_H */

