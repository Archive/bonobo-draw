/*
 * image-canvas-object.c: Implementation of the ImageCanvasObject class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <libgnomecanvas/gnome-canvas-rect-ellipse.h>
#include <libgnomecanvas/gnome-canvas-pixbuf.h>
#include "image-canvas-object.h"


static void
gdraw_image_canvas_object_destroy (GtkObject *object)
{
	GDrawCanvasObject *cobj = GDRAW_CANVAS_OBJECT (object);

	gtk_object_destroy (GTK_OBJECT (cobj->object_group));
}

static void
image_update_shadow (GDrawImageCanvasObject *rcobj, 
		    GDrawShadowProperties *shadow)
{
	GDrawCanvasObject *cobj = GDRAW_CANVAS_OBJECT (rcobj);
	GDrawShape *shape = cobj->shape;

	if (shadow->visible) {
		if (rcobj->shadow_item)
			rcobj->shadow_item = gnome_canvas_item_new (
				GNOME_CANVAS_GROUP (cobj->object_group),
				gnome_canvas_rect_get_type (),
				"x1", shadow->x_dist,
				"y1", shadow->y_dist,
				"x2", shape->width + shadow->x_dist,
				"y2", shape->height + shadow->y_dist,
				"fill_color_rgba", shadow->rgba, NULL);
		else
			gnome_canvas_item_set (
				rcobj->shadow_item,
				"x1", shadow->x_dist,
				"y1", shadow->y_dist,
				"x2", shape->width + shadow->x_dist,
				"y2", shape->height + shadow->y_dist,
				"fill_color_rgba", shadow->rgba, NULL);
	} else if (rcobj->shadow_item) {
		gtk_object_destroy (GTK_OBJECT (rcobj->shadow_item));
		rcobj->shadow_item = NULL;
	}
}

static void
redraw_image_cobj (GDrawShape *shape, GDrawCanvasObject *cobj)
{
	GDrawImageShape *image = GDRAW_IMAGE_SHAPE (cobj->shape);
	GDrawImageCanvasObject *icobj = GDRAW_IMAGE_CANVAS_OBJECT (cobj);
	gdouble affine[6];

	art_affine_translate (affine, cobj->shape->x, cobj->shape->y);

	gnome_canvas_item_affine_absolute (cobj->object_group, affine);

	gnome_canvas_item_set (icobj->canvas_item,
			      "x", 0.0, "y", 0.0,
			      "width_set", TRUE, "height_set", TRUE,
			      "width", cobj->shape->width,
			      "height", cobj->shape->height,
			      "pixbuf", image->pixbuf,
			      NULL);

	image_update_shadow (icobj, image->shadow);
}

static void
gdraw_image_canvas_object_class_init (GtkObjectClass *klass)
{
	klass->destroy = gdraw_image_canvas_object_destroy;
}

static void
gdraw_image_canvas_object_init (GtkObject *object)
{
	GDrawImageCanvasObject *cobj;

	g_return_if_fail (object);

	cobj = GDRAW_IMAGE_CANVAS_OBJECT (object);

	cobj->canvas_item = NULL;
	cobj->shadow_item = NULL;
}

GtkType
gdraw_image_canvas_object_get_type (void)
{
	static GtkType cobj_type = 0;

	if (!cobj_type) {
		static const GtkTypeInfo cobj_info = {
			"GDrawImageCanvasObject",
			sizeof (GDrawImageCanvasObject),
			sizeof (GDrawImageCanvasObjectClass),
			(GtkClassInitFunc) gdraw_image_canvas_object_class_init,
			(GtkObjectInitFunc) gdraw_image_canvas_object_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		cobj_type = gtk_type_unique (gdraw_canvas_object_get_type (), 
					     &cobj_info);
	}

	return cobj_type;
}

static void
create_image_cobj (GDrawImageCanvasObject *rcobj)
{
	GDrawCanvasObject *cobj = GDRAW_CANVAS_OBJECT (rcobj);

	rcobj->canvas_item = gnome_canvas_item_new (
				GNOME_CANVAS_GROUP (cobj->object_group),
				gnome_canvas_pixbuf_get_type (), NULL);

	redraw_image_cobj (cobj->shape, cobj);
}

GDrawCanvasObject *
gdraw_image_canvas_object_new (GDrawImageShape *image, 
			       GnomeCanvas *canvas)
{
	GDrawCanvasObject *cobj;
	GDrawImageCanvasObject *icobj;

	g_return_val_if_fail (image != NULL, NULL);
	g_return_val_if_fail (canvas != NULL, NULL);

	icobj = gtk_type_new (GDRAW_IMAGE_CANVAS_OBJECT_TYPE);

	cobj = GDRAW_CANVAS_OBJECT (icobj);

	gdraw_canvas_object_construct (cobj, canvas, GDRAW_SHAPE (image));

	gtk_signal_connect (GTK_OBJECT (image), "changed",
			    GTK_SIGNAL_FUNC (redraw_image_cobj), cobj);

	create_image_cobj (icobj);

	return cobj;
}
