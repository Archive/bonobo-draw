/*
 * shadow-properties.c: Implementation of the ShadowProperties
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 */

#include <config.h>
#include "properties.h"
#include "shadow-properties.h"

void
gdraw_shadow_properties_add_props (BonoboPropertyBag *bag)
{
	bonobo_property_bag_add (bag, "Shadow", PROP_SHADOW,
				 BONOBO_ARG_BOOLEAN, NULL,
				 "Whether or not to draw a shadow", 0);

	bonobo_property_bag_add (bag, "ShadowColor", PROP_SHADOW_COLOR,
				 BONOBO_ARG_LONG, NULL,
				 "RGB color of the shadow", 0);

	bonobo_property_bag_add (bag, "ShadowTransparency", 
				 PROP_SHADOW_TRANSPARENCY,
				 BONOBO_ARG_INT, NULL,
				 "Alpha transparency of the shadow", 0);

	bonobo_property_bag_add (bag, "ShadowRGBA", PROP_SHADOW_RGBA,
				 BONOBO_ARG_LONG, NULL,
				 "RGBA color of the shadow", 0);

	bonobo_property_bag_add (bag, "ShadowXDistance", 
				 PROP_SHADOW_X_DISTANCE,
				 BONOBO_ARG_LONG, NULL,
				 "X offset of the shadow", 0);

	bonobo_property_bag_add (bag, "ShadowYDistance", 
				 PROP_SHADOW_Y_DISTANCE,
				 BONOBO_ARG_LONG, NULL,
				 "Y offset of the shadow", 0);
}

gboolean
gdraw_shadow_properties_get_prop (GDrawShadowProperties *shadow, 
				  guint arg_id, BonoboArg *arg)
{
	switch (arg_id) {

	case PROP_SHADOW:
		BONOBO_ARG_SET_BOOLEAN (arg, shadow->visible);
		break;
	case PROP_SHADOW_COLOR:
		BONOBO_ARG_SET_LONG (arg, shadow->rgba >> 8);
		break;
	case PROP_SHADOW_TRANSPARENCY:
		BONOBO_ARG_SET_INT (arg, shadow->rgba & 0xf);
		break;
	case PROP_SHADOW_RGBA:
		BONOBO_ARG_SET_LONG (arg, shadow->rgba);
		break;
	case PROP_SHADOW_X_DISTANCE:
		BONOBO_ARG_SET_LONG (arg, shadow->x_dist);
		break;
	case PROP_SHADOW_Y_DISTANCE:
		BONOBO_ARG_SET_LONG (arg, shadow->y_dist);
		break;
	default:
		return FALSE;
	}

	return TRUE;
}
gboolean
gdraw_shadow_properties_set_prop (GDrawShadowProperties *shadow, 
				  guint arg_id, BonoboArg *arg)
{
	switch (arg_id) {

	case PROP_SHADOW:
		shadow->visible = BONOBO_ARG_GET_BOOLEAN (arg);
		break;
	case PROP_SHADOW_COLOR:
		shadow->rgba = (BONOBO_ARG_GET_LONG (arg) << 8) +
			       (shadow->rgba & 0xf);
		break;
	case PROP_SHADOW_TRANSPARENCY:
		shadow->rgba = (BONOBO_ARG_GET_INT (arg) & 0xf) +
			       (shadow->rgba & 0xfff0);
		break;
	case PROP_SHADOW_RGBA:
		shadow->rgba = BONOBO_ARG_GET_LONG (arg);
		break;
	case PROP_SHADOW_X_DISTANCE:
		shadow->x_dist = BONOBO_ARG_GET_LONG (arg);
		break;
	case PROP_SHADOW_Y_DISTANCE:
		shadow->y_dist = BONOBO_ARG_GET_LONG (arg);
		break;
	default:
		return FALSE;
	}

	return TRUE;
}

gboolean
gdraw_shadow_properties_load_from_xml (GDrawShadowProperties *shadow, 
				       xmlNode *node)
{
	gchar *prop;

	if (g_strcasecmp (node->name, "ShadowProperties"))
		return FALSE;

	prop = xmlGetProp (node, "Visible");
	if (!prop)
		return FALSE;

	shadow->visible = (g_strcasecmp (prop, "1") ? FALSE : TRUE);
	g_free (prop);

	if (!shadow->visible)
		return TRUE;

	prop = xmlGetProp (node, "Color");
	if (prop)
		shadow->rgba = strtoul (prop, NULL, 16);
	else {
		shadow->rgba = 0;
		free (prop);
	}

	prop = xmlGetProp (node, "XDist");
	if (prop)
		shadow->x_dist = strtol (prop, NULL, 10);
	else {
		shadow->x_dist = 0;
		free (prop);
	}

	prop = xmlGetProp (node, "YDist");
	if (prop)
		shadow->y_dist = strtol (prop, NULL, 10);
	else {
		shadow->y_dist = 0;
		free (prop);
	}

	return TRUE;
}

xmlNode *
gdraw_shadow_properties_save_to_xml (GDrawShadowProperties *shadow, 
				       xmlNode *node)
{
	xmlNode *child;
	gchar *prop;

	child = xmlNewChild (node, NULL, "ShadowProperties", NULL);

	if (!child)
		return NULL;

	prop = g_strdup (shadow->visible ? "1" : "0");
	if (!prop) {
		xmlFreeNode (child);
		return NULL;
	}
	xmlSetProp (child, "Visible", prop);
	g_free (prop);

	if (!shadow->visible)
		return child;

	prop = g_strdup_printf ("%x", shadow->rgba);
	if (!prop) {
		xmlFreeNode (child);
		return NULL;
	}
	xmlSetProp (child, "Color", prop);
	g_free (prop);

	prop = g_strdup_printf ("%d", shadow->x_dist);
	if (!prop) {
		xmlFreeNode (child);
		return NULL;
	}
	xmlSetProp (child, "XDist", prop);
	g_free (prop);

	prop = g_strdup_printf ("%x", shadow->y_dist);
	if (!prop) {
		xmlFreeNode (child);
		return NULL;
	}
	xmlSetProp (child, "YDist", prop);
	g_free (prop);

	return child;
}

