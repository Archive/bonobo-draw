/*
 * ellipse-shape-io.c: EllipseShape class PersistStream implementation
 *
 * Copyright (c) 2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 */

#include <config.h>
#include <libxml/parser.h>
#include <bonobo/bonobo-persist.h>
#include <bonobo/Bonobo.h>
#include "re-shape-io.h"
#include "ellipse-shape-io.h"

#define PARENT_TYPE BONOBO_PERSIST_TYPE

static gboolean
load_from_xml (GDrawEllipseShape *ellipse, xmlNode *node)
{
	xmlNode *child;

	if (g_strcasecmp (node->name, "EllipseShape"))
		return FALSE;

	for (child = node->children; child; child = child->next) {

		if (!gdraw_re_shape_load_from_xml (
					GDRAW_RE_SHAPE (ellipse), child))
				return FALSE;

	}

	return TRUE;
}

static xmlNode *
save_to_xml (GDrawEllipseShape *ellipse, xmlNode *parent)
{
	if (!gdraw_re_shape_save_to_xml (GDRAW_RE_SHAPE (ellipse), parent))
		return NULL;

	return parent;
}

static GDrawEllipseStream *
gdraw_ellipse_stream_from_servant (PortableServer_Servant servant)
{
	return GDRAW_ELLIPSE_STREAM (bonobo_object_from_servant (servant));
}

static CORBA_boolean
EllipseStream_is_dirty (PortableServer_Servant servant, CORBA_Environment *ev)
{
	GDrawEllipseStream *ges = gdraw_ellipse_stream_from_servant (servant);

	return ges->is_dirty;
}

static void
EllipseStream_load (PortableServer_Servant servant, const Bonobo_Stream stream,
		    Bonobo_Persist_ContentType type, CORBA_Environment *ev)
{
	GDrawEllipseStream *ges = gdraw_ellipse_stream_from_servant (servant);
	GDrawEllipseShape *ellipse = ges->ellipse;

	if (!g_strcasecmp (type, "text/xml") || !g_strcasecmp (type, "")) {
		xmlDoc *doc;
		gchar *str = NULL;

		bonobo_stream_client_read_string (stream, &str, ev);
		doc = xmlParseMemory (str, strlen (str));
		if (!doc || !load_from_xml (ellipse, xmlDocGetRootElement(doc)))
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
					     ex_Bonobo_Persist_WrongDataType, 
					     NULL);
		xmlFreeDoc (doc);
		g_free (str);
	} else
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
				     ex_Bonobo_NotSupported, NULL);
}

static void
EllipseStream_save (PortableServer_Servant servant, const Bonobo_Stream stream,
		    Bonobo_Persist_ContentType type, CORBA_Environment *ev)
{
	GDrawEllipseStream *ges = gdraw_ellipse_stream_from_servant (servant);
	GDrawEllipseShape *ellipse = ges->ellipse;

	if (!g_strcasecmp (type, "text/xml") || !g_strcasecmp (type, "")) {
		xmlDoc *doc;
		xmlChar *str;
		gint len;

		doc = xmlNewDoc ("1.0");
		doc->children = xmlNewDocNode (doc, NULL, "EllipseShape", NULL);

		if( !save_to_xml (ellipse, xmlDocGetRootElement(doc))) {
			xmlFreeDoc (doc);
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
					     ex_Bonobo_Stream_IOError, 
					     NULL);
			return;
		}
		
		xmlDocDumpMemory (doc, &str, &len);
		bonobo_stream_client_write_string (stream, str, TRUE, ev);
		xmlFreeDoc (doc);
		g_free (str);
	}
}

static Bonobo_Persist_ContentTypeList *
get_content_types (BonoboPersist *persist, CORBA_Environment *ev)
{
        return  bonobo_persist_generate_content_types (1, "text/xml");
}

static void
gdraw_ellipse_stream_class_init (GtkObjectClass *klass)
{
	GDrawEllipseStreamClass *esc = GDRAW_ELLIPSE_STREAM_CLASS (klass);
	BonoboPersistClass *bpc = BONOBO_PERSIST_CLASS (klass);
	POA_Bonobo_PersistStream__epv *epv = &esc->epv;

	bpc->get_content_types = get_content_types;

#if 0
	epv->isDirty 	= EllipseStream_is_dirty;
#endif
	epv->load 	= EllipseStream_load;
	epv->save 	= EllipseStream_save;
}

static void
gdraw_ellipse_stream_init (GtkObject *object)
{
	GDrawEllipseStream *ellipse_stream = GDRAW_ELLIPSE_STREAM (object);

	ellipse_stream->ellipse = NULL;
	ellipse_stream->is_dirty = FALSE;
}

BONOBO_TYPE_FUNC_FULL (GDrawEllipseStream, Bonobo_PersistStream, 
		       PARENT_TYPE, gdraw_ellipse_stream);

GDrawEllipseStream *
gdraw_ellipse_stream_new (GDrawEllipseShape *shape)
{
	GDrawEllipseStream *ellipse_stream;

	ellipse_stream = g_object_new (GDRAW_ELLIPSE_STREAM_TYPE, NULL);

	ellipse_stream->ellipse = shape;

	return ellipse_stream;
}
