/*
 * selection.c: Implementation of the Selection class
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <libgnomecanvas/gnome-canvas.h>
#include <bonobo.h>
#include "selection.h"

#define IS_TOP_BOX(x) ((x == 0) || (x == 1) || (x == 2))
#define IS_BOTTOM_BOX(x) ((x == 4) || (x == 5) || (x == 6))
#define IS_LEFT_BOX(x) ((x == 0) || (x == 6) || (x == 7))
#define IS_RIGHT_BOX(x) ((x == 2) || (x == 3) || (x == 4))

static void
update_handles (GDrawSelection *sel)
{
	GnomeCanvasItem *o;
	int i;

	for (i = 0; i < 8; i++) {
		double x, y;

		if (IS_LEFT_BOX (i))
			x = 0.0;
		else if (IS_RIGHT_BOX (i))
			x = sel->sz.width;
		else
			x = sel->sz.width / 2.0;

		if (IS_TOP_BOX (i))
			y = 0.0;
		else if (IS_BOTTOM_BOX (i))
			y = sel->sz.height;
		else
			y = sel->sz.height / 2.0;

		o = sel->handles [i];

		gnome_canvas_item_set (
			sel->handles [i],
			"x1", x - (3.0 / o->canvas->pixels_per_unit),
			"y1", y - (3.0 / o->canvas->pixels_per_unit),
			"x2", x + (3.0 / o->canvas->pixels_per_unit),
			"y2", y + (3.0 / o->canvas->pixels_per_unit),
			NULL);
	}

	gnome_canvas_item_set (sel->object_group, "x", sel->pt.x,
			       "y", sel->pt.y, NULL);
}


static void
do_resize_event (GDrawSelection *sel, GnomeCanvasItem *box, 
		 GdkEvent *event, GNOME_Draw_Point *old_pt)
{
	int i;
	CORBA_Environment ev;
	gboolean move = FALSE;

	CORBA_exception_init (&ev);
		
	i = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (box), "index"));

	if (IS_LEFT_BOX (i)) {
		move = TRUE;
		sel->pt.x += (event->button.x - old_pt->x);
		sel->sz.width -= (event->button.x - old_pt->x);
	} else if (IS_RIGHT_BOX (i))
		sel->sz.width += (event->button.x - old_pt->x);

	if (IS_TOP_BOX (i)) {			
		move = TRUE;
		sel->pt.y += (event->button.y - old_pt->y);
		sel->sz.height -= (event->button.y - old_pt->y);
	} else if (IS_BOTTOM_BOX (i))
		sel->sz.height += (event->button.y - old_pt->y);

	if (move)
		GNOME_Draw_Shape_setPosition (sel->shape, &sel->pt, &ev);

	GNOME_Draw_Shape_setSize (sel->shape, &sel->sz, &ev);
	CORBA_exception_free (&ev);
	update_handles (sel);
}

static void
do_move_event (GDrawSelection *sel, GdkEvent *event, GNOME_Draw_Point *prev_pt)
{
	gdouble x, y;

	x = sel->pt.x + event->button.x - prev_pt->x;
	y = sel->pt.y + event->button.y - prev_pt->y;

	gdraw_selection_moveto (sel, x, y);
}

static gboolean
handle_event_cb (GnomeCanvasItem *box, GdkEvent *event, GDrawSelection *sel)
{
	static gboolean resizing = FALSE;
	static gboolean moving = FALSE;
	static GNOME_Draw_Point pt;

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		if (event->button.button == 1)
			resizing = TRUE;
		else if (event->button.button == 3)
			moving = TRUE;
		else
			return FALSE;

		pt.x = event->button.x;
		pt.y = event->button.y;

		gnome_canvas_item_grab (box, 
			GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
			NULL, event->button.time);

		return TRUE;

	case GDK_BUTTON_RELEASE:
	case GDK_MOTION_NOTIFY:
		if (moving)
			do_move_event (sel, event, &pt);
		else if (resizing)
			do_resize_event (sel, box, event, &pt);
		else
			return FALSE;

		if (event->type == GDK_BUTTON_RELEASE) {
			gnome_canvas_item_ungrab (box, event->button.time);
			moving = resizing = FALSE;
		}
		pt.x = event->button.x;
		pt.y = event->button.y;
		return TRUE;

	default:
		return FALSE;
	}
}

static void
gdraw_selection_destroy (GtkObject *object)
{
	GDrawSelection *sel = GDRAW_SELECTION (object);

	gtk_object_destroy (GTK_OBJECT (sel->object_group));

	/* FIXME: Will need to disband temp group for multi-select */
	bonobo_object_release_unref ((GNOME_Draw_Shape) sel->shape, NULL);
}

static void
gdraw_selection_class_init (GtkObjectClass *klass)
{
	klass->destroy = gdraw_selection_destroy;
}

static void
gdraw_selection_init (GtkObject *object)
{
	GDrawSelection *sel;
	gint i;

	g_return_if_fail (object);

	sel = GDRAW_SELECTION (object);

	sel->shape = CORBA_OBJECT_NIL;
	sel->object_group = NULL;

	sel->pt.x = 0.0;
	sel->pt.y = 0.0;
	sel->sz.width = 0.0;
	sel->sz.height = 0.0;
	sel->multi = FALSE;

	for (i = 0; i < 8; i++)
		sel->handles [i] = NULL;
}

GtkType
gdraw_selection_get_type (void)
{
	static GtkType sel_type = 0;

	if (!sel_type) {
		static const GtkTypeInfo sel_info = {
			"GDrawSelection",
			sizeof (GDrawSelection),
			sizeof (GDrawSelectionClass),
			(GtkClassInitFunc) gdraw_selection_class_init,
			(GInstanceInitFunc) gdraw_selection_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		sel_type = gtk_type_unique (gtk_object_get_type (), 
					    &sel_info);
	}

	return sel_type;
}

static void
create_handles (GDrawSelection *sel)
{
	GnomeCanvasItem *o;
	gint i;

	for (i = 0; i < 8; i++) {
		o = gnome_canvas_item_new (
			GNOME_CANVAS_GROUP (sel->object_group),
			gnome_canvas_rect_get_type (),
			"outline_color", "black",
			"fill_color", "white",
			"width_pixels", 1,
			NULL);
		gnome_canvas_item_show (o);
		sel->handles [i] = o;
		g_object_set_data (G_OBJECT (o), "index", 
				     GINT_TO_POINTER (i));
		g_signal_connect (G_OBJECT (o), "event",
				  G_CALLBACK (handle_event_cb), sel);
	}

	update_handles (sel);
}

/**
 * gdraw_selection_new:
 * @group: A #GnomeCanvasGroup to draw handles on.
 * @sl: A #GList containing the selected shapes.
 *
 * Creates the top-level CanvasGroup for the object and stores the shape.
 */
GDrawSelection *
gdraw_selection_new (GnomeCanvasGroup *group, GList *sl)
{
	GDrawSelection *sel;
	CORBA_Environment ev;

	g_return_val_if_fail (group != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_CANVAS_GROUP (group), NULL);
	g_return_val_if_fail (sl != NULL, NULL);

	sel = gtk_type_new (GDRAW_SELECTION_TYPE);

	if (sl->next) {
		/* FIXME: Eventually, create a temporary GroupShape */
		g_warning ("Multi-selection not implemented yet");
		gtk_object_destroy (GTK_OBJECT (sel));
		return NULL;
	} else {
		sel->shape = bonobo_object_dup_ref (
					(GNOME_Draw_Shape) sl->data, NULL);
		sel->multi = FALSE;
	}

	CORBA_exception_init (&ev);

	sel->pt = GNOME_Draw_Shape_getPosition (sel->shape, &ev);
	sel->sz = GNOME_Draw_Shape_getSize (sel->shape, &ev);

	if (BONOBO_EX (&ev)) {
		gtk_object_destroy (GTK_OBJECT (sel));
		CORBA_exception_free (&ev);
		return NULL;
	}

	sel->object_group = gnome_canvas_item_new (
				group, gnome_canvas_group_get_type (),
				"x", sel->pt.x, "y", sel->pt.y, NULL);

	create_handles (sel);

	CORBA_exception_free (&ev);

	return sel;
}

/**
 * gdraw_selection_show_handles:
 * @sel: A #GDrawSelection.
 *
 * Shows the handles of a selection.
 */
void
gdraw_selection_show_handles (GDrawSelection *sel)
{
	gint i;

	g_return_if_fail (sel != NULL);

	for (i = 0; i < 8; i++)
		gnome_canvas_item_show (sel->handles [i]);
}

/**
 * gdraw_selection_hide_handles:
 * @sel: A #GDrawSelection.
 *
 * Hides the handles of a selection.
 */
void
gdraw_selection_hide_handles (GDrawSelection *sel)
{
	gint i;

	g_return_if_fail (sel != NULL);

	for (i = 0; i < 8; i++)
		gnome_canvas_item_hide (sel->handles [i]);
}

/**
 * gdraw_selection_moveto:
 * @sel: A #GDrawSelection.
 * @x: X coordinate of target point.
 * @y: Y coordinate of target point.
 *
 * Move the selection to a point (x, y).
 */
void
gdraw_selection_moveto (GDrawSelection *sel, gdouble x, gdouble y)
{
	CORBA_Environment ev;

	g_return_if_fail (sel != NULL);

	sel->pt.x = x;
	sel->pt.y = y;

	CORBA_exception_init (&ev);
	GNOME_Draw_Shape_setPosition (sel->shape, &sel->pt, &ev);
	CORBA_exception_free (&ev);

	update_handles (sel);
}

/**
 * gdraw_selection_contains_shape:
 * @sel: A #GDrawSelection.
 * @shape: A GNOME::Draw::Shape to lookup.
 *
 * Returns: TRUE if the specified shape is in the selection.
 */
gboolean
gdraw_selection_contains_shape (GDrawSelection *sel, GNOME_Draw_Shape shape)
{
	CORBA_Environment ev;
	gboolean retval;

	g_return_val_if_fail (sel != NULL, FALSE);
	g_return_val_if_fail (shape != CORBA_OBJECT_NIL, FALSE);

	if (sel->multi) {
		g_warning ("Multi-shape selections not implemented");
		return FALSE;
	}

	CORBA_exception_init (&ev);
	retval = CORBA_Object_is_equivalent (sel->shape, shape, &ev);
	CORBA_exception_free (&ev);

	return retval;
}
