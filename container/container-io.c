#include <bonobo.h>
#include <gdraw/oafiids.h>

#include "container-io.h"

#define STORAGE_TYPE "efs"

static void
load_shapes (Bonobo_Unknown shapes, Bonobo_Storage storage, 
	     CORBA_Environment *ev)
{
	Bonobo_PersistStorage 	pstorage;

	pstorage = Bonobo_Unknown_queryInterface ( shapes, 
					"IDL:Bonobo/PersistStorage:1.0", ev);
	if (BONOBO_EX (ev) || (pstorage == CORBA_OBJECT_NIL)) {
		g_warning ("Exception querying persist storage '%s'",
			   bonobo_exception_get_text (ev));
		return;
	}

	Bonobo_PersistStorage_load (pstorage, storage, ev); 
	if (BONOBO_EX (ev)) {
		g_warning ("Exception loading persist storage '%s'",
			   bonobo_exception_get_text (ev));
		return;
	}

	Bonobo_Unknown_unref (pstorage, ev);
}

static void
save_shapes (Bonobo_Unknown shapes, Bonobo_Storage storage, 
	     CORBA_Environment *ev)
{
	Bonobo_PersistStorage 	pstorage;

	pstorage = Bonobo_Unknown_queryInterface ( shapes, 
					"IDL:Bonobo/PersistStorage:1.0", ev);
	if (BONOBO_EX (ev)) {
		g_warning ("Exception querying persist storage '%s'",
			   bonobo_exception_get_text (ev));
		return;
	}

	Bonobo_PersistStorage_save (pstorage, storage, FALSE, ev); 
	if (BONOBO_EX (ev)) {
		g_warning ("Exception saving persist storage '%s'",
			   bonobo_exception_get_text (ev));
		return;
	}

	Bonobo_Unknown_unref (pstorage, ev);
}

static void
save_component (BonoboStorage *storage,	Bonobo_Unknown shapes)
{
	Bonobo_Storage corba_storage = BONOBO_OBJREF (storage);
	Bonobo_Storage corba_subdir;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	corba_subdir = Bonobo_Storage_openStorage (corba_storage, "shapes", 
						   Bonobo_Storage_CREATE | 
						   Bonobo_Storage_READ | 
						   Bonobo_Storage_WRITE, 
						   &ev);

	if (BONOBO_EX (&ev)) {
		g_warning ("Can't create shapes storage - %s",
			   bonobo_exception_get_text (&ev));
		return;
	}

	save_shapes (shapes, corba_subdir, &ev);

	bonobo_object_release_unref (corba_subdir, &ev);

	CORBA_exception_free (&ev);
}

static void
load_component (SampleApp *app, BonoboStorage *storage)
{
	Bonobo_Storage corba_subdir;
	Bonobo_Storage corba_storage = BONOBO_OBJREF (storage);
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	corba_subdir = Bonobo_Storage_openStorage (corba_storage,
						   "shapes",
						   Bonobo_Storage_READ,
						   &ev);
	if (BONOBO_EX (&ev)) {
		g_warning ("Can't open shapes storage - %s",
			   bonobo_exception_get_text (&ev));
		return;
	}

	load_shapes (app->shapes, corba_subdir, &ev);

	bonobo_object_release_unref (corba_subdir, &ev);

	CORBA_exception_free (&ev);
}

void
sample_container_load (SampleApp *app, const char *filename)
{
#if 0
	BonoboStorage *storage;

	storage = bonobo_storage_open (STORAGE_TYPE, filename,
				       Bonobo_Storage_READ |
				       Bonobo_Storage_WRITE |
				       Bonobo_Storage_CREATE,
				       0664);
	g_return_if_fail (storage);

	load_component (app, storage);

	bonobo_object_unref (BONOBO_OBJECT (storage));
#endif
}

void
sample_container_save (SampleApp *app, const char *filename)
{
#if 0
	CORBA_Environment ev;
	BonoboStorage *storage;
	Bonobo_Storage corba_storage;

	unlink (filename);
	storage = bonobo_storage_open (STORAGE_TYPE, filename,
				       Bonobo_Storage_READ |
				       Bonobo_Storage_WRITE |
				       Bonobo_Storage_CREATE,
				       0664);
	g_return_if_fail (storage);

	CORBA_exception_init (&ev);

	corba_storage = BONOBO_OBJREF (storage);

	save_component (storage, app->shapes);

	Bonobo_Storage_commit (corba_storage, &ev);

	CORBA_exception_free (&ev);

	bonobo_object_unref (BONOBO_OBJECT (storage));
#endif
}
