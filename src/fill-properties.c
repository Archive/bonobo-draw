/*
 * fill-properties.c: Implementation of the FillProperties
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include "fill-properties.h"
#include "properties.h"

void
gdraw_fill_properties_add_props (BonoboPropertyBag *bag)
{
	bonobo_property_bag_add (bag, "FillStyle", PROP_FILL_STYLE,
				 BONOBO_ARG_INT, NULL,
				 "Style of fill", 0);

	bonobo_property_bag_add (bag, "FillColor", PROP_FILL_COLOR,
				 BONOBO_ARG_LONG, NULL,
				 "RGB color of the fill", 0);

	bonobo_property_bag_add (bag, "FillTransparency", PROP_FILL_TRANSPARENCY,
				 BONOBO_ARG_INT, NULL,
				 "Alpha transparency of the fill", 0);

	bonobo_property_bag_add (bag, "FillRGBA", PROP_FILL_RGBA,
				 BONOBO_ARG_LONG, NULL,
				 "RGBA color of the fill", 0);
}

gboolean
gdraw_fill_properties_get_prop (GDrawFillProperties *fill, 
				guint arg_id, BonoboArg *arg)
{
	switch (arg_id) {

	case PROP_FILL_STYLE:
		BONOBO_ARG_SET_INT (arg, (gint) fill->fill_style);
		break;
	case PROP_FILL_COLOR:
		BONOBO_ARG_SET_LONG (arg, fill->fill_rgba >> 8);
		break;
	case PROP_FILL_TRANSPARENCY:
		BONOBO_ARG_SET_INT (arg, fill->fill_rgba & 0xf);
		break;
	case PROP_FILL_RGBA:
		BONOBO_ARG_SET_LONG (arg, fill->fill_rgba);
		break;
	default:
		return FALSE;
	}

	return TRUE;
}
gboolean
gdraw_fill_properties_set_prop (GDrawFillProperties *fill, 
				guint arg_id, BonoboArg *arg)
{
	switch (arg_id) {

	case PROP_FILL_STYLE:
		switch ((GNOME_Draw_FillStyle) BONOBO_ARG_GET_INT (arg)) {
		case GNOME_Draw_FILLNONE:
			fill->fill_style = GNOME_Draw_FILLNONE;
			break;
		case GNOME_Draw_FILLSOLID:
			fill->fill_style = GNOME_Draw_FILLSOLID;
			break;
		case GNOME_Draw_FILLGRADIENT:
		case GNOME_Draw_FILLHATCH:
		case GNOME_Draw_FILLBITMAP:
		default:
			/* Unsupported FillStyles */
			g_warning ("Unsupported fill style");
		}
		break;
	case PROP_FILL_COLOR:
		fill->fill_rgba = (BONOBO_ARG_GET_LONG (arg) << 8) + 
				  (fill->fill_rgba & 0xf);
		break;
	case PROP_FILL_TRANSPARENCY:
		fill->fill_rgba = (BONOBO_ARG_GET_INT (arg) & 0xf) +
				  (fill->fill_rgba & 0xfff0);
		break;
	case PROP_FILL_RGBA:
		fill->fill_rgba = BONOBO_ARG_GET_LONG (arg);
		break;
	default:
		return FALSE;
	}

	return TRUE;
}

gboolean
gdraw_fill_properties_load_from_xml (GDrawFillProperties *fill, 
				     xmlNode *node)
{
	gchar *prop;

	if (g_strcasecmp (node->name, "FillProperties"))
		return FALSE;

	prop = xmlGetProp (node, "FillStyle");
	if (!prop)
		return FALSE;

	fill->fill_style = strtol (prop, NULL, 10);
	g_free (prop);

	if (!fill->fill_style)
		return TRUE;

	prop = xmlGetProp (node, "FillRGBA");
	if (prop)
		fill->fill_rgba = strtoul (prop, NULL, 16);
	else {
		fill->fill_rgba = 0;
		free (prop);
	}

	return TRUE;
}

xmlNode *
gdraw_fill_properties_save_to_xml (GDrawFillProperties *fill, 
				   xmlNode *node)
{
	xmlNode *child;
	gchar *prop;

	child = xmlNewChild (node, NULL, "FillProperties", NULL);

	prop = g_strdup_printf ("%d", fill->fill_style);
	if (!prop) {
		xmlFreeNode (child);
		return NULL;
	}
	xmlSetProp (child, "FillStyle", prop);
	g_free (prop);

	if (!fill->fill_style)
		return child;

	prop = g_strdup_printf ("%x", fill->fill_rgba);
	if (!prop) {
		xmlFreeNode (child);
		return NULL;
	}
	xmlSetProp (child, "FillRGBA", prop);
	g_free (prop);

	return child;
}

