/*
 * rect-canvas-object.c: Implementation of the RectCanvasObject class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <libgnomecanvas/gnome-canvas.h>
#include "rect-canvas-object.h"


static void
gdraw_rect_canvas_object_destroy (GtkObject *object)
{
	GDrawCanvasObject *cobj = GDRAW_CANVAS_OBJECT (object);

	gtk_object_destroy (GTK_OBJECT (cobj->object_group));
}

static void
rect_update_fill (GnomeCanvasItem *item, GDrawFillProperties *fill)
{
	switch (fill->fill_style)  {

	case GNOME_Draw_FILLSOLID:
		gnome_canvas_item_set (item, "fill_color_rgba", 
				       fill->fill_rgba, NULL);
		break;
	case GNOME_Draw_FILLNONE:
		gnome_canvas_item_set (item, "fill_color", NULL, NULL);
		break;
	default:
		g_warning ("Unsupported fill style %d", fill->fill_style);
	}
}

static void
rect_update_line (GnomeCanvasItem *item, GDrawLineProperties *line)
{
	switch (line->style)  {

	case GNOME_Draw_LINESOLID:
		gnome_canvas_item_set (item, "outline_color_rgba", 
				       line->rgba, NULL);
		break;
	case GNOME_Draw_LINENONE:
		gnome_canvas_item_set (item, "outline_color", NULL, NULL);
		break;
	default:
		g_warning ("Unsupported line style %d", line->style);
	}
}

static void
rect_update_shadow (GDrawRectCanvasObject *rcobj, 
		    GDrawShadowProperties *shadow)
{
	GDrawCanvasObject *cobj = GDRAW_CANVAS_OBJECT (rcobj);
	GDrawShape *shape = cobj->shape;

	if (shadow->visible) {
		if (rcobj->shadow_item)
			rcobj->shadow_item = gnome_canvas_item_new (
				GNOME_CANVAS_GROUP (cobj->object_group),
				gnome_canvas_rect_get_type (),
				"x1", shadow->x_dist,
				"y1", shadow->y_dist,
				"x2", shape->width + shadow->x_dist,
				"y2", shape->height + shadow->y_dist,
				"fill_color_rgba", shadow->rgba, NULL);
		else
			gnome_canvas_item_set (
				rcobj->shadow_item,
				"x1", shadow->x_dist,
				"y1", shadow->y_dist,
				"x2", shape->width + shadow->x_dist,
				"y2", shape->height + shadow->y_dist,
				"fill_color_rgba", shadow->rgba, NULL);
	} else if (rcobj->shadow_item) {
		gtk_object_destroy (GTK_OBJECT (rcobj->shadow_item));
		rcobj->shadow_item = NULL;
	}
}

static void
redraw_rect_cobj (GDrawShape *shape, GDrawCanvasObject *cobj)
{
	GDrawREShape *re = GDRAW_RE_SHAPE (cobj->shape);
	GDrawRectCanvasObject *rcobj = GDRAW_RECT_CANVAS_OBJECT (cobj);
	gdouble affine[6];

	art_affine_translate (affine, cobj->shape->x, cobj->shape->y);

	gnome_canvas_item_affine_absolute (cobj->object_group, affine);

	gnome_canvas_item_set (rcobj->canvas_item,
			      "x1", 0.0, "y1", 0.0,
			      "x2", cobj->shape->width,
			      "y2", cobj->shape->height,
			      NULL);

	rect_update_fill (rcobj->canvas_item, re->fill);
	rect_update_line (rcobj->canvas_item, re->line);
	rect_update_shadow (rcobj, re->shadow);
}

static void
gdraw_rect_canvas_object_class_init (GtkObjectClass *klass)
{
	klass->destroy = gdraw_rect_canvas_object_destroy;
}

static void
gdraw_rect_canvas_object_init (GtkObject *object)
{
	GDrawRectCanvasObject *cobj;

	g_return_if_fail (object);

	cobj = GDRAW_RECT_CANVAS_OBJECT (object);

	cobj->canvas_item = NULL;
	cobj->shadow_item = NULL;
}

GtkType
gdraw_rect_canvas_object_get_type (void)
{
	static GtkType cobj_type = 0;

	if (!cobj_type) {
		static const GtkTypeInfo cobj_info = {
			"GDrawRectCanvasObject",
			sizeof (GDrawRectCanvasObject),
			sizeof (GDrawRectCanvasObjectClass),
			(GtkClassInitFunc) gdraw_rect_canvas_object_class_init,
			(GtkObjectInitFunc) gdraw_rect_canvas_object_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		cobj_type = gtk_type_unique (gdraw_canvas_object_get_type (), 
					     &cobj_info);
	}

	return cobj_type;
}

static void
create_rect_cobj (GDrawRectCanvasObject *rcobj)
{
	GDrawCanvasObject *cobj = GDRAW_CANVAS_OBJECT (rcobj);

	rcobj->canvas_item = gnome_canvas_item_new (
				GNOME_CANVAS_GROUP (cobj->object_group),
				gnome_canvas_rect_get_type (), NULL);

	redraw_rect_cobj (cobj->shape, cobj);
}

GDrawCanvasObject *
gdraw_rect_canvas_object_new (GDrawRectShape *rect, 
			      GnomeCanvas *canvas)
{
	GDrawCanvasObject *cobj;
	GDrawRectCanvasObject *rcobj;

	g_return_val_if_fail (rect != NULL, NULL);
	g_return_val_if_fail (canvas != NULL, NULL);

	rcobj = gtk_type_new (GDRAW_RECT_CANVAS_OBJECT_TYPE);

	cobj = GDRAW_CANVAS_OBJECT (rcobj);

	gdraw_canvas_object_construct (cobj, canvas, GDRAW_SHAPE (rect));

	g_signal_connect(G_OBJECT(rect), "changed",
			 G_CALLBACK(redraw_rect_cobj), cobj);

	create_rect_cobj (rcobj);

	return cobj;
}
