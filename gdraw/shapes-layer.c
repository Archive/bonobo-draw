/*
 * shapes-layer.c: Implementation of the ShapesLayer canvas item.
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 */

#include <config.h>
#include <libgnomecanvas/gnome-canvas.h>
#include <libgnomecanvas/gnome-canvas-util.h>
#include <bonobo-activation/bonobo-activation.h>
#include <bonobo/bonobo-canvas-item.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-property-bag-client.h>

#include "GNOME_Draw.h"
#include "selection.h"
#include "shape-client.h"
#include "shapes-layer.h"
#include "shapes-layer-uih.h"
#include "shapes-listener.h"

typedef struct {
	GNOME_Draw_Shape	shape;
	GnomeCanvasItem		*item;
} shape_hash_entry_t;

struct _GDrawShapesLayerPrivate {
	GnomeCanvasGroup	*group;
	GNOME_Draw_Shapes 	shapes;
	Bonobo_UIContainer 	ui_cont;
	BonoboUIComponent 	*ui_comp;
	GDrawShapesListener	*listener;
	gulong			 list_id;
	gboolean 		activated;
	guint			grid_gran;
	GHashTable 		*canvas_items;
	GDrawSelection		*selection;

	/* For outlines while creating certain objects */
	GnomeCanvasItem 	*tobj;
	double 			tx, ty;

	/* Storage for a shape iid pending creation. */
	gchar			*oaf_iid;

	/* Current drawing mode state  */
	GDrawShapesLayerMode 	draw_mode;

	guint32			def_fill_color;
	guint32			def_line_color;
};

static void
display_popup_menu(GNOME_Draw_Shape shape)
{
	g_warning ("Popup menu unimplemented");
}

static void
update_selection (GDrawShapesLayer *layer, GNOME_Draw_Shape shape, 
		  GnomeCanvas *canvas)
{
	GDrawSelection *sel;
	GList *l = NULL;

	sel = layer->priv->selection;
	if (sel && gdraw_selection_contains_shape (sel, shape)) {
		gdraw_selection_hide_handles (sel);
		return;
	}

	if (sel)
		gtk_object_destroy (GTK_OBJECT (sel));

	l = g_list_prepend (l, shape);
	layer->priv->selection = 
			gdraw_selection_new (gnome_canvas_root (canvas), l);
	g_list_free (l);
}

static gboolean
canvas_item_handle_event (GnomeCanvasItem *item, GdkEvent *event, GNOME_Draw_Shape shape)
{
	static double offset_x, offset_y;
	double item_x, item_y;
	static gboolean dragging = FALSE;
	GDrawShapesLayer *layer;

	g_return_val_if_fail (item != NULL, FALSE);
	g_return_val_if_fail (event != NULL, FALSE);
	g_return_val_if_fail (shape != CORBA_OBJECT_NIL, FALSE);
	layer = GDRAW_SHAPES_LAYER (g_object_get_data (G_OBJECT (item), 
						       "ShapesLayer"));
	g_return_val_if_fail (layer != NULL, FALSE);

	if (layer->priv->draw_mode != GD_LAYER_MODE_NORMAL)
		return FALSE;

	item_x = event->button.x;
	item_y = event->button.y;
	gnome_canvas_item_w2i (item->parent, &item_x, &item_y);

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		if (event->button.button == 1) {
			GdkCursor *cursor;
			GNOME_Draw_Point pt;
			CORBA_Environment ev;

			update_selection (layer, shape, item->canvas);

			CORBA_exception_init (&ev);
			if (!gdraw_shape_client_is_moveable (shape, &ev)) {
				CORBA_exception_free (&ev);
				return FALSE;
			}

			cursor = gdk_cursor_new (GDK_FLEUR);
			gnome_canvas_item_grab (item,
						GDK_POINTER_MOTION_MASK | 
						GDK_BUTTON_RELEASE_MASK,
						cursor, event->button.time);

			pt = GNOME_Draw_Shape_getPosition (shape, &ev);
			if (BONOBO_EX (&ev)) {
				CORBA_exception_free (&ev);
				return FALSE;
			}

			offset_x = item_x - pt.x;
			offset_y = item_y - pt.y;
			dragging = TRUE;
			CORBA_exception_free (&ev);
			return TRUE;

		} else if (event->button.button == 3) {
			display_popup_menu (shape);
			return TRUE;
		}
		return FALSE;

	case GDK_BUTTON_RELEASE:

		if (dragging) {
			dragging = FALSE;
			gnome_canvas_item_ungrab (item, event->button.time);
			gdraw_selection_show_handles (layer->priv->selection);
			return TRUE;
		}
		return FALSE;
	
	case GDK_MOTION_NOTIFY:

		if (dragging) {
			gdraw_selection_moveto (layer->priv->selection,
						item_x - offset_x,
						item_y - offset_y);
			return TRUE;
		}
		return FALSE;

	default:
		return;
	}
	
	return FALSE;
}

static void
create_canvas_item (GDrawShapesLayer *layer, const gchar *shape_name, 
		    GNOME_Draw_Shape shape)
{
	GnomeCanvasItem *item;
	GNOME_Draw_Shape dup_shape;
	Bonobo_Embeddable corba_emb;
	shape_hash_entry_t *entry;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	corba_emb = Bonobo_Unknown_queryInterface (
				shape, "IDL:Bonobo/Embeddable:1.0", &ev);

	if (BONOBO_EX (&ev) || (corba_emb == CORBA_OBJECT_NIL)) {
		g_warning ("Couldn't get the shape's embeddable interface");
		return;
	}

	dup_shape = bonobo_object_dup_ref (shape, &ev);

	item = gnome_canvas_item_new (
			layer->priv->group,
			bonobo_canvas_item_get_type (),
			"corba_ui_container", layer->priv->ui_cont, 
			"corba_embeddable", corba_emb, NULL); 

	g_signal_connect(G_OBJECT(item), "event",
			 G_CALLBACK(canvas_item_handle_event), layer);

	entry = g_new0 (shape_hash_entry_t, 1);
	entry->shape = dup_shape;
	entry->item = item;

	g_object_set_data (G_OBJECT (item), "ShapesLayer", layer);

	g_hash_table_insert (layer->priv->canvas_items, 
			     g_strdup (shape_name), entry);
}

static void
shape_added_cb (GtkObject *object, const gchar *shape_name, 
		GNOME_Draw_Shape shape, GDrawShapesLayer *layer)
{
	if (g_hash_table_lookup (layer->priv->canvas_items, shape_name))
		return;

	create_canvas_item (layer, shape_name, shape);
}

static void
shape_removed_cb (GtkObject *object, gchar *shape_name, GDrawShapesLayer *layer)
{
	shape_hash_entry_t *entry = NULL;
	gchar *orig_key = NULL;

	if (g_hash_table_lookup_extended (layer->priv->canvas_items, shape_name, 
					  (gpointer *) &orig_key, (gpointer *) &entry)) {
		g_hash_table_remove (layer->priv->canvas_items, shape_name);
		g_free (orig_key);
		gtk_object_destroy (GTK_OBJECT (entry->item));
		bonobo_object_release_unref (entry->shape, NULL);
		g_free (entry);
	}
}

static GSList *
layer_get_shape_name_list (GDrawShapesLayer *layer, CORBA_Environment *ev)
{
	GNOME_Draw_Shapes_ShapeNames *corba_names;
	GSList *l = NULL;
	gint i;

	corba_names = GNOME_Draw_Shapes_getShapeNames (layer->priv->shapes, ev);

	if (BONOBO_EX (ev))
		return NULL;

	for (i = 0; i < corba_names->_length; i++)
		l = g_slist_prepend (l, corba_names->_buffer [i]);

	/* FIXME: Release the sequence, but not the strings. */
	return l;
}

static gboolean
background_event_cb (GnomeCanvasItem *item, GdkEvent *event, 
		     GDrawShapesLayer *layer)
{
	if (layer->priv->draw_mode != GD_LAYER_MODE_NORMAL)
		return FALSE;

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		if (event->button.button == 1) {
			if (layer->priv->selection) {
				gtk_object_destroy (GTK_OBJECT (
						layer->priv->selection));
				layer->priv->selection = NULL;
			}
		}
		break;

	default:
		break;
	}

	return FALSE;
}

static void
gdraw_shapes_layer_prepare (GDrawShapesLayer *layer)
{
	GnomeCanvasItem *bg;
	gdouble x1, x2, y1, y2;
	GSList *shape_list, *l;
	Bonobo_EventSource ev_src;
	CORBA_Environment ev;

	gnome_canvas_get_scroll_region (
			GNOME_CANVAS_ITEM (layer->priv->group)->canvas, 
			&x1, &y1, &x2, &y2);

	bg = gnome_canvas_item_new (layer->priv->group,
				    gnome_canvas_rect_get_type (),
				    "x1", x1, "y1", y1, 
				    "x2", x2, "y2", y2, 
				    "fill_color", NULL, 
				    NULL);

	g_signal_connect(G_OBJECT(bg), "event",
			 G_CALLBACK(background_event_cb), layer);

	CORBA_exception_init (&ev);

	shape_list = layer_get_shape_name_list (layer, &ev);

	for (l = shape_list; l; l = l->next) {
		gchar *name = (gchar *) l->data;
		GNOME_Draw_Shape shape;

		shape = GNOME_Draw_Shapes_getShapeByName (layer->priv->shapes, name, &ev);
		if (BONOBO_EX (&ev) || (shape == CORBA_OBJECT_NIL))
			continue;
		create_canvas_item (layer, name, shape);
	}

	g_slist_free (shape_list);

	layer->priv->listener = gdraw_shapes_listener_new ();
	ev_src = Bonobo_Unknown_queryInterface (layer->priv->shapes,
				"IDL:Bonobo/EventSource:1.0", &ev);
	if (BONOBO_EX (&ev) || (ev_src == CORBA_OBJECT_NIL))
		g_warning ("%s exception getting EventSource\n", ev._id);
	//FIXME: void value not ignored as it ought to be
	//layer->priv->list_id = Bonobo_EventSource_addListener (
	//	ev_src, BONOBO_OBJREF (layer->priv->listener), &ev);
		Bonobo_EventSource_addListener (
				ev_src, BONOBO_OBJREF (layer->priv->listener), &ev);
	if (BONOBO_EX (&ev))
		g_warning ("%s exception adding ShapesListener\n", ev._id);

	Bonobo_Unknown_unref (ev_src, &ev);

	g_signal_connect(
		G_OBJECT(layer->priv->listener), "shape_added",
		G_CALLBACK(shape_added_cb), layer);
	g_signal_connect(
		G_OBJECT(layer->priv->listener), "shape_removed",
		G_CALLBACK(shape_removed_cb), layer);

	CORBA_exception_free (&ev);
}

static gdouble
snap_to_grid (GDrawShapesLayer *layer, gdouble x)
{
	gint remain, numerator;
	gboolean negative;

	negative = (x < 0.0);
	numerator = abs ((gint)x);

	remain = numerator % layer->priv->grid_gran;

	if (abs (remain) < (layer->priv->grid_gran / 2)) {
		numerator -= remain;
	} else {
		numerator += (layer->priv->grid_gran - remain);
	}

	if (negative)
		numerator = 0 - numerator; 

	return (gdouble) numerator;
}

static gboolean
canvas_motion_notify (GtkWidget *canvas, GdkEvent *event, GDrawShapesLayer *layer)
{
	gnome_canvas_window_to_world (GNOME_CANVAS (canvas), 
				      event->motion.x, event->motion.y,
				      &event->motion.x, &event->motion.y);

	switch (layer->priv->draw_mode) {
	case GD_LAYER_MODE_CREATE_POINTS: {
		GnomeCanvasPoints *old_points = NULL;
		GnomeCanvasPoints *new_points;
		gint i;

		gtk_object_get (GTK_OBJECT (layer->priv->tobj), 
				"points", old_points, NULL);

		new_points = gnome_canvas_points_new (old_points->num_points);
		for (i = 0; i < (old_points->num_points - 2); i++)
			new_points->coords[i] = old_points->coords[i];

		new_points->coords[old_points->num_points - 2] = (int) event->motion.x;
		new_points->coords[old_points->num_points - 1] = (int) event->motion.y;
			
		gnome_canvas_item_set (layer->priv->tobj, "points", new_points, NULL);

		gnome_canvas_points_unref (new_points);
		break;
	}

	case GD_LAYER_MODE_CREATE_RECT:
	case GD_LAYER_MODE_CREATE_OVAL:
		gnome_canvas_item_set (layer->priv->tobj,
			"x1", MIN (layer->priv->tx, event->motion.x),
			"y1", MIN (layer->priv->ty, event->motion.y),
			"x2", MAX (layer->priv->tx, event->motion.x),
			"y2", MAX (layer->priv->ty, event->motion.y),
			NULL);
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	return TRUE;
}

static gboolean
property_bag_client_has_property (Bonobo_PropertyBag bag, const gchar *prop_name,
				  CORBA_Environment *ev)
{
	CORBA_any *prop;

	prop = Bonobo_PropertyBag_getValue(bag, prop_name, ev);

	if (BONOBO_EX (ev)) {
		CORBA_exception_free (ev);
		CORBA_exception_init (ev);
		return FALSE;
	}

	CORBA_free(prop);

	return TRUE;
}

static void
layer_set_shape_defaults (GDrawShapesLayer *layer, GNOME_Draw_Shape shape,
			  CORBA_Environment *ev)
{
	Bonobo_PropertyBag bag;

	bag = Bonobo_Unknown_queryInterface (
				shape, "IDL:Bonobo/PropertyBag:1.0", ev);

	if (BONOBO_EX (ev) || bag == (CORBA_OBJECT_NIL))
		return;

	if (property_bag_client_has_property (bag, "FillStyle", ev)) {

		bonobo_property_bag_client_set_value_gint (
			bag, "FillStyle", (gint) GNOME_Draw_FILLSOLID, NULL);
		bonobo_property_bag_client_set_value_glong (
			bag, "FillRGBA", layer->priv->def_fill_color, NULL);
	}

	if (property_bag_client_has_property (bag, "LineStyle", ev)) {

		bonobo_property_bag_client_set_value_gint (
			bag, "LineStyle", (gint) GNOME_Draw_LINESOLID, NULL);
		bonobo_property_bag_client_set_value_glong (
			bag, "LineRGBA", layer->priv->def_line_color, NULL);
	}

	Bonobo_Unknown_unref (bag, ev);

}
static GNOME_Draw_Shape
layer_create_shape (GDrawShapesLayer *layer, CORBA_Environment *ev)
{
	GNOME_Draw_Shape shape;

	g_return_val_if_fail (layer->priv->oaf_iid != NULL, CORBA_OBJECT_NIL);

	shape = bonobo_activation_activate_from_id (layer->priv->oaf_iid, 0, NULL, ev);

	g_free (layer->priv->oaf_iid);
	layer->priv->oaf_iid = NULL;

	if (BONOBO_EX (ev)) {
		g_warning ("%s exception detected activating shape",
			   ev->_id);
		return CORBA_OBJECT_NIL;
	}

	layer_set_shape_defaults (layer, shape, ev);

	if (BONOBO_EX (ev)) {
		g_warning ("%s exception detected setting shape defaults",
			   ev->_id);
		return CORBA_OBJECT_NIL;
	}

	if (!gdraw_shapes_layer_add_shape (layer, shape)) 
		return CORBA_OBJECT_NIL;

	return shape;
}

static gboolean
canvas_button_release (GtkWidget *canvas, GdkEvent *event, GDrawShapesLayer *layer)
{
	GNOME_Draw_Shape shape;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	gnome_canvas_window_to_world (GNOME_CANVAS (canvas), 
				      event->button.x, event->button.y,
				      &event->button.x, &event->button.y);

	event->button.x = snap_to_grid (layer, event->button.x);
	event->button.y = snap_to_grid (layer, event->button.y);

	shape = layer_create_shape (layer, &ev);

	g_return_val_if_fail (shape != CORBA_OBJECT_NIL, FALSE);

	switch (layer->priv->draw_mode) {
	case GD_LAYER_MODE_CREATE_POINTS:
		g_warning ("FIXME: Points unimplemented");
		break;
	case GD_LAYER_MODE_CREATE_RECT:
	case GD_LAYER_MODE_CREATE_OVAL: {
		GNOME_Draw_Point pt;
		GNOME_Draw_Size sz;

		pt.x = MIN (layer->priv->tx, event->button.x);
		pt.y = MIN (layer->priv->ty, event->button.y);
		sz.width = MAX (layer->priv->tx, event->button.x) - pt.x;
		sz.height = MAX (layer->priv->ty, event->button.y) - pt.y;
		GNOME_Draw_Shape_setPosition (shape, &pt, &ev);
		GNOME_Draw_Shape_setSize (shape, &sz, &ev);
		break;
	}
	default:
		g_assert_not_reached ();
	}

	gtk_object_destroy (GTK_OBJECT (layer->priv->tobj));
	layer->priv->tobj = NULL;
	layer->priv->tx = 0.0;
	layer->priv->ty = 0.0;

	g_signal_handlers_disconnect_by_func(
		G_OBJECT (canvas), G_CALLBACK (canvas_button_release), layer);
	g_signal_handlers_disconnect_by_func(
		G_OBJECT (canvas), G_CALLBACK (canvas_motion_notify), layer);

	gdraw_shapes_layer_set_mode (layer, GD_LAYER_MODE_NORMAL);
	update_selection (layer, shape, GNOME_CANVAS (canvas));
	bonobo_object_release_unref (shape, &ev);
	CORBA_exception_free (&ev);
	return TRUE;
}

static gboolean
canvas_button_press (GtkWidget *canvas, GdkEvent *event, GDrawShapesLayer *layer)
{
	if (event->button.button != 1 || !layer->priv->activated) {
		return FALSE;
	}

	switch (layer->priv->draw_mode) {
	case GD_LAYER_MODE_CREATE_POINTS:
		g_warning ("Points Unimplemented");
		return FALSE;
	case GD_LAYER_MODE_CREATE_RECT:
		layer->priv->tobj = gnome_canvas_item_new (
				gnome_canvas_root (GNOME_CANVAS (canvas)), 
				gnome_canvas_rect_get_type (), 
				"outline_color_rgba", 0x000000ff, 
				"width_pixels", 2, NULL);
		break;
	case GD_LAYER_MODE_CREATE_OVAL:
		layer->priv->tobj = gnome_canvas_item_new (
				gnome_canvas_root (GNOME_CANVAS (canvas)), 
				gnome_canvas_ellipse_get_type (), 
				"outline_color_rgba", 0x000000ff, 
				"width_pixels", 2, NULL);
		break;
	default:
		return FALSE;
	}

	gnome_canvas_window_to_world (GNOME_CANVAS (canvas), 
				      event->button.x, event->button.y,
				      &layer->priv->tx, &layer->priv->ty);

	g_signal_connect(G_OBJECT(canvas), "button_release_event",
			 G_CALLBACK(canvas_button_release), layer);
	g_signal_connect(G_OBJECT(canvas), "motion_notify_event",
			 G_CALLBACK(canvas_motion_notify), layer);

	return TRUE;
}

static gboolean
remove_canvas_item (gpointer key, gpointer value, gpointer userdata)
{
	shape_hash_entry_t *entry = (shape_hash_entry_t *) value;

	g_return_val_if_fail (entry, FALSE);

	g_free (key);
	gtk_object_destroy (GTK_OBJECT (entry->item));
	bonobo_object_release_unref (entry->shape, NULL);
	g_free (entry);

	return TRUE;
}

static void
gdraw_shapes_layer_cleanup (GDrawShapesLayer *layer)
{
	Bonobo_EventSource ev_src;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	g_hash_table_foreach_remove (layer->priv->canvas_items, remove_canvas_item, NULL);

	ev_src = Bonobo_Unknown_queryInterface (layer->priv->shapes,
					"IDL:Bonobo/EventSource:1.0", &ev);
	if (!BONOBO_EX (&ev) || (ev_src != CORBA_OBJECT_NIL)) {
		//FIXME: warning: passing arg 2 of `Bonobo_EventSource_removeListener' makes pointer from integer without a cast
		//Bonobo_EventSource_removeListener (
		//	ev_src, layer->priv->list_id, &ev);
		Bonobo_EventSource_removeListener (
				ev_src, BONOBO_OBJREF (layer->priv->listener), &ev);
		Bonobo_Unknown_unref (ev_src, &ev);
	}

	bonobo_object_unref (BONOBO_OBJECT (layer->priv->listener));

	bonobo_object_release_unref (layer->priv->shapes, &ev);
	bonobo_object_release_unref (layer->priv->ui_cont, &ev);

	CORBA_exception_free (&ev);
}

static void
layer_destroy (GtkObject *object)
{
	GDrawShapesLayer *layer = GDRAW_SHAPES_LAYER (object);

	if (layer->priv->activated)
		bonobo_object_unref (BONOBO_OBJECT (layer->priv->ui_comp));

	gdraw_shapes_layer_cleanup (layer);

	g_free (layer->priv);
}

static void
layer_class_init (GtkObjectClass *object_class)
{
	object_class->destroy = layer_destroy;
}

static void
layer_init (GtkObject *object)
{
	GDrawShapesLayer *layer;

	layer = GDRAW_SHAPES_LAYER (object);

	layer->priv = g_new0 (GDrawShapesLayerPrivate, 1);

	layer->priv->group 		= NULL;
	layer->priv->shapes 		= CORBA_OBJECT_NIL;
	layer->priv->canvas_items 	= NULL;
	layer->priv->ui_cont 		= CORBA_OBJECT_NIL;
	layer->priv->ui_comp 		= NULL;
	layer->priv->draw_mode 		= GD_LAYER_MODE_NORMAL;
	layer->priv->activated 		= FALSE;
	layer->priv->tobj 		= NULL;
	layer->priv->selection 		= NULL;
	layer->priv->grid_gran 		= 1;
	layer->priv->def_fill_color	= 0xff0000ff; /* Red.  Why not? */
	layer->priv->def_line_color	= 0xffffffff; /* Black. 	*/
}

GtkType
gdraw_shapes_layer_get_type (void)
{
	static GtkType shapes_layer_type = 0;

	if (!shapes_layer_type) {
		static const GtkTypeInfo shapes_layer_info = {
			"GDrawShapesLayer",
			sizeof (GDrawShapesLayer),
			sizeof (GDrawShapesLayerClass),
			(GtkClassInitFunc) layer_class_init,
			(GtkObjectInitFunc) layer_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		shapes_layer_type = gtk_type_unique (gtk_object_get_type (), 
						     &shapes_layer_info);
	}

	return shapes_layer_type;
}

/**
 * gdraw_shapes_layer_new:
 * @shapes: CORBA ref to a remote shapes object
 * @canvas: The #GnomeCanvas the layer is to reside on.
 * @ui_cont: CORBA ref to a ui_container.
 */
GDrawShapesLayer *
gdraw_shapes_layer_new (GNOME_Draw_Shapes shapes, GnomeCanvas *canvas,
			Bonobo_UIContainer ui_cont)
{
	GDrawShapesLayer *layer;

	g_return_val_if_fail (shapes != CORBA_OBJECT_NIL, NULL);
	g_return_val_if_fail (canvas != NULL, NULL);
	g_return_val_if_fail (ui_cont != CORBA_OBJECT_NIL, NULL);

	layer = gtk_type_new (gdraw_shapes_layer_get_type ());

	layer->priv->shapes = bonobo_object_dup_ref (shapes, NULL);
	layer->priv->ui_cont = bonobo_object_dup_ref (ui_cont, NULL);
	layer->priv->canvas_items = g_hash_table_new (g_str_hash, g_str_equal);
	layer->priv->group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (
					gnome_canvas_root (canvas),
					gnome_canvas_group_get_type (),
					"x", 0.0, "y", 0.0, NULL));

	gdraw_shapes_layer_prepare (layer);

	return layer;
}

void
gdraw_shapes_layer_activate (GDrawShapesLayer *layer)
{
	GnomeCanvas *canvas;

	g_return_if_fail (layer != NULL);
	g_return_if_fail (GDRAW_IS_SHAPES_LAYER (layer));

	if (layer->priv->activated)
		return;

	layer->priv->ui_comp = gdraw_shapes_layer_ui_component_new (
					layer, layer->priv->ui_cont);

	canvas = GNOME_CANVAS_ITEM (layer->priv->group)->canvas;
	g_signal_connect(G_OBJECT(canvas), "button_press_event",
			 G_CALLBACK(canvas_button_press), layer);

	layer->priv->activated = TRUE;
}

void
gdraw_shapes_layer_deactivate (GDrawShapesLayer *layer)
{
	GnomeCanvas *canvas;

	g_return_if_fail (layer != NULL);
	g_return_if_fail (GDRAW_IS_SHAPES_LAYER (layer));

	if (!layer->priv->activated)
		return;

	bonobo_object_unref (BONOBO_OBJECT (layer->priv->ui_comp));

	canvas = GNOME_CANVAS_ITEM (layer->priv->group)->canvas;
	g_signal_handlers_disconnect_by_func(
		G_OBJECT (canvas), G_CALLBACK (canvas_button_press), layer);

	layer->priv->activated = FALSE;
}

void
gdraw_shapes_layer_set_mode (GDrawShapesLayer *layer, GDrawShapesLayerMode mode)
{
	g_return_if_fail (layer != NULL);
	g_return_if_fail (GDRAW_IS_SHAPES_LAYER (layer));

	gdraw_shapes_layer_ui_component_clear_mode (layer->priv->ui_comp,
						    layer->priv->draw_mode);

	if (mode == GD_LAYER_MODE_NORMAL) {
		
		g_free (layer->priv->oaf_iid);
		layer->priv->oaf_iid = NULL;

		if (layer->priv->tobj) {
			gtk_object_destroy (GTK_OBJECT (layer->priv->tobj));
			layer->priv->tobj = NULL;
		}
	}

	layer->priv->draw_mode = mode;
}

void
gdraw_shapes_layer_set_oaf_iid (GDrawShapesLayer *layer, const gchar *oaf_iid)
{
	g_return_if_fail (layer != NULL);
	g_return_if_fail (GDRAW_IS_SHAPES_LAYER (layer));
	g_return_if_fail (oaf_iid != NULL);

	if (layer->priv->oaf_iid)
		g_free (layer->priv->oaf_iid);

	layer->priv->oaf_iid = g_strdup (oaf_iid);
}

void 
gdraw_shapes_layer_get_center (GDrawShapesLayer *layer, gdouble *x, gdouble *y)
{
	gdouble x1, x2, y1, y2;
	GnomeCanvas *canvas;

	canvas = GNOME_CANVAS_ITEM (layer->priv->group)->canvas; 

	gnome_canvas_get_scroll_region (canvas, &x1, &y1, &x2, &y2);

	*x = (x1 + x2) / 2;
	*y = (y1 + y2) / 2;
}

gboolean
gdraw_shapes_layer_add_shape (GDrawShapesLayer *layer, GNOME_Draw_Shape shape)
{
	CORBA_Environment ev;
	gboolean retval = TRUE;

	CORBA_exception_init (&ev);
	
	GNOME_Draw_Shapes_addShape (layer->priv->shapes, shape, &ev);
	if (BONOBO_EX (&ev)) {
		g_warning ("%s exception detected adding shape to collection", 
			   ev._id);
		retval = FALSE;
	}

	CORBA_exception_free (&ev);

	return retval;
}

