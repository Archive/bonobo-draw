/*
 * shape-descriptor.h: Implementation of the ShapeDescriptor properties
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_SHAPE_DESCRIPTOR_H
#define __GDRAW_SHAPE_DESCRIPTOR_H

#include <libxml/tree.h>
#include <bonobo/bonobo-arg.h>
#include <bonobo/bonobo-property-bag.h>

G_BEGIN_DECLS

typedef struct {
	gchar		*name;
	gint		 layer_id;
	gchar		*layer_name;
	gboolean	 printable;
	gboolean	 move_protect;
	gboolean	 size_protect;
} GDrawShapeDescriptor;

/* ShapeDescriptor manipulation API */
void		gdraw_shape_descriptor_add_props (BonoboPropertyBag *bag);

gboolean	gdraw_shape_descriptor_get_prop (GDrawShapeDescriptor *,
						 guint arg_id, BonoboArg *arg);

gboolean	gdraw_shape_descriptor_set_prop (GDrawShapeDescriptor *,
						 guint arg_id, BonoboArg *arg);

gboolean	gdraw_shape_descriptor_load_from_xml (GDrawShapeDescriptor *,
						      xmlNode *);

xmlNode		*gdraw_shape_descriptor_save_to_xml (GDrawShapeDescriptor *,
						     xmlNode *);

G_END_DECLS

#endif
