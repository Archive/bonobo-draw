/*
 * re-shape-io.c: REShape class persistence implementation
 *
 * Copyright (c) 2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 */

#include "shape-io.h"
#include "re-shape-io.h"

gboolean
gdraw_re_shape_load_from_xml (GDrawREShape *re, xmlNode *node)
{
	xmlNode *child;

	if (g_strcasecmp (node->name, "REShape"))
		return FALSE;

	for (child = node->children; child; child = child->next) {
		if (!g_strcasecmp (child->name, "FillProperties")) {
			if (!gdraw_fill_properties_load_from_xml (
						re->fill, child))
				return FALSE;
		} else if (!g_strcasecmp (child->name, "LineProperties")) {
			if (!gdraw_line_properties_load_from_xml (
						re->line, child))
				return FALSE;
		} else if (!g_strcasecmp (child->name, "ShadowProperties")) {
			if (!gdraw_shadow_properties_load_from_xml (
						re->shadow, child))
				return FALSE;
		} else if (!g_strcasecmp (child->name, "RotationDesc")) {
			if (!gdraw_rotation_descriptor_load_from_xml (
						re->rotation, child))
				return FALSE;
		} else {
			if (!gdraw_shape_load_from_xml (
						GDRAW_SHAPE (re), child))
				return FALSE;
		}
	}

	return TRUE;
}

xmlNode *
gdraw_re_shape_save_to_xml (GDrawREShape *re, xmlNode *parent)
{
	xmlNode *child;

	child = xmlNewChild (parent, NULL, "REShape", NULL);

	if (!child)
		return NULL;

	if (!gdraw_fill_properties_save_to_xml (re->fill, child))
		return NULL;

	if (!gdraw_line_properties_save_to_xml (re->line, child))
		return NULL;

	if (!gdraw_shadow_properties_save_to_xml (re->shadow, child))
		return NULL;

	if (!gdraw_rotation_descriptor_save_to_xml (re->rotation, child))
		return NULL;

	if (!gdraw_shape_save_to_xml (GDRAW_SHAPE (re), child))
		return NULL;

	return child;
}

