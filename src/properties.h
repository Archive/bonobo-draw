/*
 * properties.h: Definition of the Property enumeration
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_PROPS_H
#define __GDRAW_PROPS_H

typedef enum {
	/* ShapeProperties */
	PROP_LAYER_ID,
	PROP_LAYER_NAME,
	PROP_PRINTABLE,
	PROP_MOVE_PROTECT,
	PROP_NAME,
	PROP_SIZE_PROTECT,

	/* FillProperties */
	PROP_FILL_STYLE,
	PROP_FILL_COLOR,
	PROP_FILL_TRANSPARENCY,
	PROP_FILL_RGBA,
	/* FIXME: Lots of cool fill options to add here */

	/* LineProperties */
	PROP_LINE_STYLE,
	PROP_LINE_COLOR,
	PROP_LINE_TRANSPARENCY,
	PROP_LINE_RGBA,
	PROP_LINE_WIDTH,
	/* FIXME: Some funky line props left */

	/* ShadowProperties */
	PROP_SHADOW,
	PROP_SHADOW_COLOR,
	PROP_SHADOW_TRANSPARENCY,
	PROP_SHADOW_RGBA,
	PROP_SHADOW_X_DISTANCE,
	PROP_SHADOW_Y_DISTANCE,

	/* RotationDescriptor properties */
	PROP_ROTATE_ANGLE,
	PROP_ROTATE_POINT_X,
	PROP_ROTATE_POINT_Y,
	/* FIXME: PROP_ROTATE_SHEAR, */

	/* RectangleShape properties */
	PROP_CORNER_RADIUS,

	/* ImageShape properties */
	PROP_GRAPHIC_URL,

} GDrawShapeProps;

#endif
