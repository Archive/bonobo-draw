/*
 * factory.h: Implementation of the GDraw factory
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_FACTORY_H
#define __GDRAW_FACTORY_H

#include <bonobo.h>

G_BEGIN_DECLS

void gdraw_factory_init (void);

G_END_DECLS

#endif
