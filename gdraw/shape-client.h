/*
 * shape-client.h: Interface for the Shape client helper functions.
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_SHAPE_CLIENT_H
#define __GDRAW_SHAPE_CLIENT_H

#include <gdraw/GNOME_Draw.h>

G_BEGIN_DECLS

gboolean gdraw_shape_client_is_moveable (GNOME_Draw_Shape, CORBA_Environment *);

G_END_DECLS

#endif
