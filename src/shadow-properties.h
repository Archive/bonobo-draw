/*
 * shadow-properties.h: Implementation of the ShadowProperties
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 */

#ifndef __GDRAW_SHADOW_PROPERTIES_H
#define __GDRAW_SHADOW_PROPERTIES_H

#include <bonobo.h>
#include <libxml/tree.h>

G_BEGIN_DECLS

typedef struct {
  	gboolean	 	 visible;
	gint32		 	 rgba;
	gint32			 x_dist;
	gint32			 y_dist;
} GDrawShadowProperties;

/* ShadowProperties manipulation API */
void	   gdraw_shadow_properties_add_props (BonoboPropertyBag *bag);

gboolean   gdraw_shadow_properties_get_prop (GDrawShadowProperties *,
					     guint arg_id, BonoboArg *arg);

gboolean   gdraw_shadow_properties_set_prop (GDrawShadowProperties *,
					     guint arg_id, BonoboArg *arg);

gboolean   gdraw_shadow_properties_load_from_xml (GDrawShadowProperties *,
						  xmlNode *);

xmlNode   *gdraw_shadow_properties_save_to_xml (GDrawShadowProperties *,
						xmlNode *);

G_END_DECLS

#endif
