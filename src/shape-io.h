/*
 * shape-io.h: Shape persistence interface
 *
 * Copyright (c) 2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_SHAPE_IO_H
#define __GDRAW_SHAPE_IO_H

#include <libxml/tree.h>
#include "shape.h"

G_BEGIN_DECLS

gboolean gdraw_shape_load_from_xml (GDrawShape *, xmlNode *);
xmlNode *gdraw_shape_save_to_xml (GDrawShape *, xmlNode *);

G_END_DECLS

#endif
