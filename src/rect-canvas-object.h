/*
 * rect-canvas-object.h: Implementation of the RectCanvasObject class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_RECT_CANVAS_OBJECT_H
#define __GDRAW_RECT_CANVAS_OBJECT_H

#include <gtk/gtktypeutils.h>
#include <gdraw/GNOME_Draw.h>

G_BEGIN_DECLS

typedef struct _GDrawRectCanvasObject         GDrawRectCanvasObject;
typedef struct _GDrawRectCanvasObjectClass    GDrawRectCanvasObjectClass;

#include "canvas-object.h"
#include "rect-shape.h"

#define GDRAW_RECT_CANVAS_OBJECT_TYPE  (gdraw_rect_canvas_object_get_type())
#define GDRAW_RECT_CANVAS_OBJECT(obj)  (GTK_CHECK_CAST((obj),	\
					GDRAW_RECT_CANVAS_OBJECT_TYPE, 	\
					GDrawRectCanvasObject))
#define GDRAW_RECT_CANVAS_OBJECT_CLASS(klass)  (GTK_CHECK_CLASS_CAST((klass), \
                               	     	        GDRAW_RECT_CANVAS_OBJECT_TYPE,\
					        GDrawRectCanvasObjectClass))
#define IS_GDRAW_RECT_CANVAS_OBJECT(obj)       (GTK_CHECK_TYPE((obj), 	\
					        GDRAW_RECT_CANVAS_OBJECT_TYPE))
#define IS_GDRAW_RECT_CANVAS_OBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), \
					          GDRAW_RECT_CANVAS_OBJECT_TYPE))

struct _GDrawRectCanvasObject {
	GDrawCanvasObject 	parent_object;

	GnomeCanvasItem 	*canvas_item;
	GnomeCanvasItem 	*shadow_item;
};
	
struct _GDrawRectCanvasObjectClass {
	GDrawCanvasObjectClass parent_class;
};

GtkType gdraw_rect_canvas_object_get_type (void);

GDrawCanvasObject *gdraw_rect_canvas_object_new (GDrawRectShape *,
						 GnomeCanvas *);

G_END_DECLS

#endif

