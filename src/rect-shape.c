/*
 * rect-shape.c: Implementation of the RectShape class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <gdraw/oafiids.h>
#include <libgnomecanvas/gnome-canvas.h>
#include "rect-canvas-object.h"
#include "rect-shape.h"
#include "rect-shape-io.h"

static void
gdraw_rect_shape_destroy (GtkObject *object)
{
	GDrawRectShape *rect = GDRAW_RECT_SHAPE (object);

	bonobo_object_unref (BONOBO_OBJECT (rect->bag));
	bonobo_object_unref (BONOBO_OBJECT (rect->embeddable));
	bonobo_object_unref (BONOBO_OBJECT (rect->stream));
}

static void
gdraw_rect_shape_class_init (GtkObjectClass *klass)
{
	klass->destroy = gdraw_rect_shape_destroy;
}

static void
gdraw_rect_shape_init (GtkObject *object)
{
	GDrawRectShape *rect;

	g_return_if_fail (object);

	rect = GDRAW_RECT_SHAPE (object);

	rect->bag = NULL;
	rect->embeddable = NULL;
	rect->stream = NULL;
	rect->corner_radius = 0;
}

GType
gdraw_rect_shape_get_type(void)
{
	static GType rect_type = 0;

	if (!rect_type) {
		static const GTypeInfo rect_info = {
			sizeof(GDrawRectShapeClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gdraw_rect_shape_class_init,
			NULL, NULL,
			sizeof(GDrawRectShape),
			0,
			(GInstanceInitFunc) gdraw_rect_shape_init
		};

		rect_type = g_type_register_static(
			gdraw_re_shape_get_type(), "GDrawRectShape", 
			&rect_info, 0);
	}

	return rect_type;
}

static void
get_prop (BonoboPropertyBag *bag, BonoboArg *arg, 
	  guint arg_id, CORBA_Environment *ev, GDrawRectShape *rect)
{
	if (!gdraw_re_shape_get_prop (GDRAW_RE_SHAPE (rect), arg_id, arg))
		g_warning ("Unhandled property %d", arg_id);
}

static void
set_prop (BonoboPropertyBag *bag, BonoboArg *arg,
	  guint arg_id, CORBA_Environment *ev, GDrawRectShape *rect)
{
	if (gdraw_re_shape_set_prop (GDRAW_RE_SHAPE (rect), arg_id, arg))
		g_signal_emit_by_name(G_OBJECT(rect), "changed");
	else
		g_warning ("Unhandled property %d", arg_id);
}

static BonoboPropertyBag *
create_property_bag (GDrawRectShape *rect)
{
	BonoboPropertyBag *bag = bonobo_property_bag_new (
					(BonoboPropertyGetFn) get_prop, 
					(BonoboPropertySetFn) set_prop,
					rect);

	gdraw_re_shape_add_props (bag);

	bonobo_object_add_interface (BONOBO_OBJECT (rect), 
				     BONOBO_OBJECT (bag));

	return bag;
}

static GDrawCanvasObject *
create_canvas_object (GDrawRectShape *rect, GnomeCanvas *canvas)
{
	g_return_val_if_fail (rect != NULL, NULL);
	g_return_val_if_fail (canvas != NULL, NULL);

	return GDRAW_CANVAS_OBJECT (gdraw_rect_canvas_object_new (rect, canvas));
}

static BonoboCanvasComponent *
rect_item_creator (BonoboObject *embeddable, GnomeCanvas *canvas,
		   gpointer data)
{
	BonoboCanvasComponent *component;
	GDrawCanvasObject *cobj;
	GDrawRectShape *shape = GDRAW_RECT_SHAPE (data);

	cobj = create_canvas_object (shape, canvas);

	component = bonobo_canvas_component_new (GNOME_CANVAS_ITEM (
							cobj->object_group));

	if (component == NULL) {
		gtk_object_destroy (GTK_OBJECT (cobj));
		return NULL;
	}

	gdraw_canvas_object_set_component (cobj, component);

	return component;
}

GDrawRectShape *
gdraw_rect_shape_new ()
{
	GDrawShape *shape;
	GDrawRectShape *rect;
	BonoboObject *embeddable;
	GDrawRectStream *stream;
	static gint count = 0;

	rect = g_object_new (GDRAW_RECT_SHAPE_TYPE, NULL);
	shape = GDRAW_SHAPE (rect);

	shape->desc->name = g_strdup_printf ("rect%d", count++);
	shape->type = g_strdup (GDRAW_RECT_OAFIID);

        rect->bag = create_property_bag (rect);

	//FIXME
	//embeddable = bonobo_embeddable_new_canvas_item (rect_item_creator,
	//					        rect);
	embeddable = NULL;

	bonobo_object_add_interface (BONOBO_OBJECT (rect), 
				     BONOBO_OBJECT (embeddable));

	rect->embeddable = embeddable;

	stream = gdraw_rect_stream_new (rect);

	if (!stream) {
		bonobo_object_unref (BONOBO_OBJECT (rect));
		return NULL;
	}

	bonobo_object_add_interface (BONOBO_OBJECT (rect),
				     BONOBO_OBJECT (stream));

	rect->stream = stream;

	return rect;
}

BonoboObject *
gdraw_rect_shape_factory (BonoboGenericFactory *this)
{
	GDrawRectShape *rect;

	rect = gdraw_rect_shape_new ();

	return BONOBO_OBJECT (rect);
}


