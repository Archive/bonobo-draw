/*
 * shapes-io.c: Persistence Interface for the Shapes class
 *
 * Copyright (c) 2001 Mike Kestner 
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_SHAPES_IO_H
#define __GDRAW_SHAPES_IO_H

#include <bonobo/bonobo-persist.h>

G_BEGIN_DECLS

typedef struct _GDrawShapesStorage      GDrawShapesStorage;
typedef struct _GDrawShapesStorageClass GDrawShapesStorageClass;

#include "shapes.h"

#define GDRAW_SHAPES_STORAGE_TYPE      	 (gdraw_shapes_storage_get_type())
#define GDRAW_SHAPES_STORAGE(o)          (G_TYPE_CHECK_INSTANCE_CAST((o), GDRAW_SHAPES_STORAGE_TYPE, GDrawShapesStorage))
#define GDRAW_SHAPES_STORAGE_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), GDRAW_SHAPES_STORAGE_TYPE, GDrawShapesStorageClass))
#define GDRAW_IS_SHAPES_STORAGE(o)       (G_TYPE_CHECK_CLASS_TYPE((o), GDRAW_SHAPES_STORAGE_TYPE))
#define GDRAW_IS_SHAPES_STORAGE_CLASS(k) (G_TYPE_INSTANCE_GET_CLASS((k), GDRAW_SHAPES_STORAGE_TYPE))

struct _GDrawShapesStorage {
	BonoboPersist	 	base;

	GDrawShapes		*shapes;
	gboolean		is_dirty;
};
	
struct _GDrawShapesStorageClass {
	BonoboPersistClass parent_class;

	POA_Bonobo_PersistStorage__epv epv;
};

GType gdraw_shapes_storage_get_type (void);

GDrawShapesStorage *gdraw_shapes_storage_new (GDrawShapes *shapes);

G_END_DECLS

#endif /* __GDRAW_SHAPES_IO_H */
