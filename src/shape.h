/*
 * shape.h: Implementation of the Shape abstract class
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_SHAPE_H
#define __GDRAW_SHAPE_H

#include <bonobo.h>
#include <gdraw/GNOME_Draw.h>
#include "shape-descriptor.h"

G_BEGIN_DECLS

typedef struct _GDrawShape         GDrawShape;
typedef struct _GDrawShapeClass    GDrawShapeClass;

#define GDRAW_SHAPE_TYPE            (gdraw_shape_get_type ())
#define GDRAW_SHAPE(obj)            (GTK_CHECK_CAST((obj), GDRAW_SHAPE_TYPE, GDrawShape))
#define GDRAW_SHAPE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
                               	     GDRAW_SHAPE_TYPE, GDrawShapeClass))
#define GDRAW_IS_SHAPE(obj)         (GTK_CHECK_TYPE ((obj), GDRAW_SHAPE_TYPE))
#define GDRAW_IS_SHAPE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GDRAW_SHAPE_TYPE))

struct _GDrawShape {
	BonoboObject		parent_object;

	gdouble 		x, y;
	gdouble			width, height;

	gchar			*type;
	GDrawShapeDescriptor 	*desc;
};
	
struct _GDrawShapeClass {
	BonoboObjectClass 	parent_class;

	POA_GNOME_Draw_Shape__epv epv;

	void (* changed)(GDrawShape *shape);
};

/* Standard GtkObject function */
GtkType gdraw_shape_get_type (void);

/* Shape transformation API */
void gdraw_shape_set_position (GDrawShape *shape, gdouble x, gdouble y);
void gdraw_shape_set_size (GDrawShape *shape, gdouble width, gdouble height);

/* Shape descriptor API */
void gdraw_shape_set_move_protect (GDrawShape *, gboolean protect);
void gdraw_shape_set_size_protect (GDrawShape *, gboolean protect);
void gdraw_shape_set_printable (GDrawShape *, gboolean printable);

G_END_DECLS

#endif
