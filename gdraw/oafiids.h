/*
 * oafiids.h: IIDs for the GDraw Objects
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_OAFIIDS_H
#define __GDRAW_OAFIIDS_H

#define GDRAW_FACTORY_OAFIID 	"OAFIID:GNOME/Draw/Factory:d1cde142-c28f-454d-aa75-ffeee4191f98"
#define GDRAW_SHAPES_OAFIID 	"OAFIID:GNOME/Draw/Shapes:57a6631c-08d2-4887-bbf1-6530eb0b8f50"
#define GDRAW_RECT_OAFIID 	"OAFIID:GNOME/Draw/Rect:2b7972b3-a08c-41d0-a608-ef2ad0915e88"
#define GDRAW_ELLIPSE_OAFIID 	"OAFIID:GNOME/Draw/Ellipse:0ed3871f-cf03-420a-84ae-eccb3ec538fc"
#define GDRAW_IMAGE_OAFIID 	"OAFIID:GNOME/Draw/Image:d14131dd-d025-4042-beaa-f9eac5cd7e1d"

#endif
