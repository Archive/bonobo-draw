/*
 * shapes.c: Implements the Shapes aggregation class
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Authors: Mike Kestner <mkestner@ameritech.net>
 */

#include <config.h>
#include <bonobo.h>
#include <gdraw/GNOME_Draw.h>
#include "shapes.h"

#define PARENT_TYPE BONOBO_OBJECT_TYPE
#define SHAPE_ADDED_EVENT "GNOME/Draw/ShapeAddedEvent"
#define SHAPE_REMOVED_EVENT "GNOME/Draw/ShapeRemovedEvent"

static inline GDrawShapes *
gdraw_shapes_from_servant (PortableServer_Servant servant)
{
	return GDRAW_SHAPES (bonobo_object_from_servant (servant));
}	   

static void
emit_added_signal (GDrawShapes *shapes, gchar *name, GNOME_Draw_Shape shape)
{
	GNOME_Draw_ShapeAddedEvent *sae = GNOME_Draw_ShapeAddedEvent__alloc ();
	CORBA_any *arg = CORBA_any__alloc ();

	arg->_type = TC_GNOME_Draw_ShapeAddedEvent;
	arg->_value = sae;

	sae->name = name;
	sae->add_shape = shape;

	bonobo_event_source_notify_listeners (shapes->ev_src, 
					      SHAPE_ADDED_EVENT,
					      arg, NULL);

	CORBA_free (arg);
}

static void
emit_removed_signal (GDrawShapes *shapes, gchar *name)
{
	CORBA_any *arg = CORBA_any__alloc ();

	arg->_type->kind = CORBA_tk_string;
	arg->_value = name;

	bonobo_event_source_notify_listeners (shapes->ev_src, 
					      SHAPE_REMOVED_EVENT,
					      arg, NULL);

	CORBA_free (arg);
}

void
gdraw_shapes_add_shape (GDrawShapes *shapes, GNOME_Draw_Shape shape)
{
	CORBA_Environment ev;
	Bonobo_PropertyBag bag;
	GNOME_Draw_Shape dup_shape;
	gchar *name;

	CORBA_exception_init (&ev);

	bag = Bonobo_Unknown_queryInterface (
				shape, "IDL:Bonobo/PropertyBag:1.0", &ev);
	if (BONOBO_EX (&ev)) {
		g_warning ("PBag QI failed - %s", 
			   bonobo_exception_get_text (&ev));
		return;
	}

	name = bonobo_property_bag_client_get_value_string (bag, "Name", &ev);
	Bonobo_Unknown_unref (bag, &ev);
	if (BONOBO_EX (&ev)) {
		g_warning ("Couldn't get shape name - %s", 
			   bonobo_exception_get_text (&ev));
		return;
	}

	if (g_hash_table_lookup (shapes->shape_by_name, name)) {
		g_warning ("Shape already in collection");
		g_free (name);
		return;
	}

	dup_shape = bonobo_object_dup_ref (shape, &ev);

	g_hash_table_insert (shapes->shape_by_name, name, dup_shape);
	shapes->shape_names = g_list_prepend (shapes->shape_names, name);

	emit_added_signal (shapes, name, dup_shape);
}

static void
Shapes_add_shape (PortableServer_Servant servant, GNOME_Draw_Shape shape,
		  CORBA_Environment *ev)
{
	GDrawShapes *shapes = gdraw_shapes_from_servant (servant);

	gdraw_shapes_add_shape (shapes, shape);
}

static void
Shapes_remove_shape (PortableServer_Servant servant, GNOME_Draw_Shape shape,
		     CORBA_Environment * ev)
{
	GNOME_Draw_Shape orig_shape;
	Bonobo_PropertyBag bag;
	gchar *name, *orig_key;

	GDrawShapes *shapes = gdraw_shapes_from_servant (servant);

	bag = Bonobo_Unknown_queryInterface (
				shape, "Bonobo/PropertyBag:1.0", ev);
	if (BONOBO_EX (ev))
		return;

	name = bonobo_property_bag_client_get_value_string (bag, "Name", ev);
	Bonobo_Unknown_unref (bag, ev);
	if (BONOBO_EX (ev))
		return;

	if (!g_hash_table_lookup_extended (shapes->shape_by_name, name, 
					   (gpointer *) &orig_key, 
					   (gpointer *) &orig_shape)) {
		/* FIXME: This should raise an exception */
		g_free (name);
		return;
	}

	g_hash_table_remove (shapes->shape_by_name, name);
	shapes->shape_names = g_list_remove (shapes->shape_names, orig_key);
	g_free (orig_key);

	bonobo_object_release_unref (orig_shape, ev);
	Bonobo_Unknown_unref (bag, ev);

	emit_removed_signal (shapes, name);
	g_free (name);
}

static GNOME_Draw_Shapes_ShapeNames *
Shapes_get_shape_names (PortableServer_Servant servant, CORBA_Environment * ev)
{
	GDrawShapes *shapes = gdraw_shapes_from_servant (servant);
	GNOME_Draw_Shapes_ShapeNames *name_seq;
	GList *l;
	gint i, len;

	name_seq = GNOME_Draw_Shapes_ShapeNames__alloc ();
	if (!name_seq) {
		g_warning ("Unable to allocate sequence");
		return NULL;
	}

	len = g_list_length (shapes->shape_names);

	name_seq->_length = name_seq->_maximum = len;

	if (len == 0)
		return name_seq;

	name_seq->_buffer = CORBA_sequence_CORBA_string_allocbuf (len);
	if (!name_seq->_buffer) {
		CORBA_free (name_seq);
		return NULL;
	}

	for (i = 0, l = shapes->shape_names; i < len; i++, l = l->next)
		name_seq->_buffer [i] = CORBA_string_dup (l->data);

	return name_seq;
}

static GNOME_Draw_Shape
Shapes_get_shape_by_name (PortableServer_Servant servant, 
			  const CORBA_char *name,
			  CORBA_Environment * ev)
{
	GDrawShapes *shapes = gdraw_shapes_from_servant (servant);
	GNOME_Draw_Shape shape;

	shape = (GNOME_Draw_Shape) g_hash_table_lookup (shapes->shape_by_name,
							name);

	return bonobo_object_dup_ref (shape, NULL);
}

static gboolean
remove_entry (gpointer key, gpointer value, gpointer user_data)
{
	gchar *name = (gchar *) key;
	CORBA_Object shape = (CORBA_Object) value;

	g_free (name);
	bonobo_object_release_unref (shape, NULL);

	return TRUE;
}

static void
gdraw_shapes_finalize (GObject *obj)
{
	GDrawShapes *shapes = GDRAW_SHAPES (obj);
	GSList *l;

	g_hash_table_foreach_remove (shapes->shape_by_name, 
				     remove_entry, NULL);
	g_hash_table_destroy (shapes->shape_by_name);

	g_list_free (shapes->shape_names);

	bonobo_object_unref (BONOBO_OBJECT (shapes->storage));
	bonobo_object_unref (BONOBO_OBJECT (shapes->ev_src));
}

static void
gdraw_shapes_class_init (GObjectClass *klass)
{
	GDrawShapesClass *shapes_class = GDRAW_SHAPES_CLASS (klass);
	POA_GNOME_Draw_Shapes__epv *epv = &shapes_class->epv;

	epv->addShape = Shapes_add_shape;
	epv->removeShape = Shapes_remove_shape;
	epv->getShapeNames = Shapes_get_shape_names;
	epv->getShapeByName = Shapes_get_shape_by_name;

	klass->finalize = gdraw_shapes_finalize;
}

static void
gdraw_shapes_init (GObject *object)
{
	GDrawShapes *shapes = GDRAW_SHAPES (object);

	shapes->shape_by_name = g_hash_table_new (g_str_hash, g_str_equal);
	shapes->shape_names = NULL;
	shapes->ev_src = NULL;
	shapes->storage = NULL;
}

BONOBO_TYPE_FUNC_FULL (GDrawShapes, GNOME_Draw_Shapes, 
		       PARENT_TYPE, gdraw_shapes);

GDrawShapes *
gdraw_shapes_new ()
{
	GDrawShapes *shapes;
	GDrawShapesStorage *storage;
	BonoboEventSource *ev_src;

	shapes = g_object_new (GDRAW_SHAPES_TYPE, NULL);

	storage = gdraw_shapes_storage_new (shapes);
	if (!storage) {
		bonobo_object_unref (BONOBO_OBJECT (shapes));
		return NULL;
	}
	bonobo_object_add_interface (BONOBO_OBJECT (shapes),
				     BONOBO_OBJECT (storage));
	shapes->storage = storage;

	ev_src = bonobo_event_source_new ();
	if (!ev_src) {
		bonobo_object_unref (BONOBO_OBJECT (shapes));
		return NULL;
	}
	bonobo_object_add_interface (BONOBO_OBJECT (shapes),
				     BONOBO_OBJECT (ev_src));
	shapes->ev_src = ev_src;

	return shapes;
}

BonoboObject *
gdraw_shapes_factory (BonoboGenericFactory *this)
{
	GDrawShapes *shapes;

	shapes = gdraw_shapes_new ();

	return BONOBO_OBJECT (shapes);
}

