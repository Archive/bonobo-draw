/*
 * ellipse-canvas-object.c: Implementation of the EllipseCanvasObject class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <libgnomecanvas/gnome-canvas-rect-ellipse.h>
#include "ellipse-canvas-object.h"


static void
gdraw_ellipse_canvas_object_destroy (GtkObject *object)
{
	GDrawCanvasObject *cobj = GDRAW_CANVAS_OBJECT (object);

	gtk_object_destroy (GTK_OBJECT (cobj->object_group));

}

static void
ellipse_update_fill (GnomeCanvasItem *item, GDrawFillProperties *fill)
{
	switch (fill->fill_style)  {

	case GNOME_Draw_FILLSOLID:
		gnome_canvas_item_set (item, "fill_color_rgba", fill->fill_rgba,
				       NULL);
		break;
	case GNOME_Draw_FILLNONE:
		gnome_canvas_item_set (item, "fill_color", NULL, NULL);
		break;
	default:
		g_warning ("Unsupported fill style %d", fill->fill_style);
	}
}

static void
ellipse_update_line (GnomeCanvasItem *item, GDrawLineProperties *line)
{
	switch (line->style)  {

	case GNOME_Draw_LINESOLID:
		gnome_canvas_item_set (item, "outline_color_rgba", 
				       line->rgba, NULL);
		break;
	case GNOME_Draw_LINENONE:
		gnome_canvas_item_set (item, "outline_color", NULL, NULL);
		break;
	default:
		g_warning ("Unsupported line style %d", line->style);
	}
}

static void
ellipse_update_shadow (GDrawEllipseCanvasObject *ecobj, 
		       GDrawShadowProperties *shadow)
{
	GDrawCanvasObject *cobj = GDRAW_CANVAS_OBJECT (ecobj);
	GDrawShape *shape = cobj->shape;

	if (shadow->visible) {
		if (ecobj->shadow_item)
			ecobj->shadow_item = gnome_canvas_item_new (
				GNOME_CANVAS_GROUP (cobj->object_group),
				gnome_canvas_ellipse_get_type (),
				"x1", shadow->x_dist,
				"y1", shadow->y_dist,
				"x2", shape->width + shadow->x_dist,
				"y2", shape->height + shadow->y_dist,
				"fill_color_rgba", shadow->rgba, NULL);
		else
			gnome_canvas_item_set (
				ecobj->shadow_item,
				"x1", shadow->x_dist,
				"y1", shadow->y_dist,
				"x2", shape->width + shadow->x_dist,
				"y2", shape->height + shadow->y_dist,
				"fill_color_rgba", shadow->rgba, NULL);
	} else if (ecobj->shadow_item) {
		gtk_object_destroy (GTK_OBJECT (ecobj->shadow_item));
		ecobj->shadow_item = NULL;
	}
}

static void
redraw_ellipse_cobj (GDrawShape *shape, GDrawEllipseCanvasObject *ecobj)
{
	GDrawREShape *re = GDRAW_RE_SHAPE (shape);
	GDrawCanvasObject *cobj = GDRAW_CANVAS_OBJECT (ecobj);
	gdouble affine[6];

	art_affine_translate (affine, cobj->shape->x, cobj->shape->y);

	gnome_canvas_item_affine_absolute (cobj->object_group, affine);


	gnome_canvas_item_set (ecobj->canvas_item,
			      "x1", 0.0, "y1", 0.0,
			      "x2", shape->width,
			      "y2", shape->height,
			      NULL);

	ellipse_update_fill (ecobj->canvas_item, re->fill);
	ellipse_update_line (ecobj->canvas_item, re->line);
	ellipse_update_shadow (ecobj, re->shadow);
}

static void
gdraw_ellipse_canvas_object_class_init (GtkObjectClass *klass)
{
	klass->destroy = gdraw_ellipse_canvas_object_destroy;
}

static void
gdraw_ellipse_canvas_object_init (GtkObject *object)
{
	GDrawEllipseCanvasObject *cobj;

	g_return_if_fail (object);

	cobj = GDRAW_ELLIPSE_CANVAS_OBJECT (object);

	cobj->canvas_item = NULL;
	cobj->shadow_item = NULL;
}

GtkType
gdraw_ellipse_canvas_object_get_type (void)
{
	static GtkType cobj_type = 0;

	if (!cobj_type) {
		static const GtkTypeInfo cobj_info = {
			"GDrawEllipseCanvasObject",
			sizeof (GDrawEllipseCanvasObject),
			sizeof (GDrawEllipseCanvasObjectClass),
			(GtkClassInitFunc) gdraw_ellipse_canvas_object_class_init,
			(GtkObjectInitFunc) gdraw_ellipse_canvas_object_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		cobj_type = gtk_type_unique (gdraw_canvas_object_get_type (), 
					    &cobj_info);
	}

	return cobj_type;
}

static void
create_ellipse_cobj (GDrawEllipseCanvasObject *ecobj)
{
	GDrawCanvasObject *cobj = GDRAW_CANVAS_OBJECT (ecobj);

	ecobj->canvas_item = gnome_canvas_item_new (
				GNOME_CANVAS_GROUP (cobj->object_group),
				gnome_canvas_ellipse_get_type (), NULL);

	redraw_ellipse_cobj (cobj->shape, ecobj);
}

GDrawCanvasObject *
gdraw_ellipse_canvas_object_new (GDrawEllipseShape *ellipse, 
			        GnomeCanvas *canvas)
{
	GDrawCanvasObject *cobj;
	GDrawEllipseCanvasObject *ecobj;

	g_return_val_if_fail (ellipse != NULL, NULL);
	g_return_val_if_fail (canvas != NULL, NULL);

	ecobj = gtk_type_new (GDRAW_ELLIPSE_CANVAS_OBJECT_TYPE);

	cobj = GDRAW_CANVAS_OBJECT (ecobj);

	gdraw_canvas_object_construct (cobj, canvas, GDRAW_SHAPE (ellipse));

	create_ellipse_cobj (ecobj);

	g_signal_connect (G_OBJECT (ellipse), "changed",
			  G_CALLBACK (redraw_ellipse_cobj), cobj);

	return cobj;
}
