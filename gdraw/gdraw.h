/**
 * Main include file for the GNOME::Draw component helper library.
 *
 * Copyright 2001 Mike Kestner
 */

#ifndef GDRAW_H
#define GDRAW_H 1

#include <gdraw/GNOME_Draw.h>

#include <gdraw/shape-client.h>
#include <gdraw/shapes-layer.h>
#include <gdraw/shapes-listener.h>

#endif /* GDRAW_H */
