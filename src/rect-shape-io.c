/*
 * rect-shape-io.c: RectShape class PersistStream implementation
 *
 * Copyright (c) 2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 */

#include <config.h>
#include <libxml/parser.h>
#include <bonobo/bonobo-xobject.h>
#include <bonobo/Bonobo.h>
#include "re-shape-io.h"
#include "rect-shape-io.h"

#define PARENT_TYPE BONOBO_PERSIST_TYPE

static gboolean
load_from_xml (GDrawRectShape *rect, xmlNode *node)
{
	xmlNode *child;

	if (g_strcasecmp (node->name, "RectShape"))
		return FALSE;

	for (child = node->children; child; child = child->next) {

		if (!g_strcasecmp (child->name, "RectProperties")) {

			gchar *prop;

			prop = xmlGetProp (child, "CornerRadius");
			if (!prop)
				return FALSE;

			rect->corner_radius = strtol (prop, NULL, 10);
			g_free (prop);

		} else {

			if (!gdraw_re_shape_load_from_xml (
					GDRAW_RE_SHAPE (rect), child))
				return FALSE;

		}

	}

	return TRUE;
}

static xmlNode *
save_to_xml (GDrawRectShape *rect, xmlNode *parent)
{
	xmlNode *rp;
	gchar *prop;

	rp = xmlNewChild (parent, NULL, "RectProperties", NULL);
	if (!rp)
		return NULL;

	prop = g_strdup_printf ("%d", rect->corner_radius);
	if (!prop) {
		xmlFreeNode (rp);
		return NULL;
	}
	xmlSetProp (rp, "CornerRadius", prop);
	g_free (prop);

	if (!gdraw_re_shape_save_to_xml (GDRAW_RE_SHAPE (rect), parent))
		return NULL;

	return parent;
}

static GDrawRectStream *
gdraw_rect_stream_from_servant (PortableServer_Servant servant)
{
	return GDRAW_RECT_STREAM (bonobo_object_from_servant (servant));
}

static CORBA_boolean
RectStream_is_dirty (PortableServer_Servant servant, CORBA_Environment *ev)
{
	GDrawRectStream *grs = gdraw_rect_stream_from_servant (servant);

	return grs->is_dirty;
}

static void
RectStream_load (PortableServer_Servant servant, const Bonobo_Stream stream,
		 Bonobo_Persist_ContentType type, CORBA_Environment *ev)
{
	GDrawRectStream *grs = gdraw_rect_stream_from_servant (servant);
	GDrawRectShape *rect = grs->rect;

	if (!g_strcasecmp (type, "text/xml") || !g_strcasecmp (type, "")) {
		xmlDoc *doc;
		gchar *str = NULL;

		bonobo_stream_client_read_string (stream, &str, ev);
		doc = xmlParseMemory (str, strlen (str));
		if (!doc || !load_from_xml (rect, xmlDocGetRootElement(doc)))
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
					     ex_Bonobo_Persist_WrongDataType, 
					     NULL);
		xmlFreeDoc (doc);
		g_free (str);
	} else
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
				     ex_Bonobo_NotSupported, NULL);
}

static void
RectStream_save (PortableServer_Servant servant, const Bonobo_Stream stream,
		 Bonobo_Persist_ContentType type, CORBA_Environment *ev)
{
	GDrawRectStream *grs = gdraw_rect_stream_from_servant (servant);
	GDrawRectShape *rect = grs->rect;

	if (!g_strcasecmp (type, "text/xml") || !g_strcasecmp (type, "")) {
		xmlDoc *doc;
		xmlChar *str;
		gint len;

		doc = xmlNewDoc ("1.0");
		doc->children = xmlNewDocNode (doc, NULL, "RectShape", NULL);

		if( !save_to_xml (rect, xmlDocGetRootElement(doc))) {
			xmlFreeDoc (doc);
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
					     ex_Bonobo_Stream_IOError, 
					     NULL);
			return;
		}
		
		xmlDocDumpMemory (doc, &str, &len);
		bonobo_stream_client_write_string (stream, str, TRUE, ev);
		xmlFreeDoc (doc);
		g_free (str);
	}
}

static Bonobo_Persist_ContentTypeList *
get_content_types (BonoboPersist *persist, CORBA_Environment *ev)
{
	return bonobo_persist_generate_content_types (1, "text/xml");
}

static void
gdraw_rect_stream_class_init (GObjectClass *klass)
{
	GDrawRectStreamClass *rsc = GDRAW_RECT_STREAM_CLASS (klass);
	BonoboPersistClass *bpc = BONOBO_PERSIST_CLASS (klass);
	POA_Bonobo_PersistStream__epv *epv = &rsc->epv;

	bpc->get_content_types = get_content_types;

#if 0
	epv->isDirty 	= RectStream_is_dirty;
#endif
	epv->load 	= RectStream_load;
	epv->save 	= RectStream_save;
}

static void
gdraw_rect_stream_init (GObject *object)
{
	GDrawRectStream *rect_stream = GDRAW_RECT_STREAM (object);

	rect_stream->rect = NULL;
	rect_stream->is_dirty = FALSE;
}

BONOBO_TYPE_FUNC_FULL (GDrawRectStream, Bonobo_PersistStream, 
		       PARENT_TYPE, gdraw_rect_stream);

GDrawRectStream *
gdraw_rect_stream_new (GDrawRectShape *shape)
{
	GDrawRectStream *rect_stream;

	rect_stream = g_object_new(GDRAW_RECT_STREAM_TYPE, NULL);

	rect_stream->rect = shape;

	return rect_stream;
}
