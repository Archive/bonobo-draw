/*
 * re-shape.h: Implementation of the REShape class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_RE_SHAPE_H
#define __GDRAW_RE_SHAPE_H

#include <bonobo.h>
#include "shape.h"
#include "fill-properties.h"
#include "canvas-object.h"
#include "line-properties.h"
#include "shadow-properties.h"
#include "rotation-descriptor.h"

G_BEGIN_DECLS

typedef struct _GDrawREShape         GDrawREShape;
typedef struct _GDrawREShapeClass    GDrawREShapeClass;

#define GDRAW_RE_SHAPE_TYPE          (gdraw_re_shape_get_type ())
#define GDRAW_RE_SHAPE(obj)          (GTK_CHECK_CAST((obj), 		\
				      GDRAW_RE_SHAPE_TYPE,		\
				      GDrawREShape))
#define GDRAW_RE_SHAPE_CLASS(klass)  (GTK_CHECK_CLASS_CAST ((klass), 	\
                               	      GDRAW_RE_SHAPE_TYPE, 		\
				      GDrawREShapeClass))
#define IS_GDRAW_RE_SHAPE(obj)       (GTK_CHECK_TYPE ((obj),		\
				      GDRAW_RE_SHAPE_TYPE))
#define IS_GDRAW_RE_SHAPE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
					GDRAW_RE_SHAPE_TYPE))

struct _GDrawREShape {
	GDrawShape	 		parent_object;

	GDrawFillProperties		*fill;
	GDrawLineProperties		*line;
	GDrawShadowProperties		*shadow;
	GDrawRotationDescriptor		*rotation;
};
	
struct _GDrawREShapeClass {
	GDrawShapeClass parent_class;
};


GType     	 gdraw_re_shape_get_type (void);

void 		 gdraw_re_shape_add_props (BonoboPropertyBag *);
gboolean	 gdraw_re_shape_get_prop (GDrawREShape *, guint, BonoboArg *);
gboolean	 gdraw_re_shape_set_prop (GDrawREShape *, guint, BonoboArg *);


G_END_DECLS

#endif
