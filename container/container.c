#include "config.h"
#include <bonobo-activation/bonobo-activation.h>
#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-window.h>

#include <gdraw/oafiids.h>
#include <gdraw/shapes-layer.h>

#include "container.h"
#include "container-menu.h"

poptContext ctx;

void
sample_app_exit (SampleApp *app)
{
	gtk_object_destroy (GTK_OBJECT (app->layer));

	bonobo_object_unref (BONOBO_OBJECT (app->shapes));
	bonobo_object_unref (BONOBO_OBJECT (app->ui_container));

	gtk_widget_destroy (app->app);

	gtk_main_quit ();
}

static gint
delete_cb (GtkWidget *caller, GdkEvent *event, SampleApp *app)
{
	sample_app_exit (app);

	return TRUE;
}

static gint
sample_app_create_layer (gpointer data)
{
	SampleApp *app = (SampleApp *) data;
	CORBA_Environment ev;

	CORBA_exception_init(&ev);

	app->shapes = bonobo_activation_activate_from_id(GDRAW_SHAPES_OAFIID, 0, NULL, &ev);
	if (app->shapes == CORBA_OBJECT_NIL || BONOBO_EX(&ev)) {
		g_warning ("Couldn't create a shapes collection.");
		return FALSE;
	}

	app->layer = gdraw_shapes_layer_new (
			app->shapes, 
			GNOME_CANVAS (app->canvas), 
			BONOBO_OBJREF (app->ui_container));

	gdraw_shapes_layer_activate (app->layer);

	return FALSE;
}

static SampleApp *
sample_app_create (void)
{
	SampleApp *app = g_new0 (SampleApp, 1);

	app->app = bonobo_window_new ("gdraw-canvas-container",
				      "GDraw Canvas Container");

        app->canvas = gnome_canvas_new ();
        
	gnome_canvas_set_scroll_region (GNOME_CANVAS (app->canvas),
					-400.0, -300.0, 400, 300);

	gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (app->canvas)),
			       gnome_canvas_rect_get_type (),
			       "x1", -400.0, "y1", -300.0,
			       "x2", 400.0, "y2", 300.0,
			       "fill_color", "white", NULL);

	g_signal_connect(G_OBJECT (app->app), "delete_event",
			 G_CALLBACK (delete_cb), app);

	bonobo_window_set_contents (BONOBO_WINDOW (app->app), app->canvas);
	gtk_widget_set_usize (app->app, 400, 300);

	app->ui_container = bonobo_window_get_ui_container(
		BONOBO_WINDOW(app->app));

	sample_app_fill_menu (app);

	gtk_widget_show_all (app->app);

	gtk_idle_add (sample_app_create_layer, app);

	return app;
}

int
main (int argc, char **argv)
{
	SampleApp *app;
	CORBA_Environment ev;
	CORBA_ORB orb;

 	CORBA_exception_init (&ev);
	gnome_init_with_popt_table ("gdraw-canvas-container", VERSION,
				    argc, argv, bonobo_activation_popt_options, 0, &ctx);

	if (bonobo_init (&argc, argv) == FALSE)
		g_error ("Could not initialize Bonobo!\n");

	app = sample_app_create ();

	bonobo_main ();

	if (ctx)
		poptFreeContext (ctx);

	return 0;
}
