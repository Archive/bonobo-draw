/*
 * line-properties.c: Implementation of the LineProperties
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 */

#include <config.h>
#include <bonobo.h>
#include <gdraw/GNOME_Draw.h>
#include "line-properties.h"
#include "properties.h"

void
gdraw_line_properties_add_props (BonoboPropertyBag *bag)
{
	bonobo_property_bag_add (bag, "LineStyle", PROP_LINE_STYLE,
				 BONOBO_ARG_INT, NULL,
				 "Style of line", 0);

	bonobo_property_bag_add (bag, "LineColor", PROP_LINE_COLOR,
				 BONOBO_ARG_LONG, NULL,
				 "RGB color of the line", 0);

	bonobo_property_bag_add (bag, "LineTransparency", PROP_LINE_TRANSPARENCY,
				 BONOBO_ARG_INT, NULL,
				 "Alpha transparency of the line", 0);

	bonobo_property_bag_add (bag, "LineRGBA", PROP_LINE_RGBA,
				 BONOBO_ARG_LONG, NULL,
				 "RGBA color of the line", 0);

	bonobo_property_bag_add (bag, "LineWidth", PROP_LINE_WIDTH,
				 BONOBO_ARG_LONG, NULL,
				 "Width of the line", 0);
}

gboolean
gdraw_line_properties_get_prop (GDrawLineProperties *line, 
				guint arg_id, BonoboArg *arg)
{
	switch (arg_id) {

	case PROP_LINE_STYLE:
		BONOBO_ARG_SET_INT (arg, (gint) line->style);
		break;
	case PROP_LINE_COLOR:
		BONOBO_ARG_SET_LONG (arg, line->rgba >> 8);
		break;
	case PROP_LINE_TRANSPARENCY:
		BONOBO_ARG_SET_INT (arg, line->rgba & 0xf);
		break;
	case PROP_LINE_RGBA:
		BONOBO_ARG_SET_LONG (arg, line->rgba);
		break;
	case PROP_LINE_WIDTH:
		BONOBO_ARG_SET_LONG (arg, line->width);
		break;
	default:
		return FALSE;
	}

	return TRUE;
}
gboolean
gdraw_line_properties_set_prop (GDrawLineProperties *line, 
				guint arg_id, BonoboArg *arg)
{
	switch (arg_id) {

	case PROP_LINE_STYLE:
		switch ((GNOME_Draw_LineStyle) BONOBO_ARG_GET_INT (arg)) {
		case GNOME_Draw_LINENONE:
			line->style = GNOME_Draw_LINENONE;
			break;
		case GNOME_Draw_LINESOLID:
			line->style = GNOME_Draw_LINESOLID;
			break;
		case GNOME_Draw_LINEDASHED:
		default:
			/* Unsupported LineStyles */
			g_warning ("Unsupported line style");
		}
		break;
	case PROP_LINE_COLOR:
		line->rgba = (BONOBO_ARG_GET_LONG (arg) << 8) + 
			    (line->rgba & 0xf);
		break;
	case PROP_LINE_TRANSPARENCY:
		line->rgba = (BONOBO_ARG_GET_INT (arg) & 0xf) +
			    (line->rgba & 0xfff0);
		break;
	case PROP_LINE_RGBA:
		line->rgba = BONOBO_ARG_GET_LONG (arg);
		break;
	case PROP_LINE_WIDTH:
		line->width = BONOBO_ARG_GET_LONG (arg);
		break;
	default:
		return FALSE;
	}

	return TRUE;
}

gboolean
gdraw_line_properties_load_from_xml (GDrawLineProperties *line, 
				     xmlNode *node)
{
	gchar *prop;

	if (g_strcasecmp (node->name, "LineProperties"))
		return FALSE;

	prop = xmlGetProp (node, "LineStyle");
	if (!prop)
		return FALSE;

	line->style = strtol (prop, NULL, 10);
	g_free (prop);

	if (!line->style)
		return TRUE;

	prop = xmlGetProp (node, "RGBA");
	if (prop)
		line->rgba = strtoul (prop, NULL, 16);
	else {
		line->rgba = 0;
		free (prop);
	}

	prop = xmlGetProp (node, "Width");
	if (prop)
		line->width = strtol (prop, NULL, 10);
	else {
		line->width = 0;
		free (prop);
	}

	return TRUE;
}

xmlNode *
gdraw_line_properties_save_to_xml (GDrawLineProperties *line, 
				   xmlNode *node)
{
	xmlNode *child;
	gchar *prop;

	child = xmlNewChild (node, NULL, "LineProperties", NULL);

	prop = g_strdup_printf ("%x", line->style);
	if (!prop) {
		xmlFreeNode (child);
		return NULL;
	}
	xmlSetProp (child, "LineStyle", prop);
	g_free (prop);

	if (!line->style)
		return child;

	prop = g_strdup_printf ("%x", line->rgba);
	if (!prop) {
		xmlFreeNode (child);
		return NULL;
	}
	xmlSetProp (child, "RGBA", prop);
	g_free (prop);

	prop = g_strdup_printf ("%d", line->width);
	if (!prop) {
		xmlFreeNode (child);
		return NULL;
	}
	xmlSetProp (child, "Width", prop);
	g_free (prop);

	return child;
}

