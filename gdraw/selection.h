/*
 * selection.h: Implementation of the Selection class
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_SELECTION_H
#define __GDRAW_SELECTION_H

#include <libgnomecanvas/gnome-canvas.h>
#include <gdraw/GNOME_Draw.h>

G_BEGIN_DECLS

typedef struct _GDrawSelection         GDrawSelection;
typedef struct _GDrawSelectionClass    GDrawSelectionClass;

#define GDRAW_SELECTION_TYPE            (gdraw_selection_get_type())
#define GDRAW_SELECTION(obj)            (GTK_CHECK_CAST((obj),	\
					 GDRAW_SELECTION_TYPE, 	\
					 GDrawSelection))
#define GDRAW_SELECTION_CLASS(klass)    (GTK_CHECK_CLASS_CAST((klass), \
                               	     	 GDRAW_SELECTION_TYPE, 	    \
					 GDrawSelectionClass))
#define GDRAW_IS_SELECTION(obj)         (GTK_CHECK_TYPE((obj), 	\
					 GDRAW_SELECTION_TYPE))
#define GDRAW_IS_SELECTION_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), \
					 GDRAW_SELECTION_TYPE))

struct _GDrawSelection {
	GtkObject 		parent_object;

	GNOME_Draw_Shape	shape;
	GnomeCanvasItem 	*object_group;
	GnomeCanvasItem 	*handles[8];

	GNOME_Draw_Point	pt;
	GNOME_Draw_Size		sz;

	gboolean		multi;
};
	
struct _GDrawSelectionClass {
	GtkObjectClass parent_class;
};

/* Standard GtkObject function */
GtkType gdraw_selection_get_type (void);

GDrawSelection *gdraw_selection_new (GnomeCanvasGroup *, GList *shape_list);

void 		gdraw_selection_hide_handles (GDrawSelection *);
void 		gdraw_selection_show_handles (GDrawSelection *);
void 		gdraw_selection_update_handles (GDrawSelection *);

void		gdraw_selection_moveto (GDrawSelection *, 
					gdouble x, 
					gdouble y);

gboolean	gdraw_selection_contains_shape (GDrawSelection *,
						GNOME_Draw_Shape);

G_END_DECLS

#endif
