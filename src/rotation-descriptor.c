/*
 * rotation-descriptor.c: Implementation of the RotationDescriptor props
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include "properties.h"
#include "rotation-descriptor.h"

void
gdraw_rotation_descriptor_add_props (BonoboPropertyBag *bag)
{
	bonobo_property_bag_add (bag, "RotateAngle", PROP_ROTATE_ANGLE,
				 BONOBO_ARG_LONG, NULL,
				 "Angle of rotation", 0);

	bonobo_property_bag_add (bag, "RotationPointX", PROP_ROTATE_POINT_X,
				 BONOBO_ARG_LONG, NULL,
				 "X coordinate of rotation point", 0);

	bonobo_property_bag_add (bag, "RotationPointY", PROP_ROTATE_POINT_Y,
				 BONOBO_ARG_LONG, NULL,
				 "Y coordinate of rotation point", 0);

}

gboolean
gdraw_rotation_descriptor_get_prop (GDrawRotationDescriptor *rd, 
				    guint arg_id, BonoboArg *arg)
{
	switch (arg_id) {

	case PROP_ROTATE_ANGLE:
		BONOBO_ARG_SET_LONG (arg, rd->angle);
		break;
	case PROP_ROTATE_POINT_X:
		BONOBO_ARG_SET_LONG (arg, rd->x);
		break;
	case PROP_ROTATE_POINT_Y:
		BONOBO_ARG_SET_LONG (arg, rd->y);
		break;
	default:
		return FALSE;
	}

	return TRUE;
}

gboolean
gdraw_rotation_descriptor_set_prop (GDrawRotationDescriptor *rd, 
				    guint arg_id, BonoboArg *arg)
{
	switch (arg_id) {

	case PROP_ROTATE_ANGLE:
		rd->angle = BONOBO_ARG_GET_LONG (arg);
		break;
	case PROP_ROTATE_POINT_X:
		rd->x = BONOBO_ARG_GET_LONG (arg);
		break;
	case PROP_ROTATE_POINT_Y:
		rd->y = BONOBO_ARG_GET_LONG (arg);
		break;
	default:
		return FALSE;
	}

	return TRUE;
}

gboolean
gdraw_rotation_descriptor_load_from_xml (GDrawRotationDescriptor *rd, 
				         xmlNode *node)
{
	gchar *prop;

	if (g_strcasecmp (node->name, "RotationDesc"))
		return FALSE;

	prop = xmlGetProp (node, "Angle");
	if (!prop)
		return FALSE;

	rd->angle = strtol (prop, NULL, 10);
	g_free (prop);

	if (!rd->angle)
		return TRUE;

	prop = xmlGetProp (node, "X");
	if (prop)
		rd->x = strtol (prop, NULL, 10);
	else {
		rd->x = 0;
		free (prop);
	}

	prop = xmlGetProp (node, "Y");
	if (prop)
		rd->y = strtol (prop, NULL, 10);
	else {
		rd->y = 0;
		free (prop);
	}

	return TRUE;
}

xmlNode *
gdraw_rotation_descriptor_save_to_xml (GDrawRotationDescriptor *rd, 
				       xmlNode *node)
{
	xmlNode *child;
	gchar *prop;

	child = xmlNewChild (node, NULL, "RotationDesc", NULL);

	if (!child)
		return NULL;

	prop = g_strdup_printf ("%d", rd->angle);
	if (!prop) {
		xmlFreeNode (child);
		return NULL;
	}
	xmlSetProp (child, "Angle", prop);
	g_free (prop);

	if (!rd->angle)
		return child;

	prop = g_strdup_printf ("%d", rd->x);
	if (!prop) {
		xmlFreeNode (child);
		return NULL;
	}
	xmlSetProp (child, "X", prop);
	g_free (prop);

	prop = g_strdup_printf ("%d", rd->y);
	if (!prop) {
		xmlFreeNode (child);
		return NULL;
	}
	xmlSetProp (child, "Y", prop);
	g_free (prop);

	return child;
}

