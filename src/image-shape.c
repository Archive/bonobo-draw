/*
 * image-shape.c: Implementation of the ImageShape class
 *
 * Copyright (c) 2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <gdraw/oafiids.h>
#include <libgnomecanvas/gnome-canvas.h>
#include "image-canvas-object.h"
#include "image-shape.h"
#include "image-shape-io.h"
#include "properties.h"

static void
gdraw_image_shape_destroy (GtkObject *object)
{
	GDrawImageShape *image = GDRAW_IMAGE_SHAPE (object);

	g_free (image->url);
	g_free (image->shadow);
	g_free (image->rotation);

	if (image->pixbuf)
		gdk_pixbuf_unref (image->pixbuf);

	bonobo_object_unref (BONOBO_OBJECT (image->bag));
	bonobo_object_unref (BONOBO_OBJECT (image->embeddable));
	bonobo_object_unref (BONOBO_OBJECT (image->stream));
}

static void
gdraw_image_shape_class_init (GtkObjectClass *klass)
{
	klass->destroy = gdraw_image_shape_destroy;
}

static void
gdraw_image_shape_init (GtkObject *object)
{
	GDrawImageShape *image;

	g_return_if_fail (object);

	image = GDRAW_IMAGE_SHAPE (object);

	image->bag = NULL;
	image->embeddable = NULL;
	image->stream = NULL;

	image->url = NULL;
	image->pixbuf = NULL;
	image->shadow = g_new0 (GDrawShadowProperties, 1);
	image->rotation = g_new0 (GDrawRotationDescriptor, 1);
}

GtkType
gdraw_image_shape_get_type (void)
{
	static GtkType image_type = 0;

	if (!image_type) {
		static const GtkTypeInfo image_info = {
			"GDrawImageShape",
			sizeof (GDrawImageShape),
			sizeof (GDrawImageShapeClass),
			(GtkClassInitFunc) gdraw_image_shape_class_init,
			(GtkObjectInitFunc) gdraw_image_shape_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		image_type = gtk_type_unique (gdraw_shape_get_type(), 
					      &image_info);
	}

	return image_type;
}

static void
get_prop (BonoboPropertyBag *bag, BonoboArg *arg, 
	  guint arg_id, CORBA_Environment *ev, GDrawImageShape *image)
{
	GDrawShape *shape = GDRAW_SHAPE (image);

	if (gdraw_shape_descriptor_get_prop (shape->desc, arg_id, arg) ||
	    gdraw_shadow_properties_set_prop (image->shadow, arg_id, arg) ||
	    gdraw_rotation_descriptor_set_prop (image->rotation, arg_id, arg))
		return;

	switch (arg_id) {
	case PROP_GRAPHIC_URL:
		if (image->url) 
			BONOBO_ARG_SET_STRING (arg, image->url);
		break;
	default:
		g_warning ("Unhandled property %d", arg_id);
	}
}

static void
set_prop (BonoboPropertyBag *bag, BonoboArg *arg,
	  guint arg_id, CORBA_Environment *ev, GDrawImageShape *image)
{
	GDrawShape *shape = GDRAW_SHAPE (image);

	if (gdraw_shape_descriptor_set_prop (shape->desc, arg_id, arg) ||
	    gdraw_shadow_properties_set_prop (image->shadow, arg_id, arg) ||
	    gdraw_rotation_descriptor_set_prop (image->rotation, arg_id, arg)) {
		gtk_signal_emit_by_name (GTK_OBJECT (image), "changed");
		return;
	}

	switch (arg_id) {
	case PROP_GRAPHIC_URL:
		if (image->url)
			g_free (image->url);

		image->url = BONOBO_ARG_GET_STRING (arg);

		if (image->pixbuf)
			gdk_pixbuf_unref (image->pixbuf);

		image->pixbuf = gdk_pixbuf_new_from_file (image->url, NULL);
		if (!image->pixbuf) {
			g_warning ("Image %s could not be loaded.", image->url);
			g_free (image->url);
			image->url = NULL;
			return;
		}

		gtk_signal_emit_by_name (GTK_OBJECT (image), "changed");
		break;
	default:
		g_warning ("Unhandled property %d", arg_id);
	}
}

static BonoboPropertyBag *
create_property_bag (GDrawImageShape *image)
{
	BonoboPropertyBag *bag = bonobo_property_bag_new (
					(BonoboPropertyGetFn) get_prop, 
					(BonoboPropertySetFn) set_prop,
					image);

	gdraw_shape_descriptor_add_props (bag);
	gdraw_shadow_properties_add_props (bag);
	gdraw_rotation_descriptor_add_props (bag);

        bonobo_property_bag_add (bag, "GraphicURL", PROP_GRAPHIC_URL,
                                 BONOBO_ARG_STRING, NULL,
                                 "URL to Graphic Object", 0);

	bonobo_object_add_interface (BONOBO_OBJECT (image), 
				     BONOBO_OBJECT (bag));

	return bag;
}

static GDrawCanvasObject *
create_canvas_object (GDrawImageShape *image, GnomeCanvas *canvas)
{
	g_return_val_if_fail (image != NULL, NULL);
	g_return_val_if_fail (canvas != NULL, NULL);

	return GDRAW_CANVAS_OBJECT (gdraw_image_canvas_object_new (image, canvas));
}

static BonoboCanvasComponent *
image_item_creator (BonoboObject *embeddable, GnomeCanvas *canvas,
		   gpointer data)
{
	BonoboCanvasComponent *component;
	GDrawCanvasObject *cobj;
	GDrawImageShape *shape = GDRAW_IMAGE_SHAPE (data);

	cobj = create_canvas_object (shape, canvas);

	component = bonobo_canvas_component_new (GNOME_CANVAS_ITEM (
							cobj->object_group));

	if (component == NULL) {
		gtk_object_destroy (GTK_OBJECT (cobj));
		return NULL;
	}

	gdraw_canvas_object_set_component (cobj, component);

	return component;
}

GDrawImageShape *
gdraw_image_shape_new ()
{
	GDrawShape *shape;
	GDrawImageShape *image;
	BonoboObject *embeddable;
	GDrawImageStream *stream;
	static gint count = 0;

	image = gtk_type_new (GDRAW_IMAGE_SHAPE_TYPE);
	shape = GDRAW_SHAPE (image);

	shape->desc->name = g_strdup_printf ("image%d", count++);
	shape->type = g_strdup (GDRAW_IMAGE_OAFIID);

        image->bag = create_property_bag (image);

	//FIXME
	//embeddable = bonobo_embeddable_new_canvas_item (image_item_creator,
	//					        image);
	embeddable = NULL;

	bonobo_object_add_interface (BONOBO_OBJECT (image), 
				     BONOBO_OBJECT (embeddable));

	image->embeddable = embeddable;

	stream = gdraw_image_stream_new (image);

	if (!stream) {
		bonobo_object_unref (BONOBO_OBJECT (image));
		return NULL;
	}

	bonobo_object_add_interface (BONOBO_OBJECT (image),
				     BONOBO_OBJECT (stream));

	image->stream = stream;

	return image;
}

BonoboObject *
gdraw_image_shape_factory (BonoboGenericFactory *this)
{
	GDrawImageShape *image;

	image = gdraw_image_shape_new ();

	return BONOBO_OBJECT (image);
}


