/*
 * ellipse-shape.h: Implementation of the EllipseShape class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_ELLIPSE_SHAPE_H
#define __GDRAW_ELLIPSE_SHAPE_H

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-generic-factory.h>
#include <bonobo/bonobo-property-bag.h>
#include "re-shape.h"

G_BEGIN_DECLS

typedef struct _GDrawEllipseShape         GDrawEllipseShape;
typedef struct _GDrawEllipseShapeClass    GDrawEllipseShapeClass;

#include "ellipse-shape-io.h"

#define GDRAW_ELLIPSE_SHAPE_TYPE          (gdraw_ellipse_shape_get_type ())
#define GDRAW_ELLIPSE_SHAPE(obj)          (GTK_CHECK_CAST((obj),     \
					   GDRAW_ELLIPSE_SHAPE_TYPE, \
					   GDrawEllipseShape))
#define GDRAW_ELLIPSE_SHAPE_CLASS(klass)  (GTK_CHECK_CLASS_CAST ((klass), \
                               	           GDRAW_ELLIPSE_SHAPE_TYPE, 	  \
					   GDrawEllipseShapeClass))
#define IS_GDRAW_ELLIPSE_SHAPE(obj)       (GTK_CHECK_TYPE ((obj),	\
					   GDRAW_ELLIPSE_SHAPE_TYPE))
#define IS_GDRAW_ELLIPSE_SHAPE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
					     GDRAW_ELLIPSE_SHAPE_TYPE))

struct _GDrawEllipseShape {
	GDrawREShape	 	 parent_object;

	BonoboObject	 	*embeddable;
	BonoboPropertyBag 	*bag;
	GDrawEllipseStream	*stream;
};
	
struct _GDrawEllipseShapeClass {
	GDrawREShapeClass parent_class;
};


GType 	           gdraw_ellipse_shape_get_type (void);

GDrawEllipseShape *gdraw_ellipse_shape_new (void);

BonoboObject	  *gdraw_ellipse_shape_factory (BonoboGenericFactory *);

G_END_DECLS

#endif
