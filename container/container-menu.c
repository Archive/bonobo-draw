#include "config.h"
#include <libgnomeui/gnome-about.h>
#include <bonobo/bonobo-ui-component.h>
#include <bonobo/bonobo-window.h>
#include <gtk/gtkfilesel.h>
#include <gtk/gtkstock.h>
#include "container-filesel.h"
#include "container-io.h"
#include "container-menu.h"

static void
load_ok_cb (GtkWidget *caller, SampleApp *app)
{
	GtkWidget *fs = app->fileselection;
	gchar *filename = g_strdup (gtk_file_selection_get_filename
		(GTK_FILE_SELECTION (fs)));

	if (filename)
		sample_container_load (app, filename);

	gtk_widget_destroy (fs);
	g_free (filename);
}

static void
save_ok_cb (GtkWidget *caller, SampleApp *app)
{
	GtkWidget *fs = app->fileselection;
	gchar *filename = g_strdup (gtk_file_selection_get_filename
	    (GTK_FILE_SELECTION (fs)));

	if (filename)
		sample_container_save (app, filename);

	gtk_widget_destroy (fs);
	g_free (filename);
}

static void
verb_FileSaveAs_cb (BonoboUIComponent *uic, gpointer user_data, const char *cname)
{
	SampleApp *app = user_data;

	container_request_file (app, TRUE, G_CALLBACK(save_ok_cb), app);
}

static void
verb_FileLoad_cb (BonoboUIComponent *uic, gpointer user_data, const char *cname)
{
	SampleApp *app = user_data;

	container_request_file (app, FALSE, G_CALLBACK(load_ok_cb), app);
}

static void
verb_XmlDump_cb (BonoboUIComponent *uic, gpointer user_data, const char *cname)
{
	SampleApp *app = user_data;

#if 0
	bonobo_window_dump (BONOBO_WINDOW (app->app), "On request");
#endif
}

static void
verb_HelpAbout_cb (BonoboUIComponent *uic, gpointer user_data, const char *cname)
{
	static const gchar *authors[] = {
		"Mike Kestner <mkestner@ameritech.net>",
		"�RDI Gerg� <cactus@cactus.rulez.org>",
		"Michael Meeks <michael@helixcode.com>",
		NULL
	};

	GtkWidget *about = gnome_about_new (
			"gdraw-canvas-container",
			VERSION,
			"(C) 2000 Mike Kestner, �RDI Gerg�, Helix Code, Inc",
			"GDraw Canvas Container",		// comments
			authors,
			NULL,					// documenters
			NULL,					// translator_credits
			NULL);					// Logopixbuf
	gtk_widget_show (about);
}

static void
verb_FileExit_cb (BonoboUIComponent *uic, gpointer user_data, const char *cname)
{
	SampleApp *app = user_data;

	sample_app_exit (app);
}

/*
 * The menus.
 */
static char ui_commands [] =
#if 1
"<commands>\n"
"	<cmd name=\"FileOpen\" _label=\"_Open\"\n"
"		pixtype=\"stock\" pixname=\""GTK_STOCK_OPEN"\" _tip=\"Open a file\"/>\n"
"	<cmd name=\"FileSaveAs\" _label=\"Save _As...\"\n"
"		pixtype=\"stock\" pixname=\""GTK_STOCK_SAVE"\"\n"
"		_tip=\"Save the current file with a different name\"/>\n"
"	<cmd name=\"XmlDump\" _label=\"Xml dump\"/>\n"
"	<cmd name=\"FileExit\" _label=\"E_xit\" _tip=\"Exit the program\"\n"
"		pixtype=\"stock\" pixname=\""GTK_STOCK_QUIT"\" accel=\"*Control*q\"/>\n"
"	<cmd name=\"HelpAbout\" _label=\"_About...\" _tip=\"About this application\"\n"
"		pixtype=\"stock\" pixname=\"About\"/>\n"
"</commands>";
#else
"<commands>\n"
"	<cmd name=\"FileOpen\"/>\n"
"	<cmd name=\"FileSaveAs\"/>\n"
"	<cmd name=\"XmlDump\"/>\n"
"	<cmd name=\"FileExit\"/>\n"
"	<cmd name=\"HelpAbout\"/>\n"
"</commands>";
#endif

static char ui_data [] =
"<menu>\n"
"	<submenu name=\"File\" _label=\"_File\">\n"
"		<menuitem name=\"FileOpen\" verb=\"\"/>\n"
"		<menuitem name=\"FileSaveAs\" verb=\"\"/>\n"
"		<placeholder name=\"Placeholder\"/>\n"
"		<menuitem name=\"XmlDump\" verb=\"\"/>\n"
"		<separator/>\n"
"		<menuitem name=\"FileExit\" verb=\"\"/>\n"
"	</submenu>\n"
"	<submenu name=\"Help\" _label=\"_Help\">\n"
"		<menuitem name=\"HelpAbout\" verb=\"\"/>\n"
"	</submenu>\n"
"</menu>";

static BonoboUIVerb sample_app_verbs[] = {
	BONOBO_UI_VERB ("FileOpen", verb_FileLoad_cb),
	BONOBO_UI_VERB ("FileSaveAs", verb_FileSaveAs_cb),
	BONOBO_UI_VERB ("XmlDump", verb_XmlDump_cb),
	BONOBO_UI_VERB ("FileExit", verb_FileExit_cb),
	BONOBO_UI_VERB ("HelpAbout", verb_HelpAbout_cb),
	BONOBO_UI_VERB_END
};

void
sample_app_fill_menu (SampleApp *app)
{
	Bonobo_UIContainer corba_container;
	BonoboUIComponent *uic;

	uic = bonobo_ui_component_new ("sample");
	corba_container = bonobo_object_corba_objref (BONOBO_OBJECT (app->ui_container));
	bonobo_ui_component_set_container (uic, corba_container, NULL);

	bonobo_ui_component_set_translate (uic, "/", ui_commands, NULL);
	bonobo_ui_component_set_translate (uic, "/", ui_data, NULL);

	bonobo_ui_component_add_verb_list_with_data (uic, sample_app_verbs, app);
}
