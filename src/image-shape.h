/*
 * image-shape.h: Implementation of the ImageShape class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_IMAGE_SHAPE_H
#define __GDRAW_IMAGE_SHAPE_H

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-generic-factory.h>
#include <bonobo/bonobo-property-bag.h>
#include "rotation-descriptor.h"
#include "shadow-properties.h"
#include "shape.h"

G_BEGIN_DECLS

typedef struct _GDrawImageShape         GDrawImageShape;
typedef struct _GDrawImageShapeClass    GDrawImageShapeClass;

#include "image-shape-io.h"

#define GDRAW_IMAGE_SHAPE_TYPE          (gdraw_image_shape_get_type ())
#define GDRAW_IMAGE_SHAPE(obj)          (GTK_CHECK_CAST((obj), 		\
					GDRAW_IMAGE_SHAPE_TYPE,		\
					GDrawImageShape))
#define GDRAW_IMAGE_SHAPE_CLASS(klass)  (GTK_CHECK_CLASS_CAST ((klass), \
                               	        GDRAW_IMAGE_SHAPE_TYPE, 	\
					GDrawImageShapeClass))
#define IS_GDRAW_IMAGE_SHAPE(obj)       (GTK_CHECK_TYPE ((obj),		\
					GDRAW_IMAGE_SHAPE_TYPE))
#define IS_GDRAW_IMAGE_SHAPE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
					  GDRAW_IMAGE_SHAPE_TYPE))

struct _GDrawImageShape {
	GDrawShape	 	 parent_object;

	BonoboObject	 	*embeddable;
	BonoboPropertyBag 	*bag;
	GDrawImageStream	*stream;

	GDrawShadowProperties 	*shadow;
	GDrawRotationDescriptor	*rotation;

	gchar 			*url;
	GdkPixbuf		*pixbuf;
};
	
struct _GDrawImageShapeClass {
	GDrawShapeClass parent_class;
};


GtkType 	   gdraw_image_shape_get_type (void);

GDrawImageShape    *gdraw_image_shape_new (void);

BonoboObject	  *gdraw_image_shape_factory (BonoboGenericFactory *);

G_END_DECLS

#endif
