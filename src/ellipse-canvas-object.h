/*
 * ellipse-canvas-object.h: Implementation of the EllipseCanvasObject class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_ELLIPSE_CANVAS_OBJECT_H
#define __GDRAW_ELLIPSE_CANVAS_OBJECT_H

#include <gtk/gtktypeutils.h>
#include <gdraw/GNOME_Draw.h>

G_BEGIN_DECLS

typedef struct _GDrawEllipseCanvasObject         GDrawEllipseCanvasObject;
typedef struct _GDrawEllipseCanvasObjectClass    GDrawEllipseCanvasObjectClass;

#include "canvas-object.h"
#include "ellipse-shape.h"

#define GDRAW_ELLIPSE_CANVAS_OBJECT_TYPE  (gdraw_ellipse_canvas_object_get_type())
#define GDRAW_ELLIPSE_CANVAS_OBJECT(obj)  (GTK_CHECK_CAST((obj),	     \
					   GDRAW_ELLIPSE_CANVAS_OBJECT_TYPE, \
					   GDrawEllipseCanvasObject))
#define GDRAW_ELLIPSE_CANVAS_OBJECT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),     \
                               	     	          GDRAW_ELLIPSE_CANVAS_OBJECT_TYPE, \
					          GDrawEllipseCanvasObjectClass))
#define IS_GDRAW_ELLIPSE_CANVAS_OBJECT(obj)       (GTK_CHECK_TYPE((obj), 	\
					           GDRAW_ELLIPSE_CANVAS_OBJECT_TYPE))
#define IS_GDRAW_ELLIPSE_CANVAS_OBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), \
					             GDRAW_ELLIPSE_CANVAS_OBJECT_TYPE))

struct _GDrawEllipseCanvasObject {
	GDrawCanvasObject 	parent_object;

	GnomeCanvasItem 	*canvas_item;
	GnomeCanvasItem 	*shadow_item;
};
	
struct _GDrawEllipseCanvasObjectClass {
	GDrawCanvasObjectClass parent_class;
};

GtkType gdraw_ellipse_canvas_object_get_type (void);

GDrawCanvasObject *gdraw_ellipse_canvas_object_new (GDrawEllipseShape *,
						    GnomeCanvas *);

G_END_DECLS

#endif

