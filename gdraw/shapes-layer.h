/*
 * shapes-layer.h: Implementation of the ShapesLayer canvas item
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Authors: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_SHAPES_LAYER_H
#define __GDRAW_SHAPES_LAYER_H

#include <libgnomecanvas/gnome-canvas.h>
#include <bonobo/Bonobo.h>
#include <gdraw/GNOME_Draw.h>

G_BEGIN_DECLS

typedef struct _GDrawShapesLayer     	GDrawShapesLayer;
typedef struct _GDrawShapesLayerClass 	GDrawShapesLayerClass;
typedef struct _GDrawShapesLayerPrivate GDrawShapesLayerPrivate;

#define GDRAW_SHAPES_LAYER_TYPE            (gdraw_shapes_layer_get_type ())
#define GDRAW_SHAPES_LAYER(obj)            (GTK_CHECK_CAST((obj), 	\
					    GDRAW_SHAPES_LAYER_TYPE, 	\
					    GDrawShapesLayer))
#define GDRAW_SHAPES_LAYER_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), \
					    GDRAW_SHAPES_LAYER_TYPE, 	   \
					    GDrawShapesLayerClass))
#define GDRAW_IS_SHAPES_LAYER(obj)         (GTK_CHECK_TYPE ((obj), 	\
					    GDRAW_SHAPES_LAYER_TYPE))
#define GDRAW_IS_SHAPES_LAYER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), \
					    GDRAW_SHAPES_LAYER_TYPE))

typedef enum {
	GD_LAYER_MODE_NORMAL,        	/* Stable non-editing mode 	   */
	GD_LAYER_MODE_CREATE_POINTS,	/* Point object creation   	   */
	GD_LAYER_MODE_CREATE_OVAL,	/* Oval object creation    	   */
	GD_LAYER_MODE_CREATE_RECT,	/* Rect object creation		   */
	GD_LAYER_MODE_FINALIZE_CREATE	/* Finalization of object creation */
} GDrawShapesLayerMode;

struct _GDrawShapesLayer {
	GtkObject 	parent_item;

	GDrawShapesLayerPrivate *priv;
};
	
struct _GDrawShapesLayerClass {
	GtkObjectClass parent_class;
};


GtkType 	gdraw_shapes_layer_get_type (void);

GDrawShapesLayer *gdraw_shapes_layer_new (GNOME_Draw_Shapes, GnomeCanvas *, 
					  Bonobo_UIContainer);

void		gdraw_shapes_layer_activate (GDrawShapesLayer *);
void		gdraw_shapes_layer_deactivate (GDrawShapesLayer *);

void		gdraw_shapes_layer_set_mode (GDrawShapesLayer *,
					     GDrawShapesLayerMode);

void		gdraw_shapes_layer_set_oaf_iid (GDrawShapesLayer *,
					        const gchar *oaf_iid);

void		gdraw_shapes_layer_get_center (GDrawShapesLayer *, 
					       gdouble *x, gdouble *y);

gboolean	gdraw_shapes_layer_add_shape (GDrawShapesLayer *, 
					      GNOME_Draw_Shape);

G_END_DECLS

#endif
