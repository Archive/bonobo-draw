/*
 * fill-properties.h: Implementation of the FillProperties
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_FILL_PROPERTIES_H
#define __GDRAW_FILL_PROPERTIES_H

#include <bonobo.h>
#include <libxml/tree.h>
#include <gdraw/GNOME_Draw.h>

G_BEGIN_DECLS

typedef struct {
	GNOME_Draw_FillStyle	 fill_style;
	guint32		 	 fill_rgba;
} GDrawFillProperties;

/* FillProperties manipulation API */
void	   gdraw_fill_properties_add_props (BonoboPropertyBag *bag);

gboolean   gdraw_fill_properties_get_prop (GDrawFillProperties *,
					   guint arg_id, BonoboArg *arg);

gboolean   gdraw_fill_properties_set_prop (GDrawFillProperties *,
					   guint arg_id, BonoboArg *arg);

gboolean   gdraw_fill_properties_load_from_xml (GDrawFillProperties *,
						xmlNode *);

xmlNode   *gdraw_fill_properties_save_to_xml (GDrawFillProperties *, 
					      xmlNode *);

G_END_DECLS

#endif
