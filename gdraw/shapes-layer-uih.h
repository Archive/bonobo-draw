/*
 * shapes-layer-uih.h: Implementation of the ShapesLayer UI elements
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Authors: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_SHAPES_LAYER_UIH_H
#define __GDRAW_SHAPES_LAYER_UIH_H

#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-ui-component.h>
#include "shapes-layer.h"

G_BEGIN_DECLS

void gdraw_shapes_layer_ui_component_clear_mode (BonoboUIComponent *,
						 GDrawShapesLayerMode);

BonoboUIComponent *gdraw_shapes_layer_ui_component_new (GDrawShapesLayer *,
							Bonobo_UIContainer);

G_END_DECLS

#endif
