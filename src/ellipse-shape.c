/*
 * ellipse-shape.c: Implementation of the EllipseShape class
 *
 * Copyright (c) 2000 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#include <config.h>
#include <gdraw/oafiids.h>
#include <libgnomecanvas/gnome-canvas.h>
#include "ellipse-canvas-object.h"
#include "ellipse-shape.h"
#include "ellipse-shape-io.h"

static void
gdraw_ellipse_shape_destroy (GtkObject *object)
{
	GDrawEllipseShape *shape = GDRAW_ELLIPSE_SHAPE (object);

	bonobo_object_unref (BONOBO_OBJECT (shape->bag));
	bonobo_object_unref (BONOBO_OBJECT (shape->embeddable));
	bonobo_object_unref (BONOBO_OBJECT (shape->stream));
}

static void
gdraw_ellipse_shape_class_init (GtkObjectClass *klass)
{
	klass->destroy = gdraw_ellipse_shape_destroy;
}

static void
gdraw_ellipse_shape_init (GtkObject *object)
{
	GDrawEllipseShape *ellipse;

	g_return_if_fail (object);

	ellipse = GDRAW_ELLIPSE_SHAPE (object);

	ellipse->bag = NULL;
	ellipse->embeddable = NULL;
	ellipse->stream = NULL;
}

GType
gdraw_ellipse_shape_get_type(void)
{
	static GType ellipse_type = 0;

	if (!ellipse_type) {
		static const GTypeInfo ellipse_info = {
			sizeof(GDrawEllipseShapeClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gdraw_ellipse_shape_class_init,
			NULL, NULL,
			sizeof(GDrawEllipseShape),
			0,
			(GInstanceInitFunc) gdraw_ellipse_shape_init
		};

		ellipse_type = g_type_register_static(
			gdraw_re_shape_get_type(), "GDrawEllipseShape", 
			&ellipse_info, 0);
	}

	return ellipse_type;
}

static void
get_prop (BonoboPropertyBag *bag, BonoboArg *arg, 
	  guint arg_id, CORBA_Environment *ev, GDrawEllipseShape *ellipse)
{
	if (!gdraw_re_shape_get_prop (GDRAW_RE_SHAPE (ellipse), arg_id, arg))
		g_warning ("Unhandled property %d", arg_id);
}

static void
set_prop (BonoboPropertyBag *bag, BonoboArg *arg,
	  guint arg_id, CORBA_Environment *ev, GDrawEllipseShape *ellipse)
{
	if (gdraw_re_shape_set_prop (GDRAW_RE_SHAPE (ellipse), arg_id, arg))
		g_signal_emit_by_name (G_OBJECT (ellipse), "changed");
	else
		g_warning ("Unhandled property %d", arg_id);
}

static BonoboPropertyBag *
create_property_bag (GDrawEllipseShape *ellipse)
{
	BonoboPropertyBag *bag = bonobo_property_bag_new (
					(BonoboPropertyGetFn) get_prop, 
					(BonoboPropertySetFn) set_prop,
					ellipse);

	gdraw_re_shape_add_props (bag);

	bonobo_object_add_interface (BONOBO_OBJECT (ellipse), 
				     BONOBO_OBJECT (bag));

	return bag;
}

static GDrawCanvasObject *
create_canvas_object (GDrawEllipseShape *ellipse, GnomeCanvas *canvas)
{
	g_return_val_if_fail (ellipse != NULL, NULL);
	g_return_val_if_fail (canvas != NULL, NULL);

	return GDRAW_CANVAS_OBJECT (gdraw_ellipse_canvas_object_new (ellipse, canvas));
}

static BonoboCanvasComponent *
ellipse_item_creator (BonoboObject *embeddable, GnomeCanvas *canvas,
		      gpointer data)
{
	BonoboCanvasComponent *component;
	GDrawCanvasObject *cobj;
	GDrawEllipseShape *shape = GDRAW_ELLIPSE_SHAPE (data);

	cobj = create_canvas_object (shape, canvas);

	component = bonobo_canvas_component_new (GNOME_CANVAS_ITEM (
						 cobj->object_group));

	if (component == NULL) {
		gtk_object_destroy (GTK_OBJECT (cobj));
		return NULL;
	}

	gdraw_canvas_object_set_component (cobj, component);

	return component;
}

GDrawEllipseShape *
gdraw_ellipse_shape_new ()
{
	GDrawShape *shape;
	GDrawEllipseShape *ellipse;
	BonoboObject *embeddable;
	GDrawEllipseStream *stream;
	static gint count = 0;

	ellipse = g_object_new (GDRAW_ELLIPSE_SHAPE_TYPE, NULL);
	shape = GDRAW_SHAPE (ellipse);

	ellipse->bag = create_property_bag (ellipse);

	shape->desc->name = g_strdup_printf ("ellipse%d", count++);
	shape->type = g_strdup (GDRAW_ELLIPSE_OAFIID);

	//FIXME bonobo_embeddable_new_... is deprecated
	//embeddable = bonobo_embeddable_new_canvas_item (ellipse_item_creator,
	//					        ellipse);
	embeddable = NULL;

	bonobo_object_add_interface (BONOBO_OBJECT (ellipse), 
				     BONOBO_OBJECT (embeddable));

	ellipse->embeddable = embeddable;

	stream = gdraw_ellipse_stream_new (ellipse);

	if (!stream) {
		bonobo_object_unref (BONOBO_OBJECT (ellipse));
		return NULL;
	}

	bonobo_object_add_interface (BONOBO_OBJECT (ellipse),
				     BONOBO_OBJECT (stream));

	ellipse->stream = stream;

	return ellipse;
}

BonoboObject *
gdraw_ellipse_shape_factory (BonoboGenericFactory *this)
{
	GDrawEllipseShape *ellipse;

	ellipse = gdraw_ellipse_shape_new ();

	return BONOBO_OBJECT (ellipse);
}


