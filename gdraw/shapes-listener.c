/*
 * shapes-listener.c: Implements the ShapesListener Interface class
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Authors: Mike Kestner <mkestner@ameritech.net>
 */

#include <config.h>
#include <bonobo.h>
#include "GNOME_Draw.h"
#include "gdraw-marshal.h"
#include "shapes-listener.h"

#define PARENT_TYPE BONOBO_OBJECT_TYPE

enum SIGNALS {
	SHAPE_ADDED,
	SHAPE_REMOVED,
	LAST_SIGNAL
};
static guint signals[LAST_SIGNAL] = { 0, 0, };

static inline GDrawShapesListener *
gdraw_shapes_listener_from_servant (PortableServer_Servant servant)
{
	return GDRAW_SHAPES_LISTENER (bonobo_object_from_servant (servant));
}	   

static void
ShapesListener_event (PortableServer_Servant servant, const CORBA_char *name,
		      const CORBA_any *arg, CORBA_Environment * ev)
{
	GDrawShapesListener *sl = gdraw_shapes_listener_from_servant (servant);

	if (!g_strcasecmp (name, "GNOME/Draw/ShapeAddedEvent")) {
		GNOME_Draw_ShapeAddedEvent *sae;
		g_return_if_fail (CORBA_TypeCode_equal (
			arg->_type, TC_GNOME_Draw_ShapeAddedEvent, ev));
		sae = (GNOME_Draw_ShapeAddedEvent *) arg->_value;
		g_signal_emit_by_name(G_OBJECT(sl), "shape_added", 
				      sae->name, sae->add_shape);
	} else if (!g_strcasecmp (name, "GNOME/Draw/ShapeRemovedEvent")) {
		g_return_if_fail (arg->_type->kind != CORBA_tk_string);
		g_signal_emit_by_name(G_OBJECT(sl), "shape_removed", 
				      (gchar *)arg->_value);
	} else 
		g_warning ("Unexpected event received - %s", name);

}

static void
gdraw_shapes_listener_class_init (GObjectClass *klass)
{
	GDrawShapesListenerClass *sl_class = 
				GDRAW_SHAPES_LISTENER_CLASS (klass);
	POA_Bonobo_Listener__epv *epv = &sl_class->epv;

	epv->event = ShapesListener_event;

	signals[SHAPE_ADDED] = g_signal_new(
		"shape_added",
		G_OBJECT_CLASS_TYPE(klass),
		G_SIGNAL_RUN_LAST,
		G_STRUCT_OFFSET(GDrawShapesListenerClass, shape_added),
		NULL, NULL,
		gdraw_marshal_VOID__STRING_POINTER,
		G_TYPE_NONE, 2,
		G_TYPE_STRING, G_TYPE_POINTER);
	signals[SHAPE_REMOVED] = g_signal_new(
		"shape_removed",
		G_OBJECT_CLASS_TYPE(klass),
		G_SIGNAL_RUN_LAST,
		G_STRUCT_OFFSET(GDrawShapesListenerClass, shape_removed),
		NULL, NULL,
		g_cclosure_marshal_VOID__STRING,
		G_TYPE_NONE, 1,
		G_TYPE_STRING);
}

static void
gdraw_shapes_listener_init (GObject *object)
{
}

BONOBO_TYPE_FUNC_FULL (GDrawShapesListener, Bonobo_Listener, 
		       PARENT_TYPE, gdraw_shapes_listener);

GDrawShapesListener *
gdraw_shapes_listener_new ()
{
	GDrawShapesListener *sl;

	sl = g_object_new (GDRAW_SHAPES_LISTENER_TYPE, NULL);

	return sl;
}

