/*
 * shapes-layer-uih.c: UI implementation for the ShapesLayer class.
 *
 * Copyright (c) 2000-2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 */

#include <gtk/gtkfilesel.h>
#include <glib-object.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <bonobo/bonobo-property-bag-client.h>
#include <bonobo/bonobo-exception.h>
#include "shapes-layer-uih.h"
#include "oafiids.h"
#include "GNOME_Draw.h"

static void
rect_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	GDrawShapesLayer *layer = GDRAW_SHAPES_LAYER (user_data);
	gchar *val = NULL;
	gchar *path = NULL;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	path = g_strdup_printf ("/commands/%s", cname);
	val = bonobo_ui_component_get_prop (uic, path, "state", &ev);

	if (!g_strcasecmp (val, "1")) {
		gdraw_shapes_layer_set_mode (layer, GD_LAYER_MODE_CREATE_RECT);
		gdraw_shapes_layer_set_oaf_iid (layer, GDRAW_RECT_OAFIID);
	} else
		gdraw_shapes_layer_set_mode (layer, GD_LAYER_MODE_NORMAL);

	g_free (val);
	g_free (path);
	CORBA_exception_free (&ev);
}

static void
ellipse_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	GDrawShapesLayer *layer = GDRAW_SHAPES_LAYER (user_data);
	gchar *val = NULL;
	gchar *path = NULL;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	path = g_strdup_printf ("/commands/%s", cname);
	val = bonobo_ui_component_get_prop (uic, path, "state", &ev);

	if (!g_strcasecmp (val, "1")) {
		gdraw_shapes_layer_set_mode (layer, GD_LAYER_MODE_CREATE_OVAL);
		gdraw_shapes_layer_set_oaf_iid (layer, GDRAW_ELLIPSE_OAFIID);
	} else
		gdraw_shapes_layer_set_mode (layer, GD_LAYER_MODE_NORMAL);

	g_free (val);
	g_free (path);
	CORBA_exception_free (&ev);
}

typedef struct {
	GDrawShapesLayer *layer;
	GtkFileSelection *fs;
} layer_fs_t;

static void
image_ok (GtkObject *button, layer_fs_t *lfs)
{
	GDrawShapesLayer *layer = lfs->layer;
	GdkPixbuf *pixbuf;
	gchar *filename;
	Bonobo_PropertyBag pbag;
	GNOME_Draw_Shape shape;
	GNOME_Draw_Point pt;
	GNOME_Draw_Size sz;
	CORBA_Environment ev;

	filename = g_strdup (gtk_file_selection_get_filename (lfs->fs));
	gtk_object_destroy (GTK_OBJECT (lfs->fs));
	g_free (lfs);

	pixbuf = gdk_pixbuf_new_from_file (filename, NULL);
	g_free (filename);
	if (!pixbuf) {
		/* FIXME: Throw a Load failure dialog */
		g_warning ("Image file could not be loaded.");
		return;
	}
	sz.width = gdk_pixbuf_get_width (pixbuf);
	sz.height = gdk_pixbuf_get_height (pixbuf);
	gdk_pixbuf_unref (pixbuf);
	gdraw_shapes_layer_get_center (layer, &pt.x, &pt.y);
	pt.x -= (sz.width / 2);
	pt.y -= (sz.height / 2);

	CORBA_exception_init (&ev);

	shape = bonobo_activation_activate_from_id (GDRAW_IMAGE_OAFIID, 0, NULL, &ev);
	g_return_if_fail (shape != CORBA_OBJECT_NIL && !BONOBO_EX (&ev));

	GNOME_Draw_Shape_setPosition (shape, &pt, &ev);
	GNOME_Draw_Shape_setSize (shape, &sz, &ev);
	if (BONOBO_EX (&ev)) {
		g_warning ("%s exception occured while sizing/position shape.",
			   bonobo_exception_get_text (&ev));
		return;
	}

	pbag = Bonobo_Unknown_queryInterface(
		shape, "IDL:Bonobo/PropertyBag:1.0", &ev);
	if (pbag == CORBA_OBJECT_NIL || BONOBO_EX(&ev)) {
		g_warning ("%s exception occured while getting pbag.",
			   bonobo_exception_get_text (&ev));
		return;
	}
	bonobo_property_bag_client_set_value_string (
					pbag, "GraphicURL", filename, NULL);
	Bonobo_Unknown_unref (pbag, &ev);

	gdraw_shapes_layer_add_shape (layer, shape);

	Bonobo_Unknown_unref (shape, &ev);

	CORBA_exception_free (&ev);
}

static void
image_cancel (GtkObject *button, layer_fs_t *lfs)
{
	gtk_object_destroy (GTK_OBJECT (lfs->fs));
	g_free (lfs);
}

static void
image_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *cname)
{
	layer_fs_t *lfs = g_new0 (layer_fs_t, 1);

	lfs->layer = GDRAW_SHAPES_LAYER (user_data);
	lfs->fs = GTK_FILE_SELECTION (gtk_file_selection_new (
							"Select Image File"));

	gtk_file_selection_hide_fileop_buttons (lfs->fs);
	gtk_window_set_modal (GTK_WINDOW (lfs->fs), TRUE);

	g_signal_connect (G_OBJECT (lfs->fs->ok_button), "clicked", 
			  G_CALLBACK(image_ok), lfs);
	g_signal_connect (G_OBJECT (lfs->fs->cancel_button), "clicked", 
			  G_CALLBACK(image_cancel), lfs);

	gtk_widget_show (GTK_WIDGET (lfs->fs));
}

static BonoboUIVerb layer_verbs[] = {
	BONOBO_UI_VERB ("CreateRect", rect_cb),
	BONOBO_UI_VERB ("CreateEllipse", ellipse_cb),
	BONOBO_UI_VERB ("CreateImage", image_cb),
	BONOBO_UI_VERB_END
};

gchar layer_cmds [] =
	"<commands>\n"
	"	<cmd name=\"CreateRect\" _tip=\"Draw a rectangle shape\"/>\n"
	"	<cmd name=\"CreateEllipse\" _tip=\"Draw an ellipse shape\"/>\n"
	"	<cmd name=\"CreateImage\" _tip=\"Insert an image shape\"/>\n"
	"</commands>\n";

gchar layer_tbar [] = 
	"<dockitem name=\"Draw_Toolbar\" _tip=\"Main toolbar\">\n"
	"	<toolitem name=\"CreateRect\" type=\"toggle\" _label=\"Rect\" pixtype=\"filename\" pixname=\"GDraw_rect.xpm\" verb=\"\"/>\n"
	"	<toolitem name=\"CreateEllipse\" type=\"toggle\" _label=\"Ellipse\" pixtype=\"filename\" pixname=\"GDraw_oval.xpm\" verb=\"\"/>\n"
	"	<toolitem name=\"CreateImage\" _label=\"Image\" pixtype=\"filename\" pixname=\"GDraw_image.xpm\" verb=\"\"/>\n"
	"</dockitem>\n";

BonoboUIComponent *
gdraw_shapes_layer_ui_component_new (GDrawShapesLayer *layer,
				     Bonobo_UIContainer corba_cont)
{
	BonoboUIComponent *comp;
	static gint count = 0;
	gchar *name;

	name = g_strdup_printf ("ShapesLayer%d", count++);
	comp = bonobo_ui_component_new (name);
	g_free (name);

	//FIXME: too few arguments to function `bonobo_ui_component_set_container'
	//bonobo_ui_component_set_container (comp, corba_cont);
	bonobo_ui_component_set_container (comp, corba_cont, NULL);

	bonobo_ui_component_set_translate (comp, "/", layer_tbar, NULL);
	bonobo_ui_component_set_translate (comp, "/", layer_cmds, NULL);

	bonobo_ui_component_add_verb_list_with_data (comp, layer_verbs, layer);

	return comp;
}

void
gdraw_shapes_layer_ui_component_clear_mode (BonoboUIComponent *uic,
					    GDrawShapesLayerMode mode)
{
	gchar *path = NULL;
	CORBA_Environment ev;

	switch (mode) {
	case GD_LAYER_MODE_CREATE_RECT:
		path = g_strdup ("/commands/CreateRect");
		break;
	case GD_LAYER_MODE_CREATE_OVAL:
		path = g_strdup ("/commands/CreateEllipse");
		break;
	default:
		return;
	}

	CORBA_exception_init (&ev);
	bonobo_ui_component_set_prop (uic, path, "state", "0", &ev);
	g_free (path);
	CORBA_exception_free (&ev);
}

