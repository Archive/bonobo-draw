/*
 * rect-shape-io.h: RectShape PersistStream implementation
 *
 * Copyright (c) 2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_RECT_SHAPE_IO_H
#define __GDRAW_RECT_SHAPE_IO_H

#include <bonobo/bonobo-persist.h>
#include <bonobo/Bonobo.h>

G_BEGIN_DECLS

typedef struct _GDrawRectStream      GDrawRectStream;
typedef struct _GDrawRectStreamClass GDrawRectStreamClass;

#include "rect-shape.h"

#define GDRAW_RECT_STREAM_TYPE      	 (gdraw_rect_stream_get_type())
#define GDRAW_RECT_STREAM(o)          (GTK_CHECK_CAST((o), GDRAW_RECT_STREAM_TYPE, GDrawRectStream))
#define GDRAW_RECT_STREAM_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GDRAW_RECT_STREAM_TYPE, GDrawRectStreamClass))
#define GDRAW_IS_RECT_STREAM(o)       (GTK_CHECK_TYPE((o), GDRAW_RECT_STREAM_TYPE))
#define GDRAW_IS_RECT_STREAM_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), GDRAW_RECT_STREAM_TYPE))


struct _GDrawRectStream {
	BonoboPersist	 	base;

	GDrawRectShape		*rect;
	gboolean		is_dirty;
};
	
struct _GDrawRectStreamClass {
	BonoboPersistClass parent_class;

	POA_Bonobo_PersistStream__epv epv;
};

GtkType gdraw_rect_stream_get_type (void);

GDrawRectStream *gdraw_rect_stream_new (GDrawRectShape *);

G_END_DECLS

#endif
