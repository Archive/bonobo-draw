/*
 * re-shape-io.h: REShape persistence interface
 *
 * Copyright (c) 2001 Mike Kestner
 *
 * Author: Mike Kestner <mkestner@ameritech.net>
 *
 */

#ifndef __GDRAW_RE_SHAPE_IO_H
#define __GDRAW_RE_SHAPE_IO_H

#include <libxml/tree.h>
#include "re-shape.h"

G_BEGIN_DECLS

gboolean gdraw_re_shape_load_from_xml (GDrawREShape *, xmlNode *);
xmlNode *gdraw_re_shape_save_to_xml (GDrawREShape *, xmlNode *);

G_END_DECLS

#endif
